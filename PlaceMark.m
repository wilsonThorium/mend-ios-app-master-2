//
//  PlaceMark.m
//  ConwayCorp
//
//  Created by Developer on 7/13/09.
//  Copyright 2009 Conway Corporation. All rights reserved.
//

#import "PlaceMark.h"


@implementation PlaceMark
@synthesize coordinate, _subtitle, _title, _color;

- (NSString *)subtitle{
	return _subtitle;
}
- (NSString *)title{
	return _title;
}
- (NSString *)color{
	return _color;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c
				  title:(NSString *)title
			   subtitle:(NSString *)subtitle
				  color:(NSString *)color 
{
	coordinate=c;
	_title = title;
	_subtitle = subtitle;
	_color = color;
	return self;
}




@end
