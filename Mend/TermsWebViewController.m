//
//  TermsWebViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/16/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "TermsWebViewController.h"

@interface TermsWebViewController ()
{
    IBOutlet UIWebView *browser;
    IBOutlet UINavigationItem *navItem;
}
@end

@implementation TermsWebViewController
@synthesize title, url;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    navItem.title = title;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: url] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 60];
    [browser loadRequest: request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = [NSString stringWithFormat:@"Terms View (%@)", navItem.title];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
