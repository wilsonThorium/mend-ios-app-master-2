//
//  PatientActionViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientActionViewController.h"
#import "PlaceMark.h"
#import "ChargePatientViewController.h"

@interface PatientActionViewController ()
{
    IBOutlet UIImageView *imgProfile;
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblDistance;
    IBOutlet UILabel *lblName;
    IBOutlet MKMapView *patientMap;
    IBOutlet UIView *patientBG;
    
    IBOutlet UIView *mapsLinkBG;
    UIView *blackout;
    UIView *deleteConfirmation;
    
    PlaceMark *pm;
    
    NSString *phone;
    NSString *name;
    double miles;
    NSString *address1;
    NSString *address2;
    NSString *city;
    NSString *state;
    NSString *zip;
    
}
@end

@implementation PatientActionViewController
@synthesize appointment, imgPatient, time, distance;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __block PFGeoPoint *userLocation = [PFGeoPoint geoPointWithLocation:[GPS sharedGPS].lastLocation];
  
    id customer = [appointment objectForKey:@"customer"];
     id user = [customer objectForKey:@"user"];
     name = [NSString stringWithFormat:@"%@ %@",[user objectForKey:@"firstName"], [user objectForKey:@"lastName"]];
     //NSLog(@"name: %@", name);
     phone = [user objectForKey:@"phone"];
    address1 = [appointment objectForKey:@"address1"];
    address2 = [appointment objectForKey:@"address2"];
    city = [appointment objectForKey:@"city"];
    state = [appointment objectForKey:@"state"];
    zip = [appointment objectForKey:@"zip"];
    
     NSString *dateString = [self getLocalDate:[appointment objectForKey:@"time"]];
     
     PFGeoPoint *apptLocation = [appointment objectForKey:@"location"];
     
     miles = [userLocation distanceInMilesTo:apptLocation];
    
    lblTime.text = dateString;
    lblDistance.text = [NSString stringWithFormat:@"%.1f mi", miles];
    lblName.text = name;
     
    
    // Do any additional setup after loading the view.
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = YES;
    imgProfile.layer.borderWidth = 5.0f;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if(imgPatient != nil)
        imgProfile.image = imgPatient;
    else{
        PFQuery *query = [PFQuery queryWithClassName:@"Customer"];
        
        // Retrieve the object by id
        [query getObjectInBackgroundWithId:[customer valueForKey:@"objectId"] block:^(PFObject *customer, NSError *error) {
            if(!error){
                if([customer objectForKey:@"avatar"])
                {
                    PFFile *userImageFile = customer[@"avatar"];
                    [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                        if (!error) {
                            imgProfile.image = [UIImage imageWithData:imageData];
                            imgPatient = imgProfile.image;
                        }
                    }];
                }
                else{
                    imgPatient = [UIImage imageNamed:@"profile-pic"];
                }
                
            }
            else{
                ////NSLog(@"Error retrieving profile image");
                imgPatient = [UIImage imageNamed:@"profile-pic"];
            }
        }];
    }
    
    patientBG.layer.cornerRadius = patientBG.frame.size.width/4.1;
    patientBG.clipsToBounds = YES;
    
    UITapGestureRecognizer *openMaps = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openGoogleMaps:)];
    openMaps.numberOfTapsRequired = 1;
    [mapsLinkBG addGestureRecognizer:openMaps];
    
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.033;
    span.longitudeDelta=0.033;
    //32.782572, -96.802236
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(apptLocation.latitude - .0085, apptLocation.longitude);
    
    region.span=span;
    region.center=coordinate;
    
    [patientMap setRegion:region animated:TRUE];
    [patientMap regionThatFits:region];
    //patientMap.userInteractionEnabled = NO;
    
    CLLocationCoordinate2D house = CLLocationCoordinate2DMake(apptLocation.latitude, apptLocation.longitude);
    
    
    if(address2 != nil && address2.length > 0){
        pm = [[PlaceMark alloc] initWithCoordinate:house title:[NSString stringWithFormat:@"%@, %@", address1, address2] subtitle:[NSString stringWithFormat:@"%@, %@ %@", city, state, zip] color:@"Red"];
    }
    else{
        pm = [[PlaceMark alloc] initWithCoordinate:house title:[NSString stringWithFormat:@"%@", address1] subtitle:[NSString stringWithFormat:@"%@, %@ %@", city, state, zip] color:@"Red"];
    }
    
    [patientMap addAnnotation:pm];
    
    
}

-(NSString *)getLocalDate:(NSDate *)utc{
    
    //NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //[df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"h:mm a"];
    
    NSString *localDateTime = [df_local stringFromDate:utc];
    /*NSDate *localDate = [df_local dateFromString:localDateTime];
     
     NSDateFormatter* timeFormat = [[NSDateFormatter alloc] init];
     [timeFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
     [timeFormat setDateFormat:@"h:mm a"];*/
    
    return localDateTime;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.screenName = @"Patient Action (Physician View)";
    
    //[self performSelectorInBackground:@selector(selectAnnotation) withObject:nil];
    [self performSelector:@selector(selectAnnotation) withObject:nil afterDelay:1.0];
}

-(void)selectAnnotation{
    [patientMap selectAnnotation:pm animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    else if ([annotation isKindOfClass:[PlaceMark class]]) // use whatever annotation class you used when creating the annotation
    {
        static NSString * const identifier = @"MyCustomAnnotation";
        
        MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView)
        {
            annotationView.annotation = annotation;
        }
        else
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:identifier];
        }
        annotationView.frame = CGRectMake(annotationView.frame.origin.x, annotationView.frame.origin.y, 30.0, 30.0);
        annotationView.contentMode = UIViewContentModeScaleAspectFit;
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"icon_mend_map_small"];
        annotationView.selected = YES;
        return annotationView;
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelVisit:(id)sender {
    blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
    blackout.backgroundColor = [UIColor blackColor];
    blackout.layer.opacity = 0.7;
    blackout.layer.zPosition = 2000;
    [self.view.window addSubview:blackout];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeConfirmation)];
    singleTap.numberOfTapsRequired = 1;
    [blackout addGestureRecognizer:singleTap];
    
    deleteConfirmation = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.view.window.frame.size.height/2 - 80, self.view.window.frame.size.width - 100.0, 175.0)];
    deleteConfirmation.backgroundColor = [UIColor whiteColor];
    deleteConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
    deleteConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
    deleteConfirmation.layer.shadowOpacity = 0.7;
    deleteConfirmation.clipsToBounds = NO;
    deleteConfirmation.layer.shadowRadius = 4;
    deleteConfirmation.layer.zPosition = 2001;
    [self.view.window addSubview:deleteConfirmation];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, deleteConfirmation.frame.size.width - 40.0, 60.0)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:14];
    lblTitle.text = @"Are you sure you want to cancel this visit?";
    lblTitle.numberOfLines = 0;
    [deleteConfirmation addSubview:lblTitle];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(deleteConfirmation.frame.size.width/2 - 60.0 - 10.0, 100.0, 60.0, 60.0);
    [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    [btnCancel setTitle:@"" forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(closeConfirmation) forControlEvents:UIControlEventTouchUpInside];
    [deleteConfirmation addSubview:btnCancel];
    
    UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
    btnConfirm.frame = CGRectMake(deleteConfirmation.frame.size.width/2 + 10, 100.0, 60.0, 60.0);
    [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
    [btnConfirm setTitle:@"" forState:UIControlStateNormal];
    [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
    [deleteConfirmation addSubview:btnConfirm];
}

-(void)closeConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    [deleteConfirmation removeFromSuperview];
    deleteConfirmation = nil;
}

-(void)confirmDeletion{
    [blackout removeFromSuperview];
    blackout = nil;
    [deleteConfirmation removeFromSuperview];
    deleteConfirmation = nil;
    //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[appointment valueForKey:@"objectId"] forKey:@"appointmentId"];
    [params setValue:[User sharedUser].providerID forKey:@"providerId"];
    ////NSLog(@"params: %@", params);
    
    [PFCloud callFunctionInBackground:@"cancelAppointment" withParameters:params block:^(id object, NSError *error) {
        if(error == nil){
            //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModalWithSuccess:@"Appointment cancelled"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancelled" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            ////NSLog(@"Error: %@", error.description);
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to cancel an appointment at this time."];
        }
    }];
}

- (IBAction)openGoogleMaps:(id)sender {
    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
        //NSString *directionsRequest = @"comgooglemaps-x-callback://?daddr=236+Meadow+Rd+Russellville+AR+72802&x-success=mend://?resume=true&x-source=MEND";
        NSMutableString *fullAddress = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@ %@ %@ %@ %@", address1, address2, city, state, zip]];
        [fullAddress replaceOccurrencesOfString:@" " withString:@"+" options:0 range:NSMakeRange(0, fullAddress.length)];
        NSString *directionsRequest = [NSString stringWithFormat:@"comgooglemaps-x-callback://?daddr=%@&x-success=mend://?resume=true&x-source=MEND", fullAddress];
        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [[UIApplication sharedApplication] openURL:directionsURL];
    } else {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showModal:@"Please install the GOOGLE MAPS app from the App Store to use this feature."];
    }
}
- (IBAction)callCustomer:(id)sender {
    NSMutableString *phNo = [[NSMutableString alloc] initWithString:phone];
    [phNo replaceOccurrencesOfString:@"(" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@")" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@"-" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:[NSString  stringWithFormat:@"*67%@",phNo]];
        exit(-1);
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:p"]];
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"This device does not support making phone calls." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"showChargeView"])
    {
        //see if it's in the future
        if([[NSDate date] compare:[appointment objectForKey:@"time"]] == NSOrderedDescending)
            return YES;
        else{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Sorry.  You cannot charge for future appointments."];
            return NO;
        }
    }
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[ChargePatientViewController class]])
    {
        ChargePatientViewController *vc = (ChargePatientViewController *)segue.destinationViewController;
        vc.appointment = appointment;
        vc.imgPatient = imgPatient;
    }
}


@end
