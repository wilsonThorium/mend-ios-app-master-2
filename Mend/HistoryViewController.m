//
//  HistoryViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/2/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "HistoryViewController.h"

@interface HistoryViewController ()
{
    NSMutableArray *history;
    IBOutlet UITableView *historyTable;
}
@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    history = [[NSMutableArray alloc] init];
               
               //WithArray:@[@{@"date":@"11/14/14",@"location":@"HOME",@"price":@"$150"},@{@"date":@"11/14/14",@"location":@"HOME",@"price":@"$150"},@{@"date":@"11/14/14",@"location":@"HOME",@"price":@"$150"}]];
    if([User sharedUser].customerID != nil)
    {
        PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
        [custQuery whereKey:@"objectId" equalTo:[User sharedUser].customerID];
        
        PFQuery *stripeQuery = [PFQuery queryWithClassName:@"StripeCharge"];
        [stripeQuery whereKey:@"customer" matchesQuery:custQuery];
        //[stripeQuery includeKey:@"customer"];
        [stripeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                ////NSLog(@"The getFirstObject request failed.");
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to get charge history at this time."];
            } else {
                [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSString *dateString = [self getLocalDate:[obj createdAt]];
                    NSString *desc;
                    if ([obj objectForKey:@"description"]) {
                        desc = [obj objectForKey:@"description"];
                    }
                    else{
                        desc = @" ";
                    }
                    
                    NSString *amt = [NSString stringWithFormat:@"$%.2f", ((NSNumber *)[obj objectForKey:@"amount"]).floatValue/100.0];
                    [history addObject:@{@"date":dateString,@"location":desc,@"price":amt}];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [historyTable reloadData];
                });
                
            }
            
            
        }];
    }
    else if ([User sharedUser].providerID != nil)
    {
        PFQuery *physQuery = [PFQuery queryWithClassName:@"Provider"];
        [physQuery whereKey:@"objectId" equalTo:[User sharedUser].providerID];
        
        PFQuery *stripeQuery = [PFQuery queryWithClassName:@"StripeCharge"];
        [stripeQuery whereKey:@"provider" matchesQuery:physQuery];
        //[stripeQuery includeKey:@"customer"];
        [stripeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                ////NSLog(@"The getFirstObject request failed.");
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to get charge history at this time."];
            } else {
                [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSString *dateString = [self getLocalDate:[obj createdAt]];
                    NSString *desc;
                    if ([obj objectForKey:@"description"]) {
                        desc = [obj objectForKey:@"description"];
                    }
                    else{
                        desc = @" ";
                    }
                    NSString *amt = [NSString stringWithFormat:@"$%.2f", ((NSNumber *)[obj objectForKey:@"amount"]).floatValue/100.0];
                    [history addObject:@{@"date":dateString,@"location":desc,@"price":amt}];
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [historyTable reloadData];
                });
            }
        }];
    }
    // Do any additional setup after loading the view.
}

-(NSString *)getLocalDate:(NSDate *)utc{
    
    //NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //[df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"yyyy.MM.dd HH:mm:ss zzz"];
    
    NSString *localDateTime = [df_local stringFromDate:utc];
    NSDate *localDate = [df_local dateFromString:localDateTime];
    
    NSDateFormatter* scheduleFormat = [[NSDateFormatter alloc] init];
    [scheduleFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [scheduleFormat setDateFormat:@"MM/dd/yy"];
    
    return [scheduleFormat stringFromDate:localDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Billing History";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return history.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSInteger const kDate = 100;
    static NSInteger const kLocation = 200;
    static NSInteger const kPrice = 300;
    
    UILabel *lblDate = nil;
    UILabel *lblLocation = nil;
    UILabel *lblPrice = nil;
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    else{
        lblDate = (UILabel *)[cell viewWithTag:kDate];
        lblLocation = (UILabel *)[cell viewWithTag:kLocation];
        lblPrice = (UILabel *)[cell viewWithTag:kPrice];
    }
    
    id History = [history objectAtIndex:indexPath.row];
    
    lblDate.text = [History objectForKey:@"date"];
    lblLocation.text = [History objectForKey:@"location"];
    lblPrice.text = [History objectForKey:@"price"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
