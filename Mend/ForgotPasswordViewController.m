//
//  ForgotPasswordViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/17/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()
{
    IBOutlet UITextField *txtEmail;
    UILabel *lblEmail;
    BOOL keyboardUp;
    IBOutlet NSLayoutConstraint *logoTopPadding;
    IBOutlet NSLayoutConstraint *resetBottomPadding;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Forgot Password";
    
    txtEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtEmail.layer.borderWidth = 1.0f;
    txtEmail.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
    
    UIView *txtEmailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtEmail.leftView = txtEmailPadding;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *bgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    bgTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:bgTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideKeyboard{
    [txtEmail resignFirstResponder];
    [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
        logoTopPadding.constant += 60.0;
        resetBottomPadding.constant -= 60.0;
    } completion:^(BOOL finished) {
        keyboardUp = NO;
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(!keyboardUp){
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            logoTopPadding.constant -= 60.0;
            resetBottomPadding.constant += 60.0;
        } completion:^(BOOL finished) {
            keyboardUp = YES;
        }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{    
        [txtEmail resignFirstResponder];
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            logoTopPadding.constant += 60.0;
            resetBottomPadding.constant -= 60.0;
        } completion:^(BOOL finished) {
            keyboardUp = NO;
        }];
    
    return YES;
}

#pragma mark - UITextfield methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *textString = @"";
    if(string.length > 0)
    {
        //typing
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) { // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
            return NO;
        }
        
        textString = [NSString stringWithFormat:@"%@%@", textField.text, string];
        //////NSLog(@"Full string: %@", textString);
    }
    else{
        //backspace
        textString = [textField.text substringToIndex:textField.text.length - 1];
    }
    
    if(textString.length == 1 && !lblEmail){
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        //lblEmail.textAlignment = NSTextAlignmentRight;
        lblEmail.textColor = [UIColor lightGrayColor];
        lblEmail.font = [UIFont systemFontOfSize:12.0];
        lblEmail.text = @"Email Address";
        [txtEmail addSubview:lblEmail];
    }
    else if (textString.length == 0 && lblEmail){
        [lblEmail removeFromSuperview];
        lblEmail = nil;
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
    }
    
    if(lblEmail.textColor == [UIColor redColor]){
        NSString *email = textString;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location != NSNotFound)
        {
            lblEmail.textColor = [UIColor lightGrayColor];
        }
    }
    
    return YES;
}

- (IBAction)resetPassword:(id)sender {
    [txtEmail resignFirstResponder];
    [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
        logoTopPadding.constant += 60.0;
        resetBottomPadding.constant -= 60.0;
    } completion:^(BOOL finished) {
        keyboardUp = NO;
    }];
    
    //TODO: actually reset the password
    [PFUser requestPasswordResetForEmailInBackground:txtEmail.text block:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Please check your email."];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"You are unable to reset your password at this time."];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
