//
//  User.h
//  Soteria
//
//  Created by Andrew Goodwin on 11/8/14.
//  Copyright (c) 2014 joebell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(nonatomic, strong) NSString *username;
@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *objectID;
@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSString *customerID;
@property(nonatomic, strong) NSString *providerID;
@property(nonatomic, strong) NSString *dob;
@property(nonatomic, strong) NSString *selectedLocationID;
@property(nonatomic) BOOL loggedIn;
+(User *)sharedUser;
-(void)save;
-(void)logout;
@end
