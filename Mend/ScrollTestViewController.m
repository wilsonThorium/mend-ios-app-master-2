//
//  ScrollTestViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/28/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "ScrollTestViewController.h"

@interface ScrollTestViewController ()
{
    IBOutlet UIScrollView *scroller;
    NSMutableArray *appointments;
    double lastOffset;
    
    double maxWidth;
    double minWidth;
    double offset;
    double halfWidth;
    double ratio;
    double widthDiff;
    
    __block int selectedAppointment;
}
@end

@implementation ScrollTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedAppointment = 0;
    
    appointments = [[NSMutableArray alloc] initWithArray:@[@{@"day":@"Today",@"time":@"8:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"10:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"11:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"12:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"9:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"10:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"11:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"12:30",@"ampm":@"AM"}]];
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    ////NSLog(@"Scroller frame: %@", NSStringFromCGRect(scroller.frame));
    
    UIView *centerLine = [[UIView alloc] initWithFrame:CGRectMake(scroller.frame.size.width/2 - 1, 0.0, 2.0, self.view.frame.size.height)];
    centerLine.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:centerLine];
    maxWidth = self.view.frame.size.width * 0.6;
    minWidth = self.view.frame.size.width * 0.25;
    //maxWidth = 200.0;
    //minWidth = 150.0;
    offset = self.view.frame.size.width / 3;
    halfWidth = scroller.frame.size.width / 2;
    ratio = minWidth/maxWidth;
    widthDiff = maxWidth - minWidth;
    
    [self reloadScrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.isDragging){
    BOOL scrollingLeft = YES;
    if (scroller.contentOffset.x > lastOffset) {
        //person's finger is moving left on the screen
        scrollingLeft = YES;
    }
    else{
        scrollingLeft = NO;
    }
    
    lastOffset = scroller.contentOffset.x;
    
    //////NSLog(@"Scroller offset: %@", NSStringFromCGPoint(scroller.contentOffset));
    
    [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        UIView *circle = (UIView *)obj;
        
        //////NSLog(@"Circle origin x: %@", NSStringFromCGPoint(circle.frame.origin));
        double scaleAmount = 80.0/110.0;
        double distanceCtoC = 0;
        distanceCtoC = ((circle.frame.origin.x + circle.frame.size.width / 2) - scroller.contentOffset.x) - scroller.frame.size.width / 2;
        //if(circle.tag == 2)
        //    ////NSLog(@"Distance to center: %.2f", distanceCtoC);
        if(distanceCtoC > -160 && distanceCtoC < 160)
        {
            if(distanceCtoC >= -40 && distanceCtoC <= 40)
                circle.layer.zPosition = 1000;
            else if((distanceCtoC < -40 && distanceCtoC >= -100) || (distanceCtoC > 40 && distanceCtoC <= 100))
                circle.layer.zPosition = 900;
            else if(distanceCtoC < -100 || distanceCtoC > 100)
                circle.layer.zPosition = 800;
            //left of center
            //circle.hidden = YES;
            if (scrollingLeft && distanceCtoC > 0 && circle.frame.size.height < 230) {
                //getting bigger
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y - scaleAmount, circle.frame.size.width + scaleAmount, circle.frame.size.height + scaleAmount);
            }
            else if (!scrollingLeft && distanceCtoC > 0 && circle.frame.size.height > 80){
                //getting smaller
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y + scaleAmount, circle.frame.size.width - scaleAmount, circle.frame.size.height - scaleAmount);
            }
            else if (scrollingLeft && distanceCtoC < 0 && circle.frame.size.height > 80) {
                //getting smaller
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y + scaleAmount, circle.frame.size.width - scaleAmount, circle.frame.size.height - scaleAmount);
            }
            else if (!scrollingLeft && distanceCtoC < 0 && circle.frame.size.height < 230){
                //getting bigger
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y - scaleAmount, circle.frame.size.width + scaleAmount, circle.frame.size.height + scaleAmount);
            }
            else{
                ////NSLog(@"Something different");
            }
            
            circle.layer.cornerRadius = circle.frame.size.width / 2;
        }
        else{
            //off screen
        }
        
        
        
    }];
    }
    //grab scroll direction L or R
    //foreach through circles
    //if circle is getting closer to the center then it needs to get bigger
    //if circle is getting farther from the center then it needs to get smaller
    //if circle is getting closer to the center then it is moving faster
    //if circle is getting farther from the center then it is moving slower
    
}*/

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.isDragging){
        BOOL scrollingLeft = YES;
        if (scroller.contentOffset.x > lastOffset) {
            scrollingLeft = YES;
        }
        else{
            scrollingLeft = NO;
        }
        
        lastOffset = scroller.contentOffset.x;
        
        //////NSLog(@"Scroller offset: %@", NSStringFromCGPoint(scroller.contentOffset));
        
        __block int highestZIndex = 0;
        
        [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIView *circle = (UIView *)obj;
            
            double distanceCtoC = ((circle.frame.origin.x + circle.frame.size.width / 2.0) - scroller.contentOffset.x) - scroller.frame.size.width / 2.0;
            
            if(abs(distanceCtoC) < halfWidth + 60)
            {
                CGPoint centerPoint = circle.center;
                
                double diameter = minWidth +  (halfWidth - abs(distanceCtoC)) * (widthDiff / halfWidth);
                
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y, diameter, diameter);
                circle.center = centerPoint;
                
                circle.layer.cornerRadius = circle.frame.size.width / 2;
                circle.layer.zPosition = 1000 - abs(distanceCtoC);
                
                if (circle.layer.zPosition > highestZIndex) {
                    highestZIndex = circle.layer.zPosition;
                    selectedAppointment = idx;
                }
            }
            else{
                //off screen
            }
            
            
            
        }];
        
        ////NSLog(@"selected appointment index: %i", selectedAppointment);
    }
    
}

/*-(void)reloadScrollView{
    [appointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *circle = nil;
        
        UILabel *lblNextAvailable = nil;
        UILabel *lblTime = nil;
        UILabel *lblAMPM = nil;
        
        if(idx == 0){
            double diameter = maxWidth;
            circle = [[UIView alloc] initWithFrame:CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter)];
            circle.backgroundColor = [UIColor whiteColor];
            circle.layer.cornerRadius = circle.frame.size.width/2;
            circle.clipsToBounds = NO;
            circle.tag = idx;
            circle.layer.shadowColor = [UIColor blackColor].CGColor;
            circle.layer.shadowOffset = CGSizeMake(0, 1);
            circle.layer.shadowOpacity = 0.7;
            circle.layer.shadowRadius = 2;
            circle.layer.zPosition = 1000;
            [scroller addSubview:circle];
            
            lblNextAvailable = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 25.0, circle.frame.size.width, 50.0)];
            lblNextAvailable.backgroundColor = [UIColor clearColor];
            lblNextAvailable.textAlignment = NSTextAlignmentCenter;
            lblNextAvailable.font = [lblNextAvailable.font fontWithSize:10];
            lblNextAvailable.tag = 1;
            [circle addSubview:lblNextAvailable];
            
            lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height / 2 - 25.0, circle.frame.size.width, 50.0)];
            lblTime.backgroundColor = [UIColor clearColor];
            lblTime.textAlignment = NSTextAlignmentCenter;
            lblTime.font = [lblTime.font fontWithSize:50];
            lblTime.tag = 2;
            [circle addSubview:lblTime];
            
            lblAMPM = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height - 75.0, circle.frame.size.width, 50.0)];
            lblAMPM.backgroundColor = [UIColor clearColor];
            lblAMPM.textAlignment = NSTextAlignmentCenter;
            lblAMPM.font = [lblAMPM.font fontWithSize:30];
            lblAMPM.tag = 3;
            [circle addSubview:lblAMPM];
        }
        else{
            double diameter = minWidth +  (halfWidth - offset) * (widthDiff / halfWidth);
            circle = [[UIView alloc] initWithFrame:CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter)];
            circle.backgroundColor = [UIColor whiteColor];
            circle.layer.cornerRadius = circle.frame.size.width/2;
            circle.clipsToBounds = NO;
            circle.tag = idx;
            circle.layer.shadowColor = [UIColor blackColor].CGColor;
            circle.layer.shadowOffset = CGSizeMake(0, 1);
            circle.layer.shadowOpacity = 0.7;
            circle.layer.shadowRadius = 2;
            [scroller addSubview:circle];
            circle.layer.zPosition = 100 - idx;
        }
        
        
        
        id Appointment = [appointments objectAtIndex:idx];
        lblNextAvailable.text = @"NEXT AVAILABLE VISIT";
        if(idx == 0)
            lblNextAvailable.hidden = NO;
        else
            lblNextAvailable.hidden = YES;
        lblTime.text = [Appointment objectForKey:@"time"];
        lblAMPM.text = [Appointment objectForKey:@"ampm"];
    }];
    
    scroller.contentSize = CGSizeMake((appointments.count * (maxWidth) - offset - 25.0), scroller.frame.size.height);
}*/

-(void)reloadScrollView{
    [appointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *circle = nil;
        
        UILabel *lblNextAvailable = nil;
        UILabel *lblTime = nil;
        UILabel *lblAMPM = nil;
        
        
            double diameter = maxWidth;
            circle = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, diameter, diameter)];
            circle.backgroundColor = [UIColor whiteColor];
        circle.layer.cornerRadius = circle.frame.size.width/2;
            circle.clipsToBounds = NO;
            circle.tag = idx;
            circle.layer.shadowColor = [UIColor blackColor].CGColor;
            circle.layer.shadowOffset = CGSizeMake(0, 1);
            circle.layer.shadowOpacity = 0.7;
            circle.layer.shadowRadius = 2;
            circle.layer.zPosition = 1000 - idx;
            //[scroller addSubview:circle];
            
            lblNextAvailable = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 25.0, circle.frame.size.width, 50.0)];
            lblNextAvailable.backgroundColor = [UIColor clearColor];
            lblNextAvailable.textAlignment = NSTextAlignmentCenter;
            lblNextAvailable.font = [lblNextAvailable.font fontWithSize:10];
            lblNextAvailable.tag = 1;
            [circle addSubview:lblNextAvailable];
            
            lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height / 2 - 25.0, circle.frame.size.width, 50.0)];
            lblTime.backgroundColor = [UIColor clearColor];
            lblTime.textAlignment = NSTextAlignmentCenter;
            lblTime.font = [lblTime.font fontWithSize:50];
            lblTime.tag = 2;
            [circle addSubview:lblTime];
            
            lblAMPM = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height - 75.0, circle.frame.size.width, 50.0)];
            lblAMPM.backgroundColor = [UIColor clearColor];
            lblAMPM.textAlignment = NSTextAlignmentCenter;
            lblAMPM.font = [lblAMPM.font fontWithSize:30];
            lblAMPM.tag = 3;
            [circle addSubview:lblAMPM];
        
        
        id Appointment = [appointments objectAtIndex:idx];
        lblNextAvailable.text = @"NEXT AVAILABLE VISIT";
        if(idx == 0)
            lblNextAvailable.hidden = NO;
        else
            lblNextAvailable.hidden = YES;
        lblTime.text = [Appointment objectForKey:@"time"];
        lblAMPM.text = [Appointment objectForKey:@"ampm"];
        
        //take picture of view and add it to scroller
        //UIGraphicsBeginImageContext(CGSizeMake(diameter, diameter));
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(diameter, diameter), NO, 0.0);
        [circle.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *circleImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageView *img = [[UIImageView alloc] initWithImage:circleImage];
        [scroller addSubview:img];
    }];
    
    __block int circleFound = 0;
    [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *circle = (UIImageView *)obj;
            if (circleFound > 0) {
                double diameter = minWidth +  (halfWidth - offset) * (widthDiff / halfWidth);
                circle.frame = CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter);
            }
            else{
                double diameter = maxWidth;
                circle.frame = CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter);
            }
            circle.layer.zPosition = 1000 - idx * 10;
            circle.layer.cornerRadius = circle.frame.size.width/2;
            circleFound++;
        }
    }];
    
    scroller.contentSize = CGSizeMake((appointments.count * (maxWidth) - offset - 25.0), scroller.frame.size.height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
