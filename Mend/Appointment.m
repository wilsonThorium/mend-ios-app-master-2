//
//  Appointment.m
//  Mend
//
//  Created by Andrew Goodwin on 2/18/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "Appointment.h"

@implementation Appointment
@synthesize appointmentID, day, time, address1, address2, physician, physicianTitle, physicianImage, appointmentDate, cancelByDate, createdDate;

- (id) init {
    self = [super init];
    if (self != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if([defaults objectForKey:@"appointmentDate"] != nil){
            self.appointmentDate = [defaults objectForKey:@"appointmentDate"];
            if ([[NSDate date] compare:self.appointmentDate] == NSOrderedDescending) {
                //appointment has already passed
                [self cancel];
            }
            else{
                if([defaults objectForKey:@"appointmentID"] != nil)
                    self.appointmentID = [defaults objectForKey:@"appointmentID"];
                
                if([defaults objectForKey:@"day"] != nil){
                    self.day = [defaults objectForKey:@"day"];
                }
                
                self.day = [self todayOrTomorrow:self.appointmentDate];
                
                if([defaults objectForKey:@"time"] != nil)
                    self.time = [defaults objectForKey:@"time"];
                
                if([defaults objectForKey:@"address1"] != nil)
                    self.address1 = [defaults objectForKey:@"address1"];
                
                if([defaults objectForKey:@"address2"] != nil)
                    self.address2 = [defaults objectForKey:@"address2"];
                
                if([defaults objectForKey:@"physician"] != nil)
                    self.physician = [defaults objectForKey:@"physician"];
                
                if([defaults objectForKey:@"physicianTitle"] != nil)
                    self.physicianTitle = [defaults objectForKey:@"physicianTitle"];
                
                if([defaults objectForKey:@"physicianImage"] != nil)
                    self.physicianImage = [defaults objectForKey:@"physicianImage"];
                
                if([defaults objectForKey:@"cancelByDate"] != nil)
                    self.cancelByDate = [defaults objectForKey:@"cancelByDate"];
                
                if([defaults objectForKey:@"createdDate"] != nil)
                    self.createdDate = [defaults objectForKey:@"createdDate"];
            }
        }
        
        
    }
    return self;
}

-(NSString *)todayOrTomorrow:(NSDate *)utc{
    
    //NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //[df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateComponents *todayComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"yyyy.MM.dd HH:mm:ss zzz"];
    
    if(utc != nil)
    {
        NSString *localDateTime = [df_local stringFromDate:utc];
        NSDate *localDate = [df_local dateFromString:localDateTime];
        
        if(localDate != nil)
        {
            NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:localDate];
            
            if(todayComps.day == dateComps.day)
            {
                return @"TODAY";
            }
            else{
                return @"TOMORROW";
            }
        }
        return @"Unknown";
    }
    
    return @"Unknown";
}

-(void)save{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(self.appointmentID != nil)
        [defaults setValue:self.appointmentID forKey:@"appointmentID"];
    else
        [defaults setValue:nil forKey:@"appointmentID"];
    
    if(self.day != nil)
        [defaults setValue:self.day forKey:@"day"];
    else
        [defaults setValue:nil forKey:@"day"];
    
    if(self.time != nil)
        [defaults setValue:self.time forKey:@"time"];
    else
        [defaults setValue:nil forKey:@"time"];
    
    if(self.address1 != nil)
        [defaults setValue:self.address1 forKey:@"address1"];
    else
        [defaults setValue:nil forKey:@"address1"];
    
    if(self.address2 != nil)
        [defaults setValue:self.address2 forKey:@"address2"];
    else
        [defaults setValue:nil forKey:@"address2"];
    
    if(self.physician != nil)
        [defaults setValue:self.physician forKey:@"physician"];
    else
        [defaults setValue:nil forKey:@"physician"];
    
    if(self.physicianTitle != nil)
        [defaults setValue:self.physicianTitle forKey:@"physicianTitle"];
    else
        [defaults setValue:nil forKey:@"physicianTitle"];
    
    if(self.physicianImage != nil)
        [defaults setValue:self.physicianImage forKey:@"physicianImage"];
    else
        [defaults setValue:nil forKey:@"physicianImage"];
    
    if(self.createdDate != nil)
        [defaults setValue:self.createdDate forKey:@"createdDate"];
    else
        [defaults setValue:nil forKey:@"createdDate"];
    
    if(self.appointmentDate != nil){
        [defaults setValue:self.appointmentDate forKey:@"appointmentDate"];
        
        ////NSLog(@"created: %@", createdDate.description);
        ////NSLog(@"date: %@", [NSDate date].description);
        //compute cancel by date
        if(self.cancelByDate == nil)
        {
            NSDate *cancelBy = [self.createdDate dateByAddingTimeInterval:30*60];
            self.cancelByDate = cancelBy;
        }
        [defaults setValue:self.cancelByDate forKey:@"cancelByDate"];
        
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [self.appointmentDate dateByAddingTimeInterval:-60*60];
        localNotification.alertBody = @"Your appointment is in an hour";
        localNotification.alertAction = @"Open MEND";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        //localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    else{
        [defaults setValue:nil forKey:@"appointmentDate"];
        [defaults setValue:nil forKey:@"cancelByDate"];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        if(countdownTimer.isValid){
            [countdownTimer invalidate];
            countdownTimer = nil;
        }
    }
        
    
    [defaults synchronize];
}

-(void)cancel{
    self.appointmentID = nil;
    self.day = nil;
    self.time = nil;
    self.address1 = nil;
    self.address2 = nil;
    self.physician = nil;
    self.physicianTitle = nil;
    self.physicianImage = nil;
    self.appointmentDate = nil;
    self.createdDate = nil;
    self.cancelByDate = nil;
    [self save];
}

-(void)startTimer{
    NSTimeInterval interval = [self.cancelByDate timeIntervalSinceDate:[NSDate date]];
    self.day = [self todayOrTomorrow:self.appointmentDate];
    int seconds = (int)interval % 60;
    [self performSelector:@selector(firstTick) withObject:nil afterDelay:seconds]; //delay is number of seconds before the next minute
    if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedDescending) {
        ////NSLog(@"date1 is later than date2");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    } else if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedAscending) {
        ////NSLog(@"date1 is earlier than date2");
        //continue
        countdownTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
        int minutes = (int)interval / 60;
        if(minutes == 1){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minute remaining to cancel for free.", minutes]];
        }
        else if (minutes == 0){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:@"Less than a minute remaining to cancel for free."];
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minutes remaining to cancel for free.", minutes + 1]];
        }
    } else {
        ////NSLog(@"dates are the same");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    }
}

-(void)firstTick{
    self.day = [self todayOrTomorrow:self.appointmentDate];
    if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedDescending) {
        ////NSLog(@"date1 is later than date2");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    } else if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedAscending) {
        ////NSLog(@"date1 is earlier than date2");
        //continue
        countdownTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
        NSTimeInterval interval = [self.cancelByDate timeIntervalSinceDate:[NSDate date]];
        int minutes = (int)interval / 60;
        if(minutes == 1){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minute remaining to cancel for free.", minutes]];
        }
        else if (minutes == 0){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:@"Less than a minute remaining to cancel for free."];
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minutes remaining to cancel for free.", minutes + 1]];
        }
    } else {
        ////NSLog(@"dates are the same");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    }
}

-(void)timerTick{
    self.day = [self todayOrTomorrow:self.appointmentDate];
    if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedDescending) {
        ////NSLog(@"date1 is later than date2");
        [countdownTimer invalidate];
        countdownTimer = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    } else if (self.cancelByDate != nil && [[NSDate date] compare:self.cancelByDate] == NSOrderedAscending) {
        ////NSLog(@"date1 is earlier than date2");
        //continue
        NSTimeInterval interval = [self.cancelByDate timeIntervalSinceDate:[NSDate date]];
        int minutes = (int)interval / 60;
        if(minutes == 1){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minute remaining to cancel for free.", minutes]];
        }
        else if (minutes == 0){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:@"Less than a minute remaining to cancel for free."];
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AdjustCountdownTimer" object:[NSString stringWithFormat:@"%i minutes remaining to cancel for free.", minutes + 1]];
        }
    } else {
        ////NSLog(@"dates are the same");
        [countdownTimer invalidate];
        countdownTimer = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerWillBeCharged" object:nil];
    }
}

+(Appointment *)currentAppointment{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[Appointment alloc] init];
    });
    return sharedInstance;
}

@end
