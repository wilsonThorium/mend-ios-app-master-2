//
//  PatientTOSViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientTOSViewController.h"
#import "PatientNoticeOfPrivacyViewController.h"

@interface PatientTOSViewController ()
{
    NSString *TOSURL;
    IBOutlet UIView *footer;
    IBOutlet UIWebView *browser;
    IBOutlet UIView *agreementView;
}
@end

@implementation PatientTOSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TOSURL = @"http://www.mendathome.com/mobile-terms-of-use/";
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: TOSURL] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 60];
    [browser loadRequest: request];
    
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToPrivacy)];
    singleTap.numberOfTapsRequired = 1;
    [footer addGestureRecognizer:singleTap];
    
    agreementView.layer.shadowColor = [UIColor blackColor].CGColor;
    agreementView.layer.shadowOffset = CGSizeMake(0, -1);
    agreementView.layer.shadowOpacity = 0.3;
    agreementView.layer.shadowRadius = 1;
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Patient TOS";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

-(void)goToPrivacy{
    [self performSegueWithIdentifier:@"segueShowAgreement2" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)unwindToTOS:(UIStoryboardSegue *)unwindSegue{
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    ////NSLog(@"Identifier: %@", segue.identifier);
    
    if([segue.destinationViewController isKindOfClass:[PatientNoticeOfPrivacyViewController class]]){
        PatientNoticeOfPrivacyViewController *vc = (PatientNoticeOfPrivacyViewController *)segue.destinationViewController;
        vc.TOSInfo = @{@"tosAccepted":[NSNumber numberWithBool:@YES],@"tosTimestamp":[NSDate date]};
    }
    else{
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showModal:@"Please contact Customer Service at (469) 458-6363 for information about our Terms of Use."];
    }
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}


@end
