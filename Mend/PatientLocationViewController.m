//
//  PatientLocationViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientLocationViewController.h"



@interface PatientLocationViewController ()
{
    IBOutlet UIView *nextView;
    IBOutlet UIScrollView *scroller;
    
    IBOutlet UILabel *lblScreenTitle;
    IBOutlet UITextField *txtNickname;
    IBOutlet UITextField *txtStreet;
    IBOutlet UITextField *txtAptSuite;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtZip;
    IBOutlet NSLayoutConstraint *txtZipBottomMargin;
    IBOutlet UIButton *btSave;
    
    __weak IBOutlet UIButton *btLocation;
    UIView *statePickerView;
    UIPickerView *statePicker;
    NSArray *stateNames;
    NSArray *stateValues;
    int pickerSelectedIndex;
    
    UILabel *lblNickname;
    UILabel *lblStreet;
    UILabel *lblAptSuite;
    UILabel *lblCity;
    UILabel *lblState;
    UILabel *lblZip;
    
    UIButton *btnChooseState;
    BOOL pickerShowing;
    
    NSString *streetSoFar;
}
@end

@implementation PatientLocationViewController{

}
@synthesize modal, profileImage, stripeID, signUpInfo, editMode, AddressToEdit;





- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    //editMode = NO;
    self.screenName = @"Patient Location";
    pickerShowing = NO;
    pickerSelectedIndex = 0;
    streetSoFar = @"";
    
    stateNames = @[@"", @"Alabama", @"Alaska", @"Arizona", @"Arkansas", @"California", @"Colorado", @"Connecticut", @"Delaware", @"Florida", @"Georgia", @"Hawaii", @"Idaho", @"Illinois", @"Indiana", @"Iowa", @"Kansas", @"Kentucky", @"Louisiana", @"Maine", @"Maryland", @"Massachusetts", @"Michigan", @"Minnesota", @"Mississippi", @"Missouri", @"Montana", @"Nebraska", @"Nevada", @"New Hampshire", @"New Jersey", @"New Mexico", @"New York", @"North Carolina", @"North Dakota", @"Ohio", @"Oklahoma", @"Oregon", @"Pennsylvania", @"Rhode Island", @"South Carolina", @"South Dakota", @"Tennessee", @"Texas", @"Utah", @"Vermont", @"Virginia", @"Washington", @"West Virginia", @"Wisconsin", @"Wyoming"];
    
    stateValues = @[@"", @"AL", @"AK", @"AZ", @"AR", @"CA", @"CO", @"CT", @"DE", @"FL", @"GA", @"HI", @"ID", @"IL", @"IN", @"IA", @"KS", @"KY", @"LA", @"ME", @"MD", @"MA", @"MI", @"MN", @"MS", @"MO", @"MT", @"NE", @"NV", @"NH", @"NJ", @"NM", @"NY", @"NC", @"ND", @"OH", @"OK", @"OR", @"PA", @"RI", @"SC", @"SD", @"TN", @"TX", @"UT", @"VT", @"VA", @"WA", @"WV", @"WI", @"WY"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    singleTap.numberOfTapsRequired = 1;
    [scroller addGestureRecognizer:singleTap];
    
    txtNickname.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtNickname.layer.borderWidth = 1.0f;
    
    txtStreet.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtStreet.layer.borderWidth = 1.0f;
    
    txtAptSuite.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtAptSuite.layer.borderWidth = 1.0f;
    
    txtCity.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtCity.layer.borderWidth = 1.0f;
    
    txtState.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtState.layer.borderWidth = 1.0f;
    
    txtZip.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtZip.layer.borderWidth = 1.0f;
    
    UIView *txtNicknamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtNickname.leftView = txtNicknamePadding;
    txtNickname.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtStreetPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtStreet.leftView = txtStreetPadding;
    txtStreet.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtAptSuitePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtAptSuite.leftView = txtAptSuitePadding;
    txtAptSuite.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtCityPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtCity.leftView = txtCityPadding;
    txtCity.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtStatePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtState.leftView = txtStatePadding;
    txtState.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtZipPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtZip.leftView = txtZipPadding;
    txtZip.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    if(modal)
    {
        self.title = @"LOCATION";
        //UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAddAddress)];
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAddAddress)];
        self.navigationItem.leftBarButtonItem = cancel;
        
        if(editMode){
            lblScreenTitle.text = @"EDIT LOCATION";
            txtNickname.text = [AddressToEdit objectForKey:@"nickname"];
            txtStreet.text = [AddressToEdit objectForKey:@"address1"];
            txtAptSuite.text = [AddressToEdit objectForKey:@"address2"];
            txtCity.text = [AddressToEdit objectForKey:@"city"];
            
            txtNickname.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblNickname = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblNickname.textColor = [UIColor lightGrayColor];
            lblNickname.font = [UIFont systemFontOfSize:12.0];
            lblNickname.text = @"Location Nickname";
            [txtNickname addSubview:lblNickname];
            
            txtStreet.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblStreet = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblStreet.textColor = [UIColor lightGrayColor];
            lblStreet.font = [UIFont systemFontOfSize:12.0];
            lblStreet.text = @"Street";
            [txtStreet addSubview:lblStreet];
            
            if(txtAptSuite.text.length == 1 && !lblAptSuite){
                txtAptSuite.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
                lblAptSuite = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
                lblAptSuite.textColor = [UIColor lightGrayColor];
                lblAptSuite.font = [UIFont systemFontOfSize:12.0];
                lblAptSuite.text = @"Apt / Suite";
                [txtAptSuite addSubview:lblAptSuite];
                
                UILabel *lblOptional = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
                lblOptional.textColor = [UIColor lightGrayColor];
                lblOptional.text = @"*optional";
                txtAptSuite.rightView = lblOptional;
                txtAptSuite.rightViewMode = UITextFieldViewModeAlways;
            }
            
            txtCity.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblCity = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblCity.textColor = [UIColor lightGrayColor];
            lblCity.font = [UIFont systemFontOfSize:12.0];
            lblCity.text = @"City";
            [txtCity addSubview:lblCity];
            
            /*txtState.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblState = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblState.textColor = [UIColor lightGrayColor];
            lblState.font = [UIFont systemFontOfSize:12.0];
            lblState.text = @"State";
            [txtState addSubview:lblState];*/
            
            txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblZip = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblZip.textColor = [UIColor lightGrayColor];
            lblZip.font = [UIFont systemFontOfSize:12.0];
            lblZip.text = @"Zip Code";
            [txtZip addSubview:lblZip];
            
            //////NSLog(@"state: %@",[AddressToEdit objectForKey:@"state"] );
            
            
            //txtState.text = [AddressToEdit objectForKey:@"state"];
            txtZip.text = [AddressToEdit objectForKey:@"zip"];
            nextView.backgroundColor = [UIColor blackColor];
        }
        else{
            lblScreenTitle.text = @"ADD LOCATION";
            
        }
    }
    else{
        self.title = @"LOCATION";
        
        //autopopulate address during registration Dec 1
        [self autoPopulateAddress];
        
        txtStreet.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblStreet = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
        lblStreet.textColor = [UIColor lightGrayColor];
        lblStreet.font = [UIFont systemFontOfSize:12.0];
        lblStreet.text = @"Street";
        [txtStreet addSubview:lblStreet];
        
        txtCity.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblCity = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
        lblCity.textColor = [UIColor lightGrayColor];
        lblCity.font = [UIFont systemFontOfSize:12.0];
        lblCity.text = @"City";
        [txtCity addSubview:lblCity];
        
        /*txtState.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
         lblState = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
         lblState.textColor = [UIColor lightGrayColor];
         lblState.font = [UIFont systemFontOfSize:12.0];
         lblState.text = @"State";
         [txtState addSubview:lblState];*/
        
        txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblZip = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
        lblZip.textColor = [UIColor lightGrayColor];
        lblZip.font = [UIFont systemFontOfSize:12.0];
        lblZip.text = @"Zip Code";
        [txtZip addSubview:lblZip];

    }
    
    txtState.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
    lblState = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
    lblState.textColor = [UIColor lightGrayColor];
    lblState.font = [UIFont systemFontOfSize:12.0];
    lblState.text = @"State";
    [txtState addSubview:lblState];

    
    
    //txtState.text = @"TX";
    
    /*txtNickname.text = @"Home";
    txtStreet.text = @"236 Meadow Rd";
    txtCity.text = @"Russellville";
    txtState.text = @"AR";
    txtZip.text = @"72802";
    [self canSubmit];*/
    //txtAptSuite.text = @"\u200B";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    //[self.navigationController.navigationBar setTranslucent:NO];
    
    statePickerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 230.0)];
    statePickerView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    [self.view addSubview:statePickerView];
    
    statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 50.0, statePickerView.frame.size.width, statePickerView.frame.size.height - 50.0)];
    statePicker.dataSource = self;
    statePicker.delegate = self;
    [statePickerView addSubview:statePicker];
    
    //[statePicker reloadAllComponents];
    
    btnChooseState = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChooseState.frame = CGRectMake(5.0, 3.0, statePickerView.frame.size.width - 10.0, 44.0);
    [btnChooseState setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnChooseState.backgroundColor = [UIColor blackColor];
    btnChooseState.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:16.0];
    [btnChooseState setTitle:@"CHOOSE STATE" forState:UIControlStateNormal];
    [btnChooseState addTarget:self action:@selector(chooseState:) forControlEvents:UIControlEventTouchUpInside];
    [statePickerView addSubview:btnChooseState];
    
    [stateValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *state = (NSString *)obj;
        if([state isEqualToString:[AddressToEdit objectForKey:@"state"]]){
            NSString *stateName = (NSString *)[stateNames objectAtIndex:idx];
            txtState.text = stateName;
            pickerSelectedIndex = idx;
            [statePicker selectRow:pickerSelectedIndex inComponent:0 animated:NO];
            *stop = YES;
        }
    }];
}

-(IBAction)chooseState:(id)sender{
    pickerShowing = NO;
    [UIView animateWithDuration:0.3 animations:^{
        statePickerView.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 230.0);
    } completion:^(BOOL finished) {
        [txtZip becomeFirstResponder];
    }];
}

-(void)cancelAddAddress{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)autoPopulateAddress{
    //During registration user can click on
    if(!editMode)
    {
        CLPlacemark *setLocation = [GPS sharedGPS].setLocation;
        
        /*CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        if(status== kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied)
        {
            NSString *title;
            title = (status == kCLAuthorizationStatusDenied) ? @"Location service are off" : @"Background location is not enabled";
            //UIAlertView
            
        }
        else if (status == kCLAuthorizationStatusNotDetermined)
        {
            
        }*/
        
        
        if(setLocation){
            txtZip.text =  setLocation.postalCode;
            txtCity.text = setLocation.locality;
            txtState.text = setLocation.administrativeArea;
            txtStreet.text = setLocation.thoroughfare;
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideKeyboard{
    [txtNickname resignFirstResponder];
    [txtStreet resignFirstResponder];
    [txtAptSuite resignFirstResponder];
    [txtCity resignFirstResponder];
    [txtState resignFirstResponder];
    [txtZip resignFirstResponder];
    [scroller setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
    //[scroller scrollRectToVisible:CGRectMake(0.0, 0.0, self.view.frame.size.width, 10.0) animated:YES];
    [self canSubmit];
}

-(void)changeStreetKeyboard:(UIKeyboardType)keyboardType{
    [txtStreet setKeyboardType:keyboardType];
    [txtStreet reloadInputViews];
    txtStreet.text = streetSoFar;
}

#pragma mark - UITextfield methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    textField.textColor = [UIColor blackColor];
    
    NSString *textString = @"";
    if(string.length > 0)
    {
        //typing
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) { // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
            return NO;
        }
        
        textString = [NSString stringWithFormat:@"%@%@", textField.text, string];
        //////NSLog(@"Full string: %@", textString);
    }
    else{
        //backspace
        textString = [textField.text substringToIndex:textField.text.length - 1];
        
        if([textField isEqual:txtStreet]){
            streetSoFar = textString;
            NSString *street = textString;
            NSString *expression = @"^[0-9\\ ]{1,}$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:street options:0 range:NSMakeRange(0, street.length)];
            if(range.location != NSNotFound)
            {
                //just numbers and a space
                [self changeStreetKeyboard:UIKeyboardTypeNumbersAndPunctuation];
                return NO;
                //txtStreet.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            }
        }
    }
    
    if ([textField isEqual:txtNickname]){
        if(textString.length == 1 && !lblNickname){
            txtNickname.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblNickname = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblNickname.textColor = [UIColor lightGrayColor];
            lblNickname.font = [UIFont systemFontOfSize:12.0];
            lblNickname.text = @"Location Nickname";
            [txtNickname addSubview:lblNickname];
        }
        else if (textString.length == 0 && lblNickname){
            [lblNickname removeFromSuperview];
            lblNickname = nil;
            txtNickname.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
    }
    else if ([textField isEqual:txtStreet]){
        streetSoFar = textString;
        if(textString.length == 1 && !lblStreet){
            txtStreet.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblStreet = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblStreet.textColor = [UIColor lightGrayColor];
            lblStreet.font = [UIFont systemFontOfSize:12.0];
            lblStreet.text = @"Street";
            [txtStreet addSubview:lblStreet];
        }
        else if (textString.length == 0 && lblStreet){
            [lblStreet removeFromSuperview];
            lblStreet = nil;
            txtStreet.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if ([textString hasSuffix:@" "]) {
            [self changeStreetKeyboard:UIKeyboardTypeDefault];
            return NO;
            //txtStreet.keyboardType = UIKeyboardTypeDefault;
        }
    }
    else if ([textField isEqual:txtAptSuite]){
        if(textString.length == 1 && !lblAptSuite){
            txtAptSuite.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblAptSuite = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblAptSuite.textColor = [UIColor lightGrayColor];
            lblAptSuite.font = [UIFont systemFontOfSize:12.0];
            lblAptSuite.text = @"Apt / Suite";
            [txtAptSuite addSubview:lblAptSuite];
            
            UILabel *lblOptional = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
            lblOptional.textColor = [UIColor lightGrayColor];
            lblOptional.text = @"*optional";
            txtAptSuite.rightView = lblOptional;
            txtAptSuite.rightViewMode = UITextFieldViewModeAlways;
        }
        else if (textString.length == 0 && lblAptSuite){
            [lblAptSuite removeFromSuperview];
            lblAptSuite = nil;
            txtAptSuite.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
            txtAptSuite.rightView = nil;
        }
    }
    else if ([textField isEqual:txtCity]){
        if(textString.length == 1 && !lblCity){
            txtCity.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblCity = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblCity.textColor = [UIColor lightGrayColor];
            lblCity.font = [UIFont systemFontOfSize:12.0];
            lblCity.text = @"City";
            [txtCity addSubview:lblCity];
        }
        else if (textString.length == 0 && lblCity){
            [lblCity removeFromSuperview];
            lblCity = nil;
            txtCity.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
    }
    else if ([textField isEqual:txtState]){
        if(textString.length == 1 && !lblState){
            txtState.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblState = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblState.textColor = [UIColor lightGrayColor];
            lblState.font = [UIFont systemFontOfSize:12.0];
            lblState.text = @"State";
            [txtState addSubview:lblState];
        }
        else if (textString.length == 0 && lblState){
            [lblState removeFromSuperview];
            lblState = nil;
            txtState.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
    }
    else if ([textField isEqual:txtZip]){
        if(textString.length == 1 && !lblZip){
            txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblZip = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblZip.textColor = [UIColor lightGrayColor];
            lblZip.font = [UIFont systemFontOfSize:12.0];
            lblZip.text = @"Zip Code";
            [txtZip addSubview:lblZip];
        }
        else if (textString.length == 0 && lblZip){
            [lblZip removeFromSuperview];
            lblZip = nil;
            txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(textString.length >= 6)
            return NO;
        
        if(textString.length == 5)
            [self performSelector:@selector(hideKeyboard) withObject:nil afterDelay:0.1];
    }
    
    
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtNickname])
        [txtStreet becomeFirstResponder];
    else if ([textField isEqual:txtStreet])
        [txtAptSuite becomeFirstResponder];
    else if ([textField isEqual:txtAptSuite])
        [txtCity becomeFirstResponder];
    else if ([textField isEqual:txtCity])
        [txtState becomeFirstResponder];
    else if ([textField isEqual:txtState])
        [txtZip becomeFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [scroller setContentOffset:CGPointMake(0, textField.frame.origin.y - 110.0) animated:YES];
    txtZipBottomMargin.constant = 216;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if([textField isEqual:txtState]){
        //show picker
        /*[txtNickname resignFirstResponder];
        [txtStreet resignFirstResponder];
        [txtAptSuite resignFirstResponder];
        [txtCity resignFirstResponder];
        [txtState resignFirstResponder];
        [txtZip resignFirstResponder];
        pickerShowing = YES;
        [UIView animateWithDuration:0.3 animations:^{
            statePickerView.frame = CGRectMake(0.0, self.view.frame.size.height - 230.0, self.view.frame.size.width, 230.0);
        } completion:^(BOOL finished) {
            
        }];*/
        [txtZip becomeFirstResponder];
        return NO;
    }
    else{
        if(pickerShowing)
        {
            [UIView animateWithDuration:0.3 animations:^{
                statePickerView.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 230.0);
            } completion:^(BOOL finished) {
                
            }];
        }
    }
    return YES;
}

-(BOOL)canSubmit{
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    UIColor *required = [UIColor redColor];
    BOOL shouldCheckRegex = YES;
    if(txtNickname.text.length < 1 || txtStreet.text.length < 1 || txtCity.text.length < 1 || txtState.text.length < 1 || txtZip.text.length < 5){
        shouldCheckRegex = NO;
        
        if(txtNickname.text.length == 0)
        {
            txtNickname.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtNickname.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        
        if(txtStreet.text.length == 0)
        {
            txtStreet.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtStreet.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        
        if(txtCity.text.length == 0)
        {
            txtCity.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtCity.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        
        if(txtState.text.length == 0)
        {
            txtState.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtState.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        
        if(txtZip.text.length == 0)
        {
            txtZip.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtZip.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
    }
    
    if(shouldCheckRegex){
        BOOL allowedToSubmit = YES;
        
        //check regex here
        if(allowedToSubmit){
            nextView.backgroundColor = [UIColor blackColor];
        }
        else{
            nextView.backgroundColor = [UIColor lightGrayColor];
        }
        
        return allowedToSubmit;
    }
    else{
        nextView.backgroundColor = [UIColor lightGrayColor];
        return NO;
    }

    
}
- (IBAction)searchZip:(id)sender {
    [self autoPopulateAddress];
}

-(IBAction)saveLocation:(id)sender{
    //////NSLog(@"Save Location");
    
    if([self canSubmit]){

        self->btSave.enabled = NO;

        if(!modal){
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:signUpInfo];
            [params setValue:stripeID forKey:@"stripeToken"];
            
            [params setValue:txtNickname.text.uppercaseString forKey:@"locationNickname"];
            [params setValue:txtStreet.text forKey:@"address1"];
            [params setValue:txtAptSuite.text forKey:@"address2"];
            [params setValue:txtCity.text forKey:@"city"];
            
            //NSString *abbr = [stateValues objectAtIndex:pickerSelectedIndex];
            [params setValue:@"TX" forKey:@"state"];
            
            [params setValue:txtZip.text forKey:@"zip"];
            
            ////NSLog(@"params: %@", params);
            
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showProcessingModal:@"Registering..."];
            
            [PFCloud callFunctionInBackground:@"customerRegistration" withParameters:params block:^(id object, NSError *error) {
                self->btSave.enabled = YES;

                if(error == nil){
                    ////NSLog(@"return obj: %@", object);
                    //good status code???
                    //upload image now (BASE64 ENCODED!)
                    id customer = [object objectForKey:@"customer"];
                    id user = [customer objectForKey:@"user"];
                    [User sharedUser].customerID = [customer valueForKey:@"objectId"];
                    [User sharedUser].objectID = [user valueForKey:@"objectId"];
                    [User sharedUser].email = [user objectForKey:@"email"];
                    [User sharedUser].firstName = [user objectForKey:@"firstName"];
                    [User sharedUser].lastName = [user objectForKey:@"lastName"];
                    [User sharedUser].phone = [user objectForKey:@"phone"];
                    [User sharedUser].username = [user objectForKey:@"username"];
                    [User sharedUser].dob = [signUpInfo objectForKey:@"dob"]; //yes, signupinfo because it is the proper string and not an NSDate
                    [[User sharedUser] save];
                    
                    [PFUser logInWithUsernameInBackground:[signUpInfo objectForKey:@"email"] password:[signUpInfo objectForKey:@"password"] block:^(PFUser *user, NSError *error) {
                        if(!error){
                            //NSLog(@"all good");
                        }
                        else{
                            //NSLog(@"error:%@", error);
                        }
                    }];
                    
                    //////NSLog(@"profile image: %@", profileImage);
                    
                    //////NSLog(@"customer id: %@", [customer valueForKey:@"objectId"]);
                    //////NSLog(@"user email: %@", [user objectForKey:@"email"]);
                    //NSString *customerID = [customer valueForKey:@"objectId"];
                    //NSString *email = [user objectForKey:@"email"];
                    //NSDictionary *avatarDict = @{@"id":customerID, @"type":@"customer", @"avatar":@{@"name":email, @"file":profileImage}};
                    //////NSLog(@"avatar dictionary: %@", avatarDict);
                    [self performSelector:@selector(uploadAvatar) withObject:nil];
                    
                }
                else{
                    //nserror should have content
                    ////NSLog(@"error: %@", error.description);
                    if([error.description containsString:@"User is not in a provider area"]){
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate hideModal];
                        [appDelegate showModal:@"We're sorry. We're not currently serving this area. More areas are coming soon."];
                    }
                    else{
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate hideModal];
                        [appDelegate showModal:@"Unable to complete registration at this time."];
                    }
                }
            }];
            //[self dismissViewControllerAnimated:YES completion:nil];
        }
        else if(!editMode){
            //adding a location
            //NSString *abbr = [stateValues objectAtIndex:pickerSelectedIndex];
            
            NSDictionary *params = @{@"customerId":[User sharedUser].customerID, @"nickname":txtNickname.text.uppercaseString, @"address1":txtStreet.text, @"address2":txtAptSuite.text, @"city":txtCity.text, @"state":@"TX", @"zip": txtZip.text};
            [PFCloud callFunctionInBackground:@"postLocation" withParameters:params block:^(id object, NSError *error) {
                self->btSave.enabled = YES;

                if(error == nil){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserLocations" object:nil]; //we only call this because it does everything we want
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else{
                    //nserror should have content
                    ////NSLog(@"error: %@", error.description);
                    if([error.description containsString:@"User is not in a provider area"]){
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate showModal:@"We're sorry. We're not currently serving this area. More areas are coming soon."];
                    }
                    else{
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate showModal:@"Unable to add a location at this time."];
                    }
                }
            }];
            /*PFObject *location = [PFObject objectWithClassName:@"Location"];
            location[@"nickname"] = txtNickname.text;
            location[@"address1"] = txtStreet.text;
            location[@"address2"] = txtAptSuite.text;
            location[@"city"] = txtCity.text;
            location[@"state"] = txtState.text;
            location[@"zip"] = txtZip.text;
            
            [location saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    // The object has been saved.
                } else {
                    // There was a problem, check error.description
                }
            }];*/
        }
        else if(editMode){
            //editing a location
            /*PFQuery *query = [PFQuery queryWithClassName:@"Location"];
            
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:[AddressToEdit objectForKey:@"objectId"] block:^(PFObject *location, NSError *error) {
                if(!error){
                    location[@"nickname"] = txtNickname.text.uppercaseString;
                    location[@"address1"] = txtStreet.text;
                    location[@"address2"] = txtAptSuite.text;
                    location[@"city"] = txtCity.text;
                    location[@"state"] = txtState.text;
                    location[@"zip"] = txtZip.text;
                    //[location saveInBackground];
                    [location saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if(succeeded){
                            //popup?
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerLoggedIn" object:nil]; //we only call this because it does everything we want
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                        }
                        else{
                            ////NSLog(@"error: %@", error);
                        }
                    }];
                }
                else{
                    ////NSLog(@"error: %@", error);
                }
                
            }];*/
            //////NSLog(@"Editing Address: %@", [AddressToEdit valueForKey:@"objectId"]);
            //NSString *abbr = [stateValues objectAtIndex:pickerSelectedIndex];
            
            NSDictionary *params = @{@"locationId":[AddressToEdit valueForKey:@"objectId"], @"nickname":txtNickname.text.uppercaseString, @"address1":txtStreet.text, @"address2":txtAptSuite.text, @"city":txtCity.text, @"state":@"TX", @"zip": txtZip.text};
            ////NSLog(@"Params: %@", params);
            [PFCloud callFunctionInBackground:@"putLocation" withParameters:params block:^(id object, NSError *error) {
                self->btSave.enabled = YES;

                if(error == nil){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserLocations" object:nil]; //we only call this because it does everything we want
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else{
                    //nserror should have content
                    ////NSLog(@"error: %@", error.description);
                    if([error.description containsString:@"User is not in a provider area"]){
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate showModal:@"We're sorry. We're not currently serving this area. More areas are coming soon."];
                    }
                    else{
                        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                        [appDelegate showModal:@"Unable to edit a location at this time."];
                    }
                }
            }];
        }
    }
}

-(void)uploadAvatar{
    if(profileImage)
    {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showProcessingModal:@"Uploading profile image..."];
        NSDictionary *avatarDict = @{@"id":[User sharedUser].customerID, @"type":@"customer", @"avatar":@{@"name":[User sharedUser].email, @"file":profileImage}};
        [PFCloud callFunctionInBackground:@"avatarUpload" withParameters:avatarDict block:^(id object, NSError *error) {
            if(error == nil){
                //good status code???
                //[self.navigationController popViewControllerAnimated:YES];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:@1 forKey:@"stayloggedin"];
                [defaults synchronize];
                [[User sharedUser] save];
                [appDelegate hideModal];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerLoggedIn" object:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else{
                //nserror should have content
                ////NSLog(@"error: %@", error.description);
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to upload your profile image."];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:@1 forKey:@"stayloggedin"];
                [defaults synchronize];
                [[User sharedUser] save];
                [appDelegate hideModal];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerLoggedIn" object:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    }
}

#pragma mark - UIPickerView methods

-(NSInteger) pickerView: (UIPickerView*) pickerView
numberOfRowsInComponent: (NSInteger) component {
    return stateNames.count;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

/*- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    if(pickerSelectedIndex == -1)
        return @"";
    txtState.text = (NSString *)[stateNames objectAtIndex:row];
    return (NSString*) [stateNames objectAtIndex: row];
}*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    pickerSelectedIndex = row;
    //txtState.text = (NSString *)[stateNames objectAtIndex:row];
    /*[UIView animateWithDuration:0.3 animations:^{
        statePickerView.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 180.0);
    } completion:^(BOOL finished) {
        
    }];*/
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //txtState.text = (NSString *)[stateNames objectAtIndex:row];
    NSString *title = (NSString*) [stateNames objectAtIndex: row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:16.0]  }];
    
    return attString;
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}

@end
