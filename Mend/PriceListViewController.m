//
//  PriceListViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/16/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PriceListViewController.h"

@interface PriceListViewController ()
{
    IBOutlet UITableView *priceTable;
    NSMutableArray *prices;
}
@end

@implementation PriceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [PFCloud callFunctionInBackground:@"getPricing" withParameters:[[NSMutableDictionary alloc] init] block:^(id objects, NSError *error) {
        if (error) {
            ////NSLog(@"The getObjects request failed.");
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to get prices at this time."];
        } else {
            // The find succeeded.
            ////NSLog(@"objects: %@", objects);
            prices = [[NSMutableArray alloc] initWithArray:objects];
            [priceTable reloadData];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Price List";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return prices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSInteger const kChoice = 100;
    static NSInteger const kPrice = 200;
    UILabel *lblChoice = nil;
    UILabel *lblPrice = nil;
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    lblChoice = (UILabel*)[cell.contentView viewWithTag:kChoice];
    lblPrice = (UILabel*)[cell.contentView viewWithTag:kPrice];
    
    id Option = [prices objectAtIndex:indexPath.row];
    
    lblChoice.text = [Option objectForKey:@"option"];
    lblPrice.text = [NSString stringWithFormat:@"$%@",[Option objectForKey:@"price"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
