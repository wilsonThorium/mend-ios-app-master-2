//
//  VisitConfirmationViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/20/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface VisitConfirmationViewController : GAITrackedViewController
@property (nonatomic, strong) id theAppointment;
@property (nonatomic, strong) NSString *countdownString;
@property (nonatomic, assign) BOOL willBeCharged;
@end
