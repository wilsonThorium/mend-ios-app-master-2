//
//  User.m
//  Soteria
//
//  Created by Andrew Goodwin on 11/8/14.
//  Copyright (c) 2014 joebell. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize objectID, username, email, firstName, lastName, phone, customerID, providerID, dob, selectedLocationID;

- (id) init {
    self = [super init];
    if (self != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults objectForKey:@"objectID"] != nil)
            self.objectID = [defaults objectForKey:@"objectID"];
        
        if([defaults objectForKey:@"username"] != nil)
            self.username = [defaults objectForKey:@"username"];
        
        if([defaults objectForKey:@"email"] != nil)
            self.email = [defaults objectForKey:@"email"];
        
        if([defaults objectForKey:@"firstName"] != nil)
            self.firstName = [defaults objectForKey:@"firstName"];
        
        if([defaults objectForKey:@"lastName"] != nil)
            self.lastName = [defaults objectForKey:@"lastName"];
        
        if([defaults objectForKey:@"phone"] != nil)
            self.phone = [defaults objectForKey:@"phone"];
        
        if([defaults objectForKey:@"customerID"] != nil)
            self.customerID = [defaults objectForKey:@"customerID"];
        
        if([defaults objectForKey:@"providerID"] != nil)
            self.providerID = [defaults objectForKey:@"providerID"];
        
        if([defaults objectForKey:@"dob"] != nil)
            self.dob = [defaults objectForKey:@"dob"];
        
        if([defaults objectForKey:@"selectedLocationID"] != nil)
            self.selectedLocationID = [defaults objectForKey:@"selectedLocationID"];
    }
    return self;
}

-(void)save{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(self.objectID != nil)
        [defaults setValue:self.objectID forKey:@"objectID"];
    else
        [defaults setValue:nil forKey:@"objectID"];        
    
    if(self.username != nil)
        [defaults setValue:self.username forKey:@"username"];
    else
        [defaults setValue:nil forKey:@"username"];
    
    if(self.email != nil)
        [defaults setValue:self.email forKey:@"email"];
    else
        [defaults setValue:nil forKey:@"email"];
    
    if(self.firstName != nil)
        [defaults setValue:self.firstName forKey:@"firstName"];
    else
        [defaults setValue:nil forKey:@"firstName"];
    
    if(self.lastName != nil)
        [defaults setValue:self.lastName forKey:@"lastName"];
    else
        [defaults setValue:nil forKey:@"lastName"];
    
    if(self.phone != nil)
        [defaults setValue:self.phone forKey:@"phone"];
    else
        [defaults setValue:nil forKey:@"phone"];
    
    if(self.customerID != nil)
        [defaults setValue:self.customerID forKey:@"customerID"];
    else
        [defaults setValue:nil forKey:@"customerID"];
    
    if(self.providerID != nil)
        [defaults setValue:self.providerID forKey:@"providerID"];
    else
        [defaults setValue:nil forKey:@"providerID"];
    
    if(self.dob != nil)
        [defaults setValue:self.dob forKey:@"dob"];
    else
        [defaults setValue:nil forKey:@"dob"];
    
    if(self.selectedLocationID != nil)
        [defaults setValue:self.selectedLocationID forKey:@"selectedLocationID"];
    else
        [defaults setValue:nil forKey:@"selectedLocationID"];
    
    [defaults synchronize];
}

-(void)logout{
    self.loggedIn = false;
    self.objectID = nil;
    self.username = nil;
    self.email = nil;
    self.firstName = nil;
    self.lastName = nil;
    self.phone = nil;
    self.customerID = nil;
    self.providerID = nil;
    self.dob = nil;
    self.selectedLocationID = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"stayloggedin"];
    [defaults synchronize];
    [self save];
    
    [[Appointment currentAppointment] cancel];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserLoggedOut" object:nil];
}

+(User *)sharedUser{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[User alloc] init];
    });
    return sharedInstance;
}
@end
