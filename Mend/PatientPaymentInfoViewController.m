//
//  PatientPaymentInfoViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientPaymentInfoViewController.h"
#import "PatientLocationIntermediateViewController.h"
#import "PatientLocationViewController.h"

@interface PatientPaymentInfoViewController ()
{
    IBOutlet NSLayoutConstraint *btnNextWidth;
    IBOutlet NSLayoutConstraint *btnNextPaddingLeft;
    IBOutlet UILabel *lblAddCard;
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnArrow;
    IBOutlet UITextField *txtNameCard;
    IBOutlet UITextField *txtCardNumber;    
    IBOutlet UITextField *txtExpirationDate;
    IBOutlet UITextField *txtCVV;
    IBOutlet UITextField *txtZip;
    IBOutlet UIScrollView *scroller;
    IBOutlet UIView *nextView;
    IBOutlet NSLayoutConstraint *txtZipBottomMargin;
    
    UILabel *lblNameCard;
    UILabel *lblCardNumber;
    UILabel *lblExpirationDate;
    UILabel *lblCVV;
    UILabel *lblZip;
    
    NSString *stripeID;
    
    NSString *segIdentifier;
    id segSender;
}
@end

@implementation PatientPaymentInfoViewController
@synthesize signUpInfo, profileImage, editMode;

- (void)viewDidLoad {
    [super viewDidLoad];
    stripeID = @"";
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    singleTap.numberOfTapsRequired = 1;
    [scroller addGestureRecognizer:singleTap];
    
    txtNameCard.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtNameCard.layer.borderWidth = 1.0f;
    
    txtCardNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtCardNumber.layer.borderWidth = 1.0f;
    UIImageView *lock = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 30.0, 15.0)];
    lock.image = [UIImage imageNamed:@"icon_lock"];
    lock.contentMode = UIViewContentModeScaleAspectFit;
    txtCardNumber.rightView = lock;
    txtCardNumber.rightViewMode = UITextFieldViewModeAlways;
    
    txtExpirationDate.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtExpirationDate.layer.borderWidth = 1.0f;
    txtExpirationDate.font = [UIFont systemFontOfSize:14];
    
    txtCVV.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtCVV.layer.borderWidth = 1.0f;
    
    txtZip.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtZip.layer.borderWidth = 1.0f;
    
    UIView *txtNameCardPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtNameCard.leftView = txtNameCardPadding;
    txtNameCard.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtCardNumberPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtCardNumber.leftView = txtCardNumberPadding;
    txtCardNumber.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtExpirationDatePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtExpirationDate.leftView = txtExpirationDatePadding;
    txtExpirationDate.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtCVVPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtCVV.leftView = txtCVVPadding;
    txtCVV.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtZipPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtZip.leftView = txtZipPadding;
    txtZip.leftViewMode = UITextFieldViewModeAlways;
    
    if(editMode)
    {
        self.title = @"ADD PAYMENT";
        nextView.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0];
        btnNextWidth.constant = 100;
        [btnNext setTitle:@"ADD CARD" forState:UIControlStateNormal];
        btnNextPaddingLeft.constant = self.view.frame.size.width / 2 - btnNext.frame.size.width / 2 - 30;
        btnArrow.hidden = YES;
        lblAddCard.text = @"ADD NEW CARD";
        
    }
    else{
        self.title = @"PAYMENT";
        nextView.backgroundColor = [UIColor lightGrayColor];
        btnNextWidth.constant = 40;
        [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
        btnNextPaddingLeft.constant = 23;
        btnArrow.hidden = NO;
        lblAddCard.text = @"ADD CARD INFO";
        
    }
    
    /*txtCardNumber.text = @"4242 4242 4242 4242";
    txtNameCard.text = @"Andrew G Goodwin";
    txtCVV.text = @"719";
    txtExpirationDate.text = @"05 / 15";
    txtZip.text = @"72802";
    [self canSubmit];*/
    
    UITapGestureRecognizer *nextTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToNext)];
    nextTap.numberOfTapsRequired = 1;
    [nextView addGestureRecognizer:nextTap];
}

-(void)goToNext{
    if([self canSubmit]){
        if(!editMode)
        {
            if([stripeID isEqualToString:@""]){
                [self performSelectorInBackground:@selector(createCard) withObject:nil];
            }
            else{
                [self performSegueWithIdentifier:@"segueShowLocation" sender:self];
            }
        }
        else{
            if([stripeID isEqualToString:@""]){
                [self performSelectorInBackground:@selector(createCard) withObject:nil];
            }
            else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
    }
}

-(void)hideKeyboard{
    [txtNameCard resignFirstResponder];
    [txtCardNumber resignFirstResponder];
    [txtExpirationDate resignFirstResponder];
    [txtCVV resignFirstResponder];
    [txtZip resignFirstResponder];
    [scroller scrollRectToVisible:CGRectMake(0.0, 0.0, self.view.frame.size.width, 100.0) animated:YES];
    [self canSubmit];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Patient Payment Info";
    //scroller.contentSize = CGSizeMake(self.view.frame.size.width, 608.0);
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Formatter methods
-(void)selectTextForInput:(UITextField *)input atRange:(NSRange)range {
    UITextPosition *start = [input positionFromPosition:[input beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [input positionFromPosition:start
                                               offset:range.length];
    [input setSelectedTextRange:[input textRangeFromPosition:start toPosition:end]];
}
-(void)setCreditCardExpirationMask:(UITextField *)textField{
    textField.textColor = [UIColor lightGrayColor];
    
    if (textField.text.length > 0){
        BOOL foundLetter = NO;
        int charIndex = -1;
        for (NSInteger charIdx=0; charIdx<=6; charIdx++){
            if(!foundLetter){
                NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
                NSString *expression = @"^[MY]$";
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
                NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
                if(range.location != NSNotFound)
                {
                    //it is a letter
                    charIndex = charIdx;
                    foundLetter = YES;
                }
            }
        }
        if(!foundLetter){
            //it's all numbers (see if it's a good date)
            NSString *firstChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
            NSString *secondChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:1]];
            NSString *thirdChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:5]];
            NSString *fourthChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:6]];
            
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@%@ / %@%@", firstChar, secondChar, thirdChar, fourthChar]];
            
            
            //check to see if date is good
            NSString *monthPart = [textField.attributedText.string substringToIndex:2];
            NSString *yearPart = [textField.attributedText.string substringFromIndex:5];
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
            
            if(yearPart.intValue + 2000 >= components.year){
                //good year so far
                
                if(yearPart.intValue + 2000 == components.year){
                    
                    //check the month to see if it's this month or later
                    if(monthPart.intValue >= components.month){
                        //good credit card expiration date
                        [text addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor blackColor]
                                     range:NSMakeRange(0, 7)];
                        
                        textField.attributedText = text;
                    }
                    else{
                        [text addAttribute:NSForegroundColorAttributeName
                                     value:[UIColor redColor]
                                     range:NSMakeRange(0, 7)];
                        
                        textField.attributedText = text;
                    }
                }
                else{
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, 7)];
                    
                    textField.attributedText = text;
                }
            }
            else{
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(0, 7)];
                
                textField.attributedText = text;
            }
            /*NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString:textField.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex)];
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex, 0)];*/
        }
        else{
            //found a letter
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString:textField.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex)];
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex, 0)];
        }
        
        
    }
    else{
        
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"MM / YY"]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor lightGrayColor]
                     range:NSMakeRange(0, 7)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(0, 0)];
    }
}

-(void)removeCreditCardExpirationMask:(UITextField *)textField{
    if (textField.text.length > 0){
        NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
        if([thisChar isEqualToString:@"M"])
            textField.text = @"";
    }
}
-(NSNumber *)creditCardExpirationHandleBackspace:(UITextField *)textField inputString:(NSString *)string resultingString:(NSString *)textString{
    
    /*   0 - NO
     1 - YES
     2 - YES with error
     -1 - Passthrough (don't return yet)
     -2 - NO with error
     */
    
    NSNumber *retVal = @-1;
    
    BOOL foundLetter = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<=6; charIdx++){
        if(!foundLetter){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[MY]$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a letter
                charIndex = charIdx;
                foundLetter = YES;
            }
        }
    }
    
    if(charIndex == 0 || charIndex == 1)
    {
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"MM / YY"]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 0)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(0, 0)];
        
        return @0;
    }
    else if(charIndex == 5)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@M / YY", firstChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(1, 0)];
        return @0;
    }
    else if(charIndex == 6)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / YY", firstChar, secondChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 2)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(2, 0)];
        return @0;
    }
    else if(charIndex == -1)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@Y", firstChar, secondChar, thirdChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 6)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(6, 0)];
        return @0;
    }
    
    return retVal;
}
-(NSNumber *)creditCardExpirationHandleTyping:(UITextField *)textField inputString:(NSString *)string resultingString:(NSString *)textString{
    /*   0 - NO
     1 - YES
     2 - NO with good date
     3 - YES with good date
     -1 - Passthrough (don't return yet)
     -2 - NO with error
     -3 - YES with error
     */
    
    NSString *lastChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:6]];
    if(textString.length == 8 && ![lastChar isEqualToString:@"Y"])
        return @0;
    
    NSNumber *retVal = @-1;
    
    BOOL foundLetter = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<textField.text.length; charIdx++){
        if(!foundLetter){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[MY]$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a letter
                charIndex = charIdx;
                foundLetter = YES;
            }
        }
    }
    
    if(charIndex == 0)
    {
        //the first digit is being entered
        
        if(string.intValue > 1){
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"0%@ / YY", string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 5)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 5, 0)];
        }
        else{
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@M / YY", string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 1)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        }
        
        
        
        return @0;
    }
    else if(charIndex == 1)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / YY", firstChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 4)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 4, 0)];
        return @0;
    }
    else if(charIndex == 5)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@Y", firstChar, secondChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        return @0;
    }
    else if(charIndex == 6)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@", firstChar, secondChar, thirdChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        
        //check to see if date is good
        NSString *monthPart = [textField.attributedText.string substringToIndex:2];
        NSString *yearPart = [textField.attributedText.string substringFromIndex:5];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        if(yearPart.intValue + 2000 >= components.year){
            //good year so far
            
            if(yearPart.intValue + 2000 == components.year){
                
                //check the month to see if it's this month or later
                if(monthPart.intValue >= components.month){
                    //good credit card expiration date
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, 7)];
                    
                    textField.attributedText = text;
                    return @2;
                }
                else{
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange(0, charIndex + 1)];
                    
                    textField.attributedText = text;
                    return @-2;
                }
            }
            else{
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor blackColor]
                             range:NSMakeRange(0, 7)];
                
                textField.attributedText = text;
                return @2;
            }
        }
        else{
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor redColor]
                         range:NSMakeRange(0, charIndex + 1)];
            
            textField.attributedText = text;
            return @-2;
        }
        
        return @0;
    }
    
    return retVal;
}
-(BOOL)hasAttemptedExpirationDate:(UITextField *)textField{
    BOOL foundNumber = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<=6; charIdx++){
        if(!foundNumber){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[0-9]";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a number
                charIndex = charIdx;
                foundNumber = YES;
            }
        }
    }
    
    return foundNumber;
}

#pragma mark - UITextfield methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(![textField isEqual:txtExpirationDate])
        textField.textColor = [UIColor blackColor];
    
    NSString *textString = @"";
    if(string.length > 0)
    {
        //typing
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) { // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
            return NO;
        }
        
        textString = [NSString stringWithFormat:@"%@%@", textField.text, string];
        //////NSLog(@"Full string: %@", textString);
    }
    else{
        //backspace
        textString = [textField.text substringToIndex:textField.text.length - 1];
        //////NSLog(@"Full string: %@", textString);
        
        if([textField isEqual:txtExpirationDate]){
            NSNumber *retVal = [self creditCardExpirationHandleBackspace:textField inputString:string resultingString:textString];
            if(retVal.intValue != -1)
            {
                if(retVal.intValue == 1 || retVal.intValue == 0)
                    return retVal.boolValue;
                else if (retVal.intValue == -2){
                    //handle incorrect date here
                    return NO;
                }
                else if (retVal.intValue == -3)
                    //handle incorrect date here
                    return YES;
                else if (retVal.intValue == 2)
                {
                    //good date
                    
                    return NO;
                }
            }
        }
        
        if([textField isEqual:txtCardNumber]){
            if(textString.length == 5 || textString.length == 10 || textString.length == 15){
                textField.text = [textField.text substringToIndex:textField.text.length - 1];
            }
        }
    }
    
    if([textField isEqual:txtNameCard]){
        if(textString.length == 1 && !lblNameCard){
            txtNameCard.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblNameCard = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            lblNameCard.textColor = [UIColor lightGrayColor];
            lblNameCard.font = [UIFont systemFontOfSize:12.0];
            lblNameCard.text = @"Name on Card";
            [txtNameCard addSubview:lblNameCard];
        }
        else if (textString.length == 0 && lblNameCard){
            [lblNameCard removeFromSuperview];
            lblNameCard = nil;
            txtNameCard.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        NSString *notAName = @"[0-9\\*\\&\\^\\%\\$\\#\\@\\!\\+\\=\\_\\(\\)\\:\\;\\/\\,\\?\\!]";
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:notAName options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
        if(range.location != NSNotFound)
        {
            //an illegal character was found
            return NO;
        }
        
        if(textString.length > 0){
            lblNameCard.textColor = [UIColor lightGrayColor];
        }
    }
    else if ([textField isEqual:txtCardNumber]){
        if(textString.length == 1 && !lblCardNumber){
            txtCardNumber.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblCardNumber = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            lblCardNumber.textColor = [UIColor lightGrayColor];
            lblCardNumber.font = [UIFont systemFontOfSize:12.0];
            lblCardNumber.text = @"Card Number";
            [txtCardNumber addSubview:lblCardNumber];
        }
        else if (textString.length == 0 && lblCardNumber){
            [lblCardNumber removeFromSuperview];
            lblCardNumber = nil;
            txtCardNumber.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(textString.length >= 20)
            return NO;
        
        if(([textString hasPrefix:@"3"] && textString.length == 18) || (![textString hasPrefix:@"3"] &&textString.length == 19))
        {
            //check for valid card
            NSMutableString *ccNum = [[NSMutableString alloc] initWithString:textString];
            if([ccNum containsString:@" "])
                [ccNum replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, ccNum.length)];
            NSString *expression = @"^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:ccNum options:0 range:NSMakeRange(0, ccNum.length)];
            if(range.location != NSNotFound)
            {
                
                if([self luhnCheck:ccNum])
                {
                    //good card
                    lblCardNumber.textColor = [UIColor lightGrayColor];
                    [self performSelector:@selector(focusExpirationDate) withObject:nil afterDelay:0.1];
                }
                else
                {
                    //invalid card
                    lblCardNumber.textColor = [UIColor redColor];
                    txtCardNumber.textColor = [UIColor redColor];
                }
            }
            else
            {
                //invalid card
                lblCardNumber.textColor = [UIColor redColor];
                txtCardNumber.textColor = [UIColor redColor];
            }
        }
        
        if(textString.length == 1 && ![textString hasPrefix:@"3"] && ![textString hasPrefix:@"4"] && ![textString hasPrefix:@"5"] && ![textString hasPrefix:@"34"] && ![textString hasPrefix:@"37"] && ![textString hasPrefix:@"6"]){
            //don't allow them to type an invalid card number
            return NO;
        }
        
        if(textString.length == 2 && [textString hasPrefix:@"3"] && ![textString hasPrefix:@"34"] && ![textString hasPrefix:@"37"]){
            //don't allow them to type an invalid card number
            return NO;
        }
        
        
        if(textString.length == 4 || textString.length == 9 || textString.length == 14){
            NSMutableString *cardNum = [[NSMutableString alloc] initWithString:textString];
            [cardNum insertString:@" " atIndex:textString.length];
            textField.text = cardNum;
            return NO;
        }
    }
    else if ([textField isEqual:txtExpirationDate]){
        
        //NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        
        
        /*else if (textString.length == 0 && lblExpirationDate){
            [lblExpirationDate removeFromSuperview];
            lblExpirationDate = nil;
            txtExpirationDate.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }*/
        
        NSNumber *retVal = [self creditCardExpirationHandleTyping:textField inputString:string resultingString:textString];
        if(retVal.intValue != -1)
        {
            if(retVal.intValue == 1 || retVal.intValue == 0){
                return retVal.boolValue;
            }
            else if (retVal.intValue == -2){
                //handle incorrect date here
                lblExpirationDate.textColor = [UIColor redColor];
                return NO;
            }
            else if (retVal.intValue == -3){
                //handle incorrect date here
                lblExpirationDate.textColor = [UIColor redColor];
                return YES;
            }
            else if (retVal.intValue == 2)
            {
                //good date
                lblExpirationDate.textColor = [UIColor lightGrayColor];
                [self performSelector:@selector(focusCVV) withObject:nil afterDelay:0.1];
                return NO;
            }
        }
        
    }
    else if ([textField isEqual:txtCVV]){
        if(textString.length == 1 && !lblCVV){
            txtCVV.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblCVV = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            lblCVV.textColor = [UIColor lightGrayColor];
            lblCVV.font = [UIFont systemFontOfSize:12.0];
            lblCVV.text = @"CVV";
            [txtCVV addSubview:lblCVV];
        }
        else if (textString.length == 0 && lblCVV){
            [lblCVV removeFromSuperview];
            lblCVV = nil;
            txtCVV.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(([txtCardNumber.text hasPrefix:@"34"] || [txtCardNumber.text hasPrefix:@"37"]) && textString.length >= 5) //AMEX
            return NO;
        
        if(([txtCardNumber.text hasPrefix:@"34"] || [txtCardNumber.text hasPrefix:@"37"]) && textString.length == 4) //AMEX
            [self performSelector:@selector(focusZip) withObject:nil afterDelay:0.1];
        
        if(([txtCardNumber.text hasPrefix:@"4"] || [txtCardNumber.text hasPrefix:@"5"] || [txtCardNumber.text hasPrefix:@"6"]) && textString.length >= 4)
            return NO;
        
        if(([txtCardNumber.text hasPrefix:@"4"] || [txtCardNumber.text hasPrefix:@"5"] || [txtCardNumber.text hasPrefix:@"6"]) && textString.length == 3)
            [self performSelector:@selector(focusZip) withObject:nil afterDelay:0.1];
    }
    else if ([textField isEqual:txtZip]){
        if(textString.length == 1 && !lblZip){
            txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblZip = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            lblZip.textColor = [UIColor lightGrayColor];
            lblZip.font = [UIFont systemFontOfSize:12.0];
            lblZip.text = @"Zip Code";
            [txtZip addSubview:lblZip];
        }
        else if (textString.length == 0 && lblZip){
            [lblZip removeFromSuperview];
            lblZip = nil;
            txtZip.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(textString.length >= 6)
            return NO;
        
        if(textString.length == 5)
            [self performSelector:@selector(hideKeyboard) withObject:nil afterDelay:0.1];
    }
    
    return YES;
}

-(void)focusExpirationDate{
    [txtExpirationDate becomeFirstResponder];
}

-(void)focusCVV{
    [txtCVV becomeFirstResponder];
}

-(void)focusZip{
    [txtZip becomeFirstResponder];
}

-(BOOL)luhnCheck:(NSString *)stringToTest {
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:stringToTest.length];
    for (int i=0; i < stringToTest.length; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [stringToTest characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = [stringToTest length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[characters objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    
    return ((oddSum + evenSum) % 10 == 0);
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [scroller setContentOffset:CGPointMake(0, textField.frame.origin.y - 110.0) animated:YES];
    txtZipBottomMargin.constant = 216;
    if([textField isEqual:txtExpirationDate]){
        if(!lblExpirationDate){
            txtExpirationDate.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblExpirationDate = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            lblExpirationDate.textColor = [UIColor lightGrayColor];
            lblExpirationDate.font = [UIFont systemFontOfSize:12.0];
            lblExpirationDate.text = @"Expiration Date";
            [txtExpirationDate addSubview:lblExpirationDate];
        }
        [self setCreditCardExpirationMask:textField];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField isEqual:txtExpirationDate]){
        if (![self hasAttemptedExpirationDate:textField] && lblExpirationDate){
            [lblExpirationDate removeFromSuperview];
            lblExpirationDate = nil;
            txtExpirationDate.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        [self removeCreditCardExpirationMask:textField];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtNameCard])
        [txtCardNumber becomeFirstResponder];
    else if([textField isEqual:txtCardNumber])
        [txtExpirationDate becomeFirstResponder];
    else if([textField isEqual:txtExpirationDate])
        [txtCVV becomeFirstResponder];
    else if([textField isEqual:txtCVV])
        [txtZip becomeFirstResponder];
    else if([textField isEqual:txtZip]){
        [txtZip resignFirstResponder];
        [self hideKeyboard];
    }
    return YES;
}

-(BOOL)canSubmit{
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    UIColor *required = [UIColor redColor];
    
    BOOL shouldCheckRegex = YES;
    if(txtNameCard.text.length < 1){
        shouldCheckRegex = NO;
        txtNameCard.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtNameCard.placeholder attributes:@{NSForegroundColorAttributeName:required}];
    }
    if(([txtCardNumber.text hasPrefix:@"3"] && txtCardNumber.text.length < 18) || (![txtCardNumber.text hasPrefix:@"3"] && txtCardNumber.text.length < 19))
    {
        shouldCheckRegex = NO;
        if(txtCardNumber.text.length == 0){
            txtCardNumber.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtCardNumber.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblCardNumber.textColor = required;
            txtCardNumber.textColor = required;
        }
    }
        
    if(txtExpirationDate.text.length < 7 || [txtExpirationDate.text hasSuffix:@"Y"]){
        shouldCheckRegex = NO;
        
        if(txtExpirationDate.text.length == 0)
        {
            txtExpirationDate.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblExpirationDate.textColor = required;
            txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:required}];
        }
    }
    if(txtCVV.text.length < 3){
        shouldCheckRegex = NO;
        if (txtCVV.text.length == 0) {
            txtCVV.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtCVV.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblCVV.textColor = required;
            txtCVV.textColor = required;
        }
    }
    if(txtZip.text.length < 5){
        shouldCheckRegex = NO;
        if(txtZip.text.length == 0){
            txtZip.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtZip.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblZip.textColor = required;
            txtZip.textColor = required;
        }
    }
    
    if(shouldCheckRegex){
        BOOL allowedToSubmit = YES;
        
        //check regex here
        //checking card
        NSMutableString *ccNum = [[NSMutableString alloc] initWithString:txtCardNumber.text];
        if([ccNum containsString:@" "])
            [ccNum replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, ccNum.length)];
        NSString *expression = @"^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:ccNum options:0 range:NSMakeRange(0, ccNum.length)];
        if(range.location != NSNotFound)
        {
            
            if([self luhnCheck:ccNum])
            {
                //good card
                lblCardNumber.textColor = [UIColor lightGrayColor];
                txtCardNumber.textColor = [UIColor blackColor];
            }
            else
            {
                //invalid card
                txtCardNumber.textColor = [UIColor redColor];
                lblCardNumber.textColor = [UIColor redColor];
                allowedToSubmit = NO;
            }
        }
        else
        {
            //invalid card
            txtCardNumber.textColor = [UIColor redColor];
            lblCardNumber.textColor = [UIColor redColor];
            allowedToSubmit = NO;
        }
        
        //checking expiration date
        NSString *firstChar = [NSString stringWithFormat:@"%c",[txtExpirationDate.text characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[txtExpirationDate.text characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[txtExpirationDate.text characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[txtExpirationDate.text characterAtIndex:6]];
        
        //check to see if date is good
        NSString *monthPart = [NSString stringWithFormat:@"%@%@", firstChar, secondChar];
        NSString *yearPart = [NSString stringWithFormat:@"%@%@", thirdChar, fourthChar];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        if(yearPart.intValue + 2000 >= components.year){
            //good year so far
            
            if(yearPart.intValue + 2000 == components.year){
                
                //check the month to see if it's this month or later
                if(monthPart.intValue >= components.month){
                    //good credit card expiration date
                    lblExpirationDate.textColor = [UIColor lightGrayColor];
                    txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                }
                else{
                    lblExpirationDate.textColor = [UIColor redColor];
                    txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}];
                    allowedToSubmit = NO;
                }
            }
            else{
                lblExpirationDate.textColor = [UIColor lightGrayColor];
                txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
            }
        }
        else{
            lblExpirationDate.textColor = [UIColor redColor];
            txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}];
            allowedToSubmit = NO;
        }
        
        //checking CVV
        if(([txtCardNumber.text hasPrefix:@"34"] || [txtCardNumber.text hasPrefix:@"37"]) && txtCVV.text.length != 4){ //AMEX
            allowedToSubmit = NO;
            lblCVV.textColor = [UIColor redColor];
            txtCVV.textColor = [UIColor redColor];
        }
        else{
            lblCVV.textColor = [UIColor lightGrayColor];
            txtCVV.textColor = [UIColor blackColor];
        }
        
        if(([txtCardNumber.text hasPrefix:@"4"] || [txtCardNumber.text hasPrefix:@"5"] || [txtCardNumber.text hasPrefix:@"6"]) && txtCVV.text.length != 3){
            allowedToSubmit = NO;
            lblCVV.textColor = [UIColor redColor];
            txtCVV.textColor = [UIColor redColor];
        }
        else{
            lblCVV.textColor = [UIColor lightGrayColor];
            txtCVV.textColor = [UIColor blackColor];
        }
        
        if(allowedToSubmit){
            lblNameCard.textColor = [UIColor lightGrayColor];
            lblCardNumber.textColor = [UIColor lightGrayColor];
            lblExpirationDate.textColor = [UIColor lightGrayColor];
            lblCVV.textColor = [UIColor lightGrayColor];
            lblZip.textColor = [UIColor lightGrayColor];
            
            txtCardNumber.textColor = [UIColor blackColor];
            txtNameCard.textColor = [UIColor blackColor];
            txtExpirationDate.attributedText = [[NSMutableAttributedString alloc] initWithString:txtExpirationDate.text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
            txtCVV.textColor = [UIColor blackColor];
            txtZip.textColor = [UIColor blackColor];
            
            nextView.backgroundColor = [UIColor blackColor];
        }
        else{
            nextView.backgroundColor = [UIColor lightGrayColor];
        }
        
        return allowedToSubmit;
    }
    else{
        nextView.backgroundColor = [UIColor lightGrayColor];
        return NO;
    }
    
    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientLocationViewController class]]){
        PatientLocationViewController *vc = (PatientLocationViewController *)segue.destinationViewController;
        vc.signUpInfo = signUpInfo;
        vc.profileImage = profileImage;
        vc.stripeID = stripeID;
    }
}

-(void)createCard{
    STPCard *card = [[STPCard alloc] init];
    card.number = txtCardNumber.text;
    card.expMonth = [[[txtExpirationDate.text componentsSeparatedByString:@" / "] objectAtIndex:0] integerValue];
    card.expYear = [[[txtExpirationDate.text componentsSeparatedByString:@" / "] objectAtIndex:1] integerValue];
    card.cvc = txtCVV.text;
    //STPAPIClient *client = [[STPAPIClient alloc] initWithPublishableKey:@"pk_live_UvuA3GZX8fVe8Ex8ZblOqykN"];
    STPAPIClient *client = [[STPAPIClient alloc] initWithPublishableKey: @"pk_test_Dbs60g1iciywgNtD25mbJgPf"];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate showProcessingModal:@"Securely saving card..."];
    
    [client createTokenWithCard:card completion:^(STPToken *token, NSError *error) {
        if (error) {
            //[self handleError:error];
            //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to save this credit card."];
            
        } else {
            //[self createBackendChargeWithToken:token];
            [appDelegate hideModal];
            stripeID = token.tokenId;
            if (!editMode) {
                [self performSegueWithIdentifier:@"segueShowLocation" sender:self];
            }
            else{
                //grab stripe customer id and change default card
                PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
                [custQuery getObjectInBackgroundWithId:[User sharedUser].customerID block:^(PFObject *customer, NSError *error2) {
                    if(!error2){
                        NSString *stripeCustID = customer[@"stripeCustomerId"];
                        
                        NSDictionary *params = @{@"stripeCustomerId":stripeCustID, @"stripeToken":stripeID};
                        
                        [PFCloud callFunctionInBackground:@"postStripeCard" withParameters:params block:^(id object, NSError *error) {
                            if(error == nil){
                                
                                NSError *jsonError;
                                NSData *objectData = [object dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                     options:NSJSONReadingMutableContainers
                                                                                       error:&jsonError];
                                
                                NSString *cardID = [json valueForKey:@"id"];
                                
                                NSDictionary *cardParams = @{@"stripeCustomerId":stripeCustID,@"data":@{@"default_card":cardID}};
                                
                                [PFCloud callFunctionInBackground:@"putStripeCustomer" withParameters:cardParams block:^(id object2, NSError *error2) {
                                    if(error2 == nil){
                                        //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                        [appDelegate showModalWithSuccess:@"Credit card has been added."];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    else{
                                        //nserror should have content
                                        ////NSLog(@"error: %@", error.description);
                                        
                                        //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                        [appDelegate showModal:@"Unable to set new card as default at this time."];
                                    }
                                }];
                            }
                            else{
                                //nserror should have content
                                ////NSLog(@"error: %@", error.description);
                                
                                //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                [appDelegate showModal:@"Unable to add a new card at this time."];
                            }
                        }];
                        
                    }
                    else{
                        ////NSLog(@"error: %@", error2);
                    }
                    
                }];
                
                
                
                
            }
            
        }
    }];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    //return [self canSubmit];
    //segIdentifier = identifier;
    //segSender = sender;
    if([self canSubmit])
    {
        //call stripe here or call it when the card info is finally entered
        if([stripeID isEqualToString:@""]){
            [self performSelectorInBackground:@selector(createCard) withObject:nil];
            return NO;
        }
        else{
            return YES;
        }
        
        
    }
    else{
        return NO;
    }
    /*[PFCloud callFunctionInBackground:@"customerRegistration" withParameters:@{@"key":@"value"} block:^(id object, NSError *error) {
        
    }];*/
    return YES;
}

@end
