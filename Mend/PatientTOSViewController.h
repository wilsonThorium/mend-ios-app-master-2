//
//  PatientTOSViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PatientTOSViewController : GAITrackedViewController <UIWebViewDelegate>
-(IBAction)unwindToTOS:(UIStoryboardSegue *)unwindSegue;
@end
