//
//  PatientDashboardViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 12/21/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GAITrackedViewController.h"

@interface PatientDashboardViewController : GAITrackedViewController <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@end
