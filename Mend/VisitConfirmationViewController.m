//
//  VisitConfirmationViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/20/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "VisitConfirmationViewController.h"

@interface VisitConfirmationViewController ()
{
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIView *patientBG;
    IBOutlet UIView *calendarBG;
    IBOutlet UILabel *lblDayTime;
    IBOutlet UILabel *lblAddress1;
    IBOutlet UILabel *lblAddress2;
    IBOutlet UILabel *lblCountdown;
    IBOutlet UILabel *lblPhysician;
    IBOutlet UILabel *lblPhysicianTitle;
    
    UIView *blackout;
    UIView *deleteConfirmation;
    
    IBOutlet NSLayoutConstraint *calendarLeft;
    IBOutlet NSLayoutConstraint *calendarCenter;
    IBOutlet NSLayoutConstraint *redHeightConstraint;
    IBOutlet NSLayoutConstraint *heightConstraint;
    IBOutlet NSLayoutConstraint *widthConstraint;
    UIView *chargeForCancelView;
    IBOutlet NSLayoutConstraint *topConstraint;
}
@end

@implementation VisitConfirmationViewController
@synthesize theAppointment, countdownString, willBeCharged;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Visit Confirmation";
    willBeCharged = NO;
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.clipsToBounds = YES;
    
    patientBG.layer.cornerRadius = patientBG.frame.size.width/2.25;
    patientBG.clipsToBounds = YES;
    // Do any additional setup after loading the view.
    
    calendarBG.layer.cornerRadius = calendarBG.frame.size.width/2;
    calendarBG.layer.borderColor = [UIColor whiteColor].CGColor;
    calendarBG.layer.borderWidth = 1.0f;
    
    lblAddress1.text = [Appointment currentAppointment].address1;
    
    lblAddress2.text = [Appointment currentAppointment].address2;
    
    lblDayTime.text = [NSString stringWithFormat:@"%@ AT %@", [Appointment currentAppointment].day, [Appointment currentAppointment].time];
    
    lblCountdown.text = countdownString;
    if([lblCountdown.text isEqualToString:@"Less than a minute remaining to cancel for free."])
    {
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Less than a minute remaining to cancel for free."];
        [attrString addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"Avenir-Book" size:12.0]
                           range:NSMakeRange(0, attrString.length - 1)];
        lblCountdown.attributedText = attrString;
    }
    else{
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:countdownString];
        [attrString addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:@"Avenir-Book" size:12.0]
                           range:NSMakeRange(0, 2)];
        lblCountdown.attributedText = attrString;
    }
    
    //id Customer = [theAppointment objectForKey:@"customer"];
    
    lblPhysicianTitle.text = [Appointment currentAppointment].physicianTitle;
    
    //query for user based on [Provider objectForKey:@"user"];
    NSString *name = [Appointment currentAppointment].physician;
    
    lblPhysician.text = name;
    
    imgProfile.image = [UIImage imageWithData:[Appointment currentAppointment].physicianImage];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AdjustCountdownTimer" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        lblCountdown.text = (NSString *)note.object;
        if([lblCountdown.text isEqualToString:@"Less than a minute remaining to cancel for free."])
        {
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Less than a minute remaining to cancel for free."];
            [attrString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Avenir-Book" size:12.0]
                               range:NSMakeRange(0, attrString.length - 1)];
            lblCountdown.attributedText = attrString;
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CustomerWillBeCharged" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        lblCountdown.hidden = YES;
        willBeCharged = YES;
    }];
    
    //TODO: verify appointment is still active with server
    /*if (self.view.frame.size.height < 500) {
        calendarCenter.priority = UILayoutPriorityDefaultLow;
        calendarLeft.priority = UILayoutPriorityDefaultHigh;
    }*/
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.view.frame.size.height < 500 || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        topConstraint.constant = 0;
        widthConstraint.constant = 80;
        heightConstraint.constant = 80;
        
        imgProfile.layer.cornerRadius = 80.0 / 2;
        imgProfile.clipsToBounds = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAppointment:(id)sender {
    blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
    blackout.backgroundColor = [UIColor blackColor];
    blackout.layer.opacity = 0.7;
    blackout.layer.zPosition = 2000;
    [self.view.window addSubview:blackout];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeDeletionConfirmation)];
    singleTap.numberOfTapsRequired = 1;
    [blackout addGestureRecognizer:singleTap];
    
    if(willBeCharged){
        chargeForCancelView = [[UIView alloc] initWithFrame:CGRectMake(20.0, self.view.window.frame.size.height/2 - 135.0, self.view.frame.size.width - 40.0, 270.0)];
        chargeForCancelView.backgroundColor = [UIColor whiteColor];
        chargeForCancelView.layer.shadowColor = [UIColor blackColor].CGColor;
        chargeForCancelView.layer.shadowOffset = CGSizeMake(0, 1);
        chargeForCancelView.layer.shadowOpacity = 0.7;
        chargeForCancelView.clipsToBounds = NO;
        chargeForCancelView.layer.shadowRadius = 4;
        chargeForCancelView.layer.zPosition = 3001;
        [self.view.window addSubview:chargeForCancelView];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, chargeForCancelView.frame.size.width - 40.0, 30.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:16];
        lblTitle.text = @"CONFIRM";
        [chargeForCancelView addSubview:lblTitle];
        
        UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 45.0, chargeForCancelView.frame.size.width - 40.0, 90.0)];
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
        lblMessage.numberOfLines = 0;
        [chargeForCancelView addSubview:lblMessage];
        
        //set amount based on time away from appointment
        //NSDate *appointmentDate = [Appointment currentAppointment].appointmentDate;
        
        //NSTimeInterval interval = [appointmentDate timeIntervalSinceDate:[NSDate date]];
        
        //if(interval > (60*60*3)){ //seconds * minutes * hours
            lblMessage.text = @"If you cancel, a charge of $75 will automatically be processed.";
        /*}
        else{
            NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
            
            ////NSLog(@"Date comp day: %ld", (long)dateComps.weekday);
            
            if (dateComps.weekday == 7 || dateComps.weekday == 1) { //saturday or sunday
                //weekend
                lblMessage.text = @"If you cancel, a charge of $249 will automatically be processed.";
            }
            else{
                lblMessage.text = @"If you cancel, a charge of $199 will automatically be processed.";
            }
        }*/
        
        UILabel *lblSure = [[UILabel alloc] initWithFrame:CGRectMake(40.0, 125.0, chargeForCancelView.frame.size.width - 80.0, 60.0)];
        lblSure.textAlignment = NSTextAlignmentCenter;
        lblSure.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblSure.text = @"Are you sure you want to cancel this visit?";
        lblSure.numberOfLines = 0;
        [chargeForCancelView addSubview:lblSure];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(chargeForCancelView.frame.size.width/2 - 60.0 - 10.0, 190.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeDeletionConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [chargeForCancelView addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(chargeForCancelView.frame.size.width/2 + 10, 190.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
        [chargeForCancelView addSubview:btnConfirm];
    }
    else{
        deleteConfirmation = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.view.window.frame.size.height/2 - 80, self.view.window.frame.size.width - 100.0, 175.0)];
        deleteConfirmation.backgroundColor = [UIColor whiteColor];
        deleteConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
        deleteConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
        deleteConfirmation.layer.shadowOpacity = 0.7;
        deleteConfirmation.clipsToBounds = NO;
        deleteConfirmation.layer.shadowRadius = 4;
        deleteConfirmation.layer.zPosition = 2001;
        [self.view.window addSubview:deleteConfirmation];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, deleteConfirmation.frame.size.width - 40.0, 60.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblTitle.text = @"Are you sure you want to cancel this visit?";
        lblTitle.numberOfLines = 0;
        [deleteConfirmation addSubview:lblTitle];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(deleteConfirmation.frame.size.width/2 - 60.0 - 10.0, 100.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeDeletionConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(deleteConfirmation.frame.size.width/2 + 10.0, 100.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnConfirm];
    }

}

-(void)closeDeletionConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    if(deleteConfirmation){
        [deleteConfirmation removeFromSuperview];
        deleteConfirmation = nil;
    }
    if(chargeForCancelView){
        [chargeForCancelView removeFromSuperview];
        chargeForCancelView = nil;
    }
}

-(void)confirmDeletion{
    [blackout removeFromSuperview];
    blackout = nil;
    if(deleteConfirmation){
        [deleteConfirmation removeFromSuperview];
        deleteConfirmation = nil;
    }
    if(chargeForCancelView){
        [chargeForCancelView removeFromSuperview];
        chargeForCancelView = nil;
    }
    ////NSLog(@"Cancelling appointment");
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[Appointment currentAppointment].appointmentID forKey:@"appointmentId"];
    
    ////NSLog(@"params: %@", params);
    
    [PFCloud callFunctionInBackground:@"cancelAppointment" withParameters:params block:^(id object, NSError *error) {
        if(error == nil){
            ////NSLog(@"return obj: %@", object);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancelled" object:nil];
            [[Appointment currentAppointment] cancel];
            [self dismissViewControllerAnimated:YES completion:nil];
            //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
        }
        else{
            ////NSLog(@"Error: %@", error.description);
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to cancel an appointment at this time."];
        }
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
