//
//  PatientSignUpViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientSignUpViewController.h"
#import "PatientPaymentInfoViewController.h"

@interface PatientSignUpViewController (){
    IBOutlet UIScrollView *scroller;
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtDOB;
    IBOutlet UITextField *txtPhone;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtConfirmEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;
    
    IBOutlet NSLayoutConstraint *confirmPasswordBottomMargin;
    
    UILabel *lblFirstName;
    UILabel *lblLastName;
    UILabel *lblDOB;
    UILabel *lblPhone;
    UILabel *lblEmail;
    UILabel *lblConfirmEmail;
    UILabel *lblPassword;
    UILabel *lblConfirmPassword;
    
    IBOutlet UIView *nextView;
    IBOutlet UIButton *btnNext;
    IBOutlet NSLayoutConstraint *btnNextPaddingLeft;
    IBOutlet UIButton *btnArrow;
    IBOutlet UILabel *lblAddPhoto;
    IBOutlet UIButton *btnAddPhoto;
    
    NSDictionary *signUpInfo;
    NSString *profileImage;
    
    UIImageView *imgProfile;
}

@end

@implementation PatientSignUpViewController
@synthesize editMode, TOSInfo, NPPInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    singleTap.numberOfTapsRequired = 1;
    [scroller addGestureRecognizer:singleTap];
    
    txtFirstName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtFirstName.layer.borderWidth = 1.0f;
    
    txtLastName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtLastName.layer.borderWidth = 1.0f;
    
    txtDOB.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtDOB.layer.borderWidth = 1.0f;
    txtDOB.font = [UIFont systemFontOfSize:14];
    
    txtPhone.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtPhone.layer.borderWidth = 1.0f;
    
    txtEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtEmail.layer.borderWidth = 1.0f;
    
    txtConfirmEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtConfirmEmail.layer.borderWidth = 1.0f;
    
    txtPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtPassword.layer.borderWidth = 1.0f;
    
    txtConfirmPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtConfirmPassword.layer.borderWidth = 1.0f;
    
    UIView *txtFirstNamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtFirstName.leftView = txtFirstNamePadding;
    txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtLastNamePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtLastName.leftView = txtLastNamePadding;
    txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtDOBPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtDOB.leftView = txtDOBPadding;
    txtDOB.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtPhonePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtPhone.leftView = txtPhonePadding;
    txtPhone.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtEmailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtEmail.leftView = txtEmailPadding;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtConfirmEmailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtConfirmEmail.leftView = txtConfirmEmailPadding;
    txtConfirmEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtPasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtPassword.leftView = txtPasswordPadding;
    txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtConfirmPasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtConfirmPassword.leftView = txtConfirmPasswordPadding;
    txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *nextTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToNext)];
    nextTap.numberOfTapsRequired = 1;
    [nextView addGestureRecognizer:nextTap];
    
    if(editMode)
    {
        self.title = @"EDIT PROFILE";
        nextView.backgroundColor = [UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0];
        btnNextPaddingLeft.constant = self.view.frame.size.width / 2 - btnNext.frame.size.width / 2;
        btnArrow.hidden = YES;
        lblAddPhoto.hidden = YES;
        [btnNext setTitle:@"SAVE" forState:UIControlStateNormal];        
        
        txtFirstName.text = [User sharedUser].firstName;
        txtLastName.text = [User sharedUser].lastName;
        txtDOB.text = [User sharedUser].dob;
        txtPhone.text = [User sharedUser].phone;
        txtEmail.text = [User sharedUser].email;
        
        txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblFirstName = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblFirstName.textColor = [UIColor lightGrayColor];
        lblFirstName.font = [UIFont systemFontOfSize:12.0];
        lblFirstName.text = @"First Name";
        [txtFirstName addSubview:lblFirstName];
        
        txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblLastName = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblLastName.textColor = [UIColor lightGrayColor];
        lblLastName.font = [UIFont systemFontOfSize:12.0];
        lblLastName.text = @"Last Name";
        [txtLastName addSubview:lblLastName];
        
        txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblDOB = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblDOB.textColor = [UIColor lightGrayColor];
        lblDOB.font = [UIFont systemFontOfSize:12.0];
        lblDOB.text = @"Date of Birth";
        [txtDOB addSubview:lblDOB];
        
        txtPhone.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblPhone = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblPhone.textColor = [UIColor lightGrayColor];
        lblPhone.font = [UIFont systemFontOfSize:12.0];
        lblPhone.text = @"Phone Number";
        [txtPhone addSubview:lblPhone];
        
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblEmail.textColor = [UIColor lightGrayColor];
        lblEmail.font = [UIFont systemFontOfSize:12.0];
        lblEmail.text = @"Email Address";
        [txtEmail addSubview:lblEmail];
        
        /*txtConfirmEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
        lblConfirmEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
        lblConfirmEmail.textColor = [UIColor lightGrayColor];
        lblConfirmEmail.font = [UIFont systemFontOfSize:12.0];
        lblConfirmEmail.text = @"Confirm Email Address";
        [txtConfirmEmail addSubview:lblConfirmEmail];*/
        
        btnAddPhoto.hidden = YES;
        
        if([User sharedUser].customerID != nil){
            PFQuery *query = [PFQuery queryWithClassName:@"Customer"];
            __block id viewController = self;
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:[User sharedUser].customerID block:^(PFObject *customer, NSError *error) {
                if(!error){
                    PFFile *userImageFile = customer[@"avatar"];
                    if(userImageFile){
                        [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                            if (!error) {
                                imgProfile = [[UIImageView alloc] initWithImage:[UIImage imageWithData:imageData]];
                                imgProfile.frame = CGRectMake(self.view.frame.size.width / 2 - 47.5, btnAddPhoto.frame.origin.y, 95, 95);
                                [scroller addSubview:imgProfile];
                                imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
                                imgProfile.clipsToBounds = YES;
                                imgProfile.userInteractionEnabled = YES;
                                
                                UIView *blackOut = [[UIView alloc] initWithFrame:imgProfile.bounds];
                                blackOut.backgroundColor = [UIColor blackColor];
                                blackOut.layer.opacity = 0.6;
                                blackOut.layer.cornerRadius = blackOut.frame.size.width / 2;
                                blackOut.clipsToBounds = YES;
                                [imgProfile addSubview:blackOut];
                                
                                UITapGestureRecognizer *editTap = [[UITapGestureRecognizer alloc] initWithTarget:viewController action:@selector(editProfileImage:)];
                                editTap.numberOfTapsRequired = 1;
                                [blackOut addGestureRecognizer:editTap];
                                
                                UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
                                editButton.frame = CGRectMake(blackOut.frame.size.width / 2 - 30, blackOut.frame.size.height / 2 - 15, 60.0, 30.0);
                                editButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
                                [editButton setTitle:@"EDIT" forState:UIControlStateNormal];
                                [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                [editButton addTarget:viewController action:@selector(editProfileImage:) forControlEvents:UIControlEventTouchUpInside];
                                [blackOut addSubview:editButton];
                                editButton.layer.zPosition = 3000;
                            }
                            else{
                                btnAddPhoto.hidden = NO;
                                lblAddPhoto.hidden = NO;
                            }
                        }];
                    }
                    else{
                        btnAddPhoto.hidden = NO;
                        lblAddPhoto.hidden = NO;
                    }
                    
                }
                else{
                    ////NSLog(@"Error retrieving profile image");
                    btnAddPhoto.hidden = NO;
                    lblAddPhoto.hidden = NO;
                }
            }];
        }
        else{
            PFQuery *query = [PFQuery queryWithClassName:@"Provider"];
            __block id viewController = self;
            // Retrieve the object by id
            [query getObjectInBackgroundWithId:[User sharedUser].providerID block:^(PFObject *provider, NSError *error) {
                if(!error){
                    PFFile *userImageFile = provider[@"avatar"];
                    if(userImageFile){
                        [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                            if (!error) {
                                imgProfile = [[UIImageView alloc] initWithImage:[UIImage imageWithData:imageData]];
                                imgProfile.frame = CGRectMake(self.view.frame.size.width / 2 - 47.5, btnAddPhoto.frame.origin.y, 95, 95);
                                [scroller addSubview:imgProfile];
                                imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
                                imgProfile.clipsToBounds = YES;
                                imgProfile.userInteractionEnabled = YES;
                                
                                UIView *blackOut = [[UIView alloc] initWithFrame:imgProfile.bounds];
                                blackOut.backgroundColor = [UIColor blackColor];
                                blackOut.layer.opacity = 0.6;
                                blackOut.layer.cornerRadius = blackOut.frame.size.width / 2;
                                blackOut.clipsToBounds = YES;
                                [imgProfile addSubview:blackOut];
                                
                                UITapGestureRecognizer *editTap = [[UITapGestureRecognizer alloc] initWithTarget:viewController action:@selector(editProfileImage:)];
                                editTap.numberOfTapsRequired = 1;
                                [blackOut addGestureRecognizer:editTap];
                                
                                UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
                                editButton.frame = CGRectMake(blackOut.frame.size.width / 2 - 30, blackOut.frame.size.height / 2 - 15, 60.0, 30.0);
                                editButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
                                [editButton setTitle:@"EDIT" forState:UIControlStateNormal];
                                [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                [editButton addTarget:viewController action:@selector(editProfileImage:) forControlEvents:UIControlEventTouchUpInside];
                                [blackOut addSubview:editButton];
                                editButton.layer.zPosition = 3000;
                            }
                            else{
                                btnAddPhoto.hidden = NO;
                                lblAddPhoto.hidden = NO;
                            }
                        }];
                    }
                    else{
                        btnAddPhoto.hidden = NO;
                        lblAddPhoto.hidden = NO;
                    }
                    
                }
                else{
                    ////NSLog(@"Error retrieving profile image");
                    btnAddPhoto.hidden = NO;
                    lblAddPhoto.hidden = NO;
                }
            }];
        }
        
        
    }
    else{
        self.title = @"PROFILE";
        nextView.backgroundColor = [UIColor lightGrayColor];
        btnNextPaddingLeft.constant = 23;
        btnArrow.hidden = NO;
        [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
        
        
    }
    
    /*txtFirstName.text = @"Andrew";
    txtLastName.text = @"Goodwin";
    txtDOB.text = @"03 / 07 / 1986";
    txtPhone.text = @"(501) 207-2336";
    txtEmail.text = @"andrewggoodwin@gmail.com";
    txtConfirmEmail.text = @"andrewggoodwin@gmail.com";
    txtPassword.text = @"ageneg";
    txtConfirmPassword.text = @"ageneg";
    profileImage = @"hi";
    [self canSubmit];*/
    
}

-(void)goToNext{
    if([self canSubmit]){
        if (!editMode) {
            signUpInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"firstName":txtFirstName.text,
                                                                           @"lastName":txtLastName.text,
                                                                           @"email":txtEmail.text,
                                                                           @"username":txtEmail.text,
                                                                           @"password":txtPassword.text,
                                                                           @"phone":txtPhone.text,
                                                                           @"dob":txtDOB.text
                                                                           }];
            
            [signUpInfo setValue:[TOSInfo objectForKey:@"tosAccepted"] forKey:@"tosAccepted"];
            [signUpInfo setValue:[TOSInfo objectForKey:@"tosTimestamp"] forKey:@"tosTimestamp"];
            [signUpInfo setValue:[NPPInfo objectForKey:@"nppAccepted"] forKey:@"nppAccepted"];
            [signUpInfo setValue:[NPPInfo objectForKey:@"nppTimestamp"] forKey:@"nppTimestamp"];
            
            [self performSegueWithIdentifier:@"segueShowPayment" sender:self];
        }
        else{
            if(txtEmail.text.length > 5 && [txtEmail.text isEqualToString:txtConfirmEmail.text])
            {
                [PFUser currentUser].email = txtEmail.text;
                [PFUser currentUser].username = txtEmail.text;
            }
            
            
            [PFUser currentUser][@"firstName"] = txtFirstName.text;
            [PFUser currentUser][@"lastName"] = txtLastName.text;
            [PFUser currentUser][@"phone"] = txtPhone.text;
            
            if(txtPassword.text.length > 5 && [txtPassword.text isEqualToString:txtConfirmPassword.text])
            {
                [PFUser currentUser].password = txtPassword.text;
            }
            
            
            [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    
                    [User sharedUser].email = txtEmail.text;
                    [User sharedUser].username = txtEmail.text;
                    [User sharedUser].firstName = txtFirstName.text;
                    [User sharedUser].lastName = txtLastName.text;
                    [User sharedUser].phone = txtPhone.text;
                    [[User sharedUser] save];
                    
                    PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
                    [custQuery getObjectInBackgroundWithId:[User sharedUser].customerID block:^(PFObject *customer, NSError *error2) {
                        if(!error2){
                            NSDateFormatter *format = [[NSDateFormatter alloc] init];
                            [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                            [format setDateFormat:@"MM / dd / yyyy"];
                            customer[@"dob"] = [format dateFromString:txtDOB.text];
                            [customer saveInBackgroundWithBlock:^(BOOL succeeded2, NSError *error3) {
                                if(succeeded2){
                                    //popup?
                                    [User sharedUser].dob = txtDOB.text;
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataChanged" object:nil]; //we only call this because it does everything we want
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                }
                                else{
                                    ////NSLog(@"error: %@", error3);
                                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                    [appDelegate showModal:@"Unable to update your customer record at this time."];
                                }
                            }];
                        }
                        else{
                            ////NSLog(@"error: %@", error2);
                        }
                        
                    }];
                    
                    
                    
                }
                else{
                    ////NSLog(@"error: %@", error);
                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate showModal:@"Unable to update your profile at this time."];
                }
            }];
        }
        
    }
    
}

-(void)hideKeyboard{
    [txtFirstName resignFirstResponder];
    [txtLastName resignFirstResponder];
    [txtDOB resignFirstResponder];
    [txtPhone resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtConfirmEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtConfirmPassword resignFirstResponder];
    [scroller scrollRectToVisible:CGRectMake(0.0, 0.0, self.view.frame.size.width, 100.0) animated:YES];
    [self canSubmit];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName = @"Patient Sign Up";
    //scroller.contentSize = CGSizeMake(self.view.frame.size.width, 1608.0);
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Formatter methods
-(void)selectTextForInput:(UITextField *)input atRange:(NSRange)range {
    UITextPosition *start = [input positionFromPosition:[input beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [input positionFromPosition:start
                                               offset:range.length];
    [input setSelectedTextRange:[input textRangeFromPosition:start toPosition:end]];
}
-(void)setDOBMask:(UITextField *)textField{
    textField.textColor = [UIColor lightGrayColor];
    
    if (textField.text.length > 0){
        BOOL foundLetter = NO;
        int charIndex = -1;
        for (NSInteger charIdx=0; charIdx<=13; charIdx++){
            if(!foundLetter){
                NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
                NSString *expression = @"^[DMY]$";
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
                NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
                if(range.location != NSNotFound)
                {
                    //it is a letter
                    charIndex = charIdx;
                    foundLetter = YES;
                }
            }
        }
        if(!foundLetter){
            //it's all numbers (see if it's a good date)
            NSString *firstChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
            NSString *secondChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:1]];
            NSString *thirdChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:5]];
            NSString *fourthChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:6]];
            NSString *fifthChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:10]];
            NSString *sixthChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:11]];
            NSString *seventhChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:12]];
            NSString *eighthChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:13]];
            
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@%@%@", firstChar, secondChar, thirdChar, fourthChar, fifthChar, sixthChar, seventhChar, eighthChar]];
            
            
            //check to see if date is good
            /*NSString *monthPart = [textField.attributedText.string substringToIndex:2];
            NSString *dayPart = [textField.attributedText.string substringWithRange:NSMakeRange(5, 7)];*/
            NSString *yearPart = [textField.attributedText.string substringFromIndex:10];
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
            
            if(yearPart.intValue > 1900 && yearPart.intValue <= components.year){
                //good year so far
                
                if(yearPart.intValue > components.year - 18){
                    //they aren't yet 18
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Too young" message:@"You must be at least 18 in order to create an account." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alert show];
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange(0, 14)];
                    
                    textField.attributedText = text;
                }
                else{
                    [text addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, 14)];
                    
                    textField.attributedText = text;
                }
            }
            else{
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(0, 14)];
                
                textField.attributedText = text;
            }
            /*NSMutableAttributedString *text =
             [[NSMutableAttributedString alloc]
             initWithAttributedString:textField.attributedText];
             
             [text addAttribute:NSForegroundColorAttributeName
             value:[UIColor blackColor]
             range:NSMakeRange(0, charIndex)];
             textField.attributedText = text;
             [self selectTextForInput:textField atRange:NSMakeRange(charIndex, 0)];*/
        }
        else{
            //found a letter
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithAttributedString:textField.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex)];
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex, 0)];
        }
        
        
    }
    else{
        
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"MM / DD / YYYY"]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor lightGrayColor]
                     range:NSMakeRange(0, 14)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(0, 0)];
    }
}
-(void)removeDOBMask:(UITextField *)textField{
    if (textField.text.length > 0){
        NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:0]];
        if([thisChar isEqualToString:@"M"])
            textField.text = @"";
    }
}
-(NSNumber *)DOBHandleBackspace:(UITextField *)textField inputString:(NSString *)string resultingString:(NSString *)textString{
    
    /*   0 - NO
     1 - YES
     2 - YES with error
     -1 - Passthrough (don't return yet)
     -2 - NO with error
     */
    
    NSNumber *retVal = @-1;
    
    BOOL foundLetter = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<=13; charIdx++){
        if(!foundLetter){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[MYD]$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a letter
                charIndex = charIdx;
                foundLetter = YES;
            }
        }
    }
    
    if(charIndex == 0 ||charIndex == 1)
    {
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"MM / DD / YYYY"]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 0)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(0, 0)];
        
        return @0;
    }
    else if(charIndex == 5)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@M / DD / YYYY", firstChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(1, 0)];
        return @0;
    }
    /*else if(charIndex == 6)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / DD / YYYY", firstChar, secondChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 2)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(2, 0)];
        return @0;
    }*/
    else if(charIndex == 6)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / DD / YYYY", firstChar, secondChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 5)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(5, 0)];
        return @0;
    }
    else if(charIndex == 10)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@D / YYYY", firstChar, secondChar, thirdChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 6)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(6, 0)];
        return @0;
    }
    else if(charIndex == 11)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / YYYY", firstChar, secondChar, thirdChar, fourthChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 10)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(10, 0)];
        return @0;
    }
    else if(charIndex == 12)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@YYY", firstChar, secondChar, thirdChar, fourthChar, fifthChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 11)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(11, 0)];
        return @0;
    }
    else if(charIndex == 13)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        NSString *sixthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:11]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@YY", firstChar, secondChar, thirdChar, fourthChar, fifthChar, sixthChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 12)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(12, 0)];
        return @0;
    }
    else if(charIndex == -1)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        NSString *sixthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:11]];
        NSString *seventhChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:12]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@%@Y", firstChar, secondChar, thirdChar, fourthChar, fifthChar, sixthChar, seventhChar]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, 13)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(13, 0)];
        return @0;
    }
    
    return retVal;
}
-(NSNumber *)DOBHandleTyping:(UITextField *)textField inputString:(NSString *)string resultingString:(NSString *)textString{
    /*   0 - NO
     1 - YES
     2 - NO with good date
     3 - YES with good date
     -1 - Passthrough (don't return yet)
     -2 - NO with error
     -3 - YES with error
     */
    
    NSString *lastChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:13]];
    if(textString.length == 14 && ![lastChar isEqualToString:@"Y"])
        return @0;
    
    NSNumber *retVal = @-1;
    
    BOOL foundLetter = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<textField.text.length; charIdx++){
        if(!foundLetter){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[MYD]$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a letter
                charIndex = charIdx;
                foundLetter = YES;
            }
        }
    }
    
    if(charIndex == 0)
    {
        //the first digit is being entered
        
        if(string.intValue > 1){
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"0%@ / DD / YYYY", string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 5)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 5, 0)];
        }
        else{
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@M / DD / YYYY", string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 1)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        }
        
        
        
        return @0;
    }
    else if(charIndex == 1)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];

        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / DD / YYYY", firstChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 4)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 4, 0)];
        return @0;
    }
    else if(charIndex == 5)
    {
        if(string.intValue >= 4){
            NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
            NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
            //the first digit is being entered
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@%@ / 0%@ / YYYY", firstChar, secondChar, string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 5)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 5, 0)];
        }
        else{
            NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
            NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
            //the first digit is being entered
            NSMutableAttributedString *text =
            [[NSMutableAttributedString alloc]
             initWithString:[NSString stringWithFormat:@"%@%@ / %@D / YYYY", firstChar, secondChar, string]];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor blackColor]
                         range:NSMakeRange(0, charIndex + 1)];
            
            textField.attributedText = text;
            [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        }
        
        return @0;
    }
    else if(charIndex == 6)
    {
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / YYYY", firstChar, secondChar, thirdChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 4)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 4, 0)];
        
        return @0;
    }
    else if (charIndex == 10){
        if(string.intValue != 1 && string.intValue != 2){
            return @0; //do not allow entry of 1900 or 2000 date
        }
        
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@YYY", firstChar, secondChar, thirdChar, fourthChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        return @0;
    }
    else if (charIndex == 11){
        if(string.intValue != 9 && string.intValue != 0 && string.intValue != 1){
            return @0; //do not allow entry of 1900, 2000, 2100 date
        }
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@YY", firstChar, secondChar, thirdChar, fourthChar, fifthChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        return @0;
    }
    else if (charIndex == 12){
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        NSString *sixthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:11]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@%@Y", firstChar, secondChar, thirdChar, fourthChar, fifthChar, sixthChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        return @0;
    }
    else if (charIndex == 13){
        NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
        NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
        NSString *thirdChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:5]];
        NSString *fourthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
        NSString *fifthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:10]];
        NSString *sixthChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:11]];
        NSString *seventhChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:12]];
        //the first digit is being entered
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:[NSString stringWithFormat:@"%@%@ / %@%@ / %@%@%@%@", firstChar, secondChar, thirdChar, fourthChar, fifthChar, sixthChar, seventhChar, string]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor blackColor]
                     range:NSMakeRange(0, charIndex + 1)];
        
        textField.attributedText = text;
        [self selectTextForInput:textField atRange:NSMakeRange(charIndex + 1, 0)];
        //check to see if date is good
        
        NSString *yearPart = [textField.attributedText.string substringFromIndex:10];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        if(yearPart.intValue > 1900 && yearPart.intValue <= components.year){
            //good year so far
            
            if(yearPart.intValue > components.year - 18){
                //they aren't yet 18
                /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Too young" message:@"You must be at least 18 in order to create an account." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alert show];*/
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"We're sorry. You must be at least 18 in order to create an account."];
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(0, 14)];
                
                textField.attributedText = text;
                return @-2;
            }
            if(yearPart.intValue < components.year - 64){
                //they aren't yet 18
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"We're sorry. You must be under 65 in order to create an account."];
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor redColor]
                             range:NSMakeRange(0, 14)];
                
                textField.attributedText = text;
                return @-2;
            }
            else{
                [text addAttribute:NSForegroundColorAttributeName
                             value:[UIColor blackColor]
                             range:NSMakeRange(0, 13)];
                
                textField.attributedText = text;
                return @2;
            }
        }
        else{
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor redColor]
                         range:NSMakeRange(0, 14)];
            
            textField.attributedText = text;
            return @-2;
        }
    }
    
    return retVal;
}
-(BOOL)hasAttemptedDOB:(UITextField *)textField{
    BOOL foundNumber = NO;
    int charIndex = -1;
    for (NSInteger charIdx=0; charIdx<=13; charIdx++){
        if(!foundNumber){
            NSString *thisChar = [NSString stringWithFormat:@"%c",[textField.text characterAtIndex:charIdx]];
            NSString *expression = @"^[0-9]";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:thisChar options:0 range:NSMakeRange(0, thisChar.length)];
            if(range.location != NSNotFound)
            {
                //it is a number
                charIndex = charIdx;
                foundNumber = YES;
            }
        }
    }
    
    return foundNumber;
}

#pragma mark - UITextfield methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //textField.backgroundColor = [UIColor whiteColor];
    textField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    if(![textField isEqual:txtDOB])
        textField.textColor = [UIColor blackColor];
    
    NSString *textString = @"";
    if(string.length > 0)
    {
        //typing
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) { // In fact, in iOS7, '[[textField textInputMode] primaryLanguage]' is nil
            return NO;
        }
        
        textString = [NSString stringWithFormat:@"%@%@", textField.text, string];
        //////NSLog(@"Full string: %@", textString);
    }
    else{
        //backspace
        textString = [textField.text substringToIndex:textField.text.length - 1];
        //////NSLog(@"Full string: %@", textString);
        
        /*if([textField isEqual:txtDOB] && ([textString hasSuffix:@"/"] || [textField.text hasSuffix:@"/"]))
        {
            textField.text = [textField.text substringToIndex:textField.text.length - 2];
            return NO;
        }*/
        if([textField isEqual:txtDOB]){
            NSNumber *retVal = [self DOBHandleBackspace:textField inputString:string resultingString:textString];
            if(retVal.intValue != -1)
            {
                if(retVal.intValue == 1 || retVal.intValue == 0)
                    return retVal.boolValue;
                else if (retVal.intValue == -2){
                    //handle incorrect dob here
                    return NO;
                }
                else if (retVal.intValue == -3)
                    //handle incorrect dob here
                    return YES;
                else if (retVal.intValue == 2)
                {
                    //good dob
                    
                    return NO;
                }
            }
        }
        else if ([textField isEqual:txtPhone] && ([textString hasSuffix:@"-"] || [textField.text hasSuffix:@"-"])){
            textField.text = [textField.text substringToIndex:textField.text.length - 2];
            return NO;
        }
        else if ([textField isEqual:txtPhone] && textString.length == 6){
            textField.text = [textField.text substringToIndex:textField.text.length - 3];
            return NO;
        }
        else if ([textField isEqual:txtPhone] && textString.length == 5){
            textField.text = [textField.text substringToIndex:textField.text.length - 2];
            return NO;
        }
        else if ([textField isEqual:txtPhone] && textString.length < 5 && textString.length != 1){
            return YES;
        }
        else if ([textField isEqual:txtPhone] && textString.length == 1){
            textField.text = @"";
            [lblPhone removeFromSuperview];
            lblPhone = nil;
            txtPhone.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
            return NO;
        }
        else if ([textField isEqual:txtPassword]){
            if (lblPassword && textString.length == 0){
                [lblPassword removeFromSuperview];
                lblPassword = nil;
                txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
                txtPassword.rightView = nil;
                txtConfirmPassword.rightView = nil;
                txtConfirmPassword.text = @"";
                [lblConfirmPassword removeFromSuperview];
                lblConfirmPassword = nil;
                txtConfirmPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
                return YES;
            }
        }
        else if ([textField isEqual:txtConfirmPassword]){
            if (lblConfirmPassword && textString.length == 0){
                [lblConfirmPassword removeFromSuperview];
                lblConfirmPassword = nil;
                txtConfirmPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
                txtConfirmPassword.rightView = nil;
                return YES;
            }
        }
    }
    
    if([textField isEqual:txtFirstName])
    {
        if(textString.length == 1 && !lblFirstName){
            txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblFirstName = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblFirstName = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 110.0, 2.0, 100.0, 14.0)];
            //lblFirstName.textAlignment = NSTextAlignmentRight;
            lblFirstName.textColor = [UIColor lightGrayColor];
            lblFirstName.font = [UIFont systemFontOfSize:12.0];
            lblFirstName.text = @"First Name";
            [txtFirstName addSubview:lblFirstName];
            
            
            //txtFirstName.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
        }
        else if (textString.length == 0 && lblFirstName){
            [lblFirstName removeFromSuperview];
            lblFirstName = nil;
            txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        NSString *notAName = @"[0-9\\*\\&\\^\\%\\$\\#\\@\\!\\+\\=\\_\\(\\)\\:\\;\\/\\,\\?\\!]";
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:notAName options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
        if(range.location != NSNotFound)
        {
            //an illegal character was found
            return NO;
        }
        
        if(textString.length > 0){
            lblFirstName.textColor = [UIColor lightGrayColor];
        }
    }
    else if([textField isEqual:txtLastName])
    {
        if(textString.length == 1 && !lblLastName){
            txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblLastName = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblLastName.textAlignment = NSTextAlignmentRight;
            lblLastName.textColor = [UIColor lightGrayColor];
            lblLastName.font = [UIFont systemFontOfSize:12.0];
            lblLastName.text = @"Last Name";
            [txtLastName addSubview:lblLastName];
        }
        else if (textString.length == 0 && lblLastName){
            [lblLastName removeFromSuperview];
            lblLastName = nil;
            txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        NSString *notAName = @"[0-9\\*\\&\\^\\%\\$\\#\\@\\!\\+\\=\\_\\(\\)\\:\\;\\/\\,\\?\\!]";
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:notAName options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
        if(range.location != NSNotFound)
        {
            //an illegal character was found
            return NO;
        }
        
        if(textString.length > 0){
            lblLastName.textColor = [UIColor lightGrayColor];
        }
    }
    else if([textField isEqual:txtDOB])
    {
        /*if(textString.length == 1 && !lblDOB){
            txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblDOB = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblDOB.textAlignment = NSTextAlignmentRight;
            lblDOB.textColor = [UIColor lightGrayColor];
            lblDOB.font = [UIFont systemFontOfSize:12.0];
            lblDOB.text = @"Date of Birth";
            [txtDOB addSubview:lblDOB];
        }
        else if (textString.length == 0 && lblDOB){
            [lblDOB removeFromSuperview];
            lblDOB = nil;
            txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(textString.length >= 11)
            return NO;
        
        BOOL hasDOBError = NO;
        if(textString.length > 0){
            NSString *firstChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:0]];
         
            
            if(textString.length == 1)
            {
                if(firstChar.intValue > 1){
                    textField.text = [NSString stringWithFormat:@"0%@/", firstChar];
                    return NO;
                }
            }
            
            if(textString.length > 1){
                NSString *secondChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:1]];
                if([firstChar isEqualToString:@"0"] && [secondChar isEqualToString:@"0"]){
                    //this is an error
                    hasDOBError = YES;
                }
                else if([firstChar isEqualToString:@"1"] && secondChar.intValue > 2){
                    //this is an error
                    hasDOBError = YES;
                }
                
                if(textString.length == 2 && !hasDOBError){
                    if(firstChar.intValue > 2){
                        if (firstChar.intValue == 3 && secondChar.intValue > 1) {
                            if(secondChar.intValue > 3){
                                textField.text = [NSString stringWithFormat:@"0%@/0%@/", firstChar, secondChar];
                            }
                            else{
                                textField.text = [NSString stringWithFormat:@"0%@/%@", firstChar, secondChar];
                            }
                            return NO;
                        }
                    }
                    textField.text = [NSString stringWithFormat:@"%@/",textString];
                    return NO;
                }
                
                if(textString.length == 4){
                    //entering first digit of day
                    NSString *firstDayChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:3]];
                    if(firstDayChar.intValue > 3){
                        //they are entering a day correctly, 04/4
                        textField.text = [NSString stringWithFormat:@"%@%@/0%@/",firstChar, secondChar,firstDayChar];
                        return NO;
                    }
                }
                
                if(textString.length == 5){
                    NSString *firstDayChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:3]];
                    NSString *dayNumber = [NSString stringWithFormat:@"%@%@", firstDayChar, string];
                    if(dayNumber.intValue < 32){
                        //valid day
                    }
                    else{
                        hasDOBError = YES;
                    }
                    
                    if(textString.length == 5 && !hasDOBError){
                        textField.text = [NSString stringWithFormat:@"%@/",textString];
                        return NO;
                    }
                }
                
                if(textString.length == 7){
                    // 04/11/9
         
                }
                
                if(textString.length == 9)
                {
                    // 04/11/994
                    NSString *yearPart = [textString substringFromIndex:6];
                     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                    if(yearPart.intValue < (components.year - 2000) || yearPart.intValue > 210){
                        NSString *dayChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:3]];
                        NSString *firstYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:4]];
                        NSString *secondYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
                        NSString *thirdYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:7]];
                        NSString *fourthYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:8]];
                        textField.text = [NSString stringWithFormat:@"%@%@/0%@/%@%@%@%@", firstChar, secondChar,dayChar,firstYearChar,secondYearChar,thirdYearChar,fourthYearChar];
                        return NO;
                    }
                }
                
                if(textString.length == 10)
                {
                    //they have entered the year
                    NSString *firstYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:6]];
                    NSString *secondYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:7]];
                    NSString *century = [NSString stringWithFormat:@"%@%@", firstYearChar, secondYearChar];
                    
                    if(century.intValue == 19 || century.intValue == 20){
                        //good year so far
                        NSString *thirdYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:8]];
                        NSString *fourthYearChar = [NSString stringWithFormat:@"%c",[textString characterAtIndex:9]];
                        NSString *decade = [NSString stringWithFormat:@"%@%@", thirdYearChar, fourthYearChar];
                        
                        NSString *fullYear = [NSString stringWithFormat:@"%@%@", century, decade];
                        
                        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                        
                        if(fullYear.intValue > components.year){
                            //cannot be born in the future
                            hasDOBError = YES;
                        }
                        else if (fullYear.intValue > components.year - 18){
                            //isnt over 18
                            hasDOBError = YES;
                        }
                        else if (!hasDOBError){
                            //good dob, show checkmark
         
                            [self performSelector:@selector(focusPhone) withObject:nil afterDelay:0.1];
                        }
                        else{
                            txtDOB.rightView = nil;
                        }
                    }
                    else{
                        //bad year so far
                        hasDOBError = YES;
                    }
                    
                    
                }
            }
        }*/
        if(textString.length > 15)
            return NO;
        NSNumber *retVal = [self DOBHandleTyping:textField inputString:string resultingString:textString];
        if(retVal.intValue != -1)
        {
            if(retVal.intValue == 1 || retVal.intValue == 0){
                return retVal.boolValue;
            }
            else if (retVal.intValue == -2){
                //handle incorrect date here
                lblDOB.textColor = [UIColor redColor];
                return NO;
            }
            else if (retVal.intValue == -3){
                //handle incorrect date here
                lblDOB.textColor = [UIColor redColor];
                return YES;
            }
            else if (retVal.intValue == 2)
            {
                //good date
                lblDOB.textColor = [UIColor lightGrayColor];
                [self performSelector:@selector(focusPhone) withObject:nil afterDelay:0.1];
                return NO;
            }
        }
    }
    else if ([textField isEqual:txtPhone])
    {
        
        if(textString.length >= 15)
            return NO;
        
        if(textString.length == 1 && !lblPhone){
            txtPhone.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblPhone = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblPhone.textAlignment = NSTextAlignmentRight;
            lblPhone.textColor = [UIColor lightGrayColor];
            lblPhone.font = [UIFont systemFontOfSize:12.0];
            lblPhone.text = @"Phone Number";
            [txtPhone addSubview:lblPhone];
        }
        /*else if (textField.text.length == 1 && string.length == 0 && lblPhone){
            [lblPhone removeFromSuperview];
            lblPhone = nil;
            txtPhone.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }*/
        
        if(textString.length == 3 && ![textString containsString:@"("]){
            textField.text = [NSString stringWithFormat:@"(%@) ", textString];
            return NO;
        }
        else if (textString.length > 3 && ![textString containsString:@")"]){
            NSMutableString *phone = [[NSMutableString alloc] initWithString:textString];
            [phone insertString:@") " atIndex:4];
            textField.text = phone;
            return NO;
        }
        else if(textString.length == 9){
            textField.text = [NSString stringWithFormat:@"%@-", textString];
            return NO;
        }
        else if(textString.length == 14){
            lblPhone.textColor = [UIColor lightGrayColor];
            [self performSelector:@selector(focusEmail) withObject:nil afterDelay:0.1];
        }
        
    }
    else if ([textField isEqual:txtEmail]){
        if(textString.length == 1 && !lblEmail){
            txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblEmail.textAlignment = NSTextAlignmentRight;
            lblEmail.textColor = [UIColor lightGrayColor];
            lblEmail.font = [UIFont systemFontOfSize:12.0];
            lblEmail.text = @"Email Address";
            [txtEmail addSubview:lblEmail];
        }
        else if (textString.length == 0 && lblEmail){
            [lblEmail removeFromSuperview];
            lblEmail = nil;
            txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(lblEmail.textColor == [UIColor redColor]){
            NSString *email = textString;
            NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
            if(range.location != NSNotFound)
            {
                lblEmail.textColor = [UIColor lightGrayColor];
                txtEmail.textColor = [UIColor blackColor];
            }
        }
    }
    else if ([textField isEqual:txtConfirmEmail]){
        if(textString.length == 1 && !lblConfirmEmail){
            txtConfirmEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblConfirmEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 180.0, 14.0)];
            //lblEmail.textAlignment = NSTextAlignmentRight;
            lblConfirmEmail.textColor = [UIColor lightGrayColor];
            lblConfirmEmail.font = [UIFont systemFontOfSize:12.0];
            lblConfirmEmail.text = @"Confirm Email Address";
            [txtConfirmEmail addSubview:lblConfirmEmail];
        }
        else if (textString.length == 0 && lblConfirmEmail){
            [lblConfirmEmail removeFromSuperview];
            lblConfirmEmail = nil;
            txtConfirmEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(lblConfirmEmail.textColor == [UIColor redColor]){
            NSString *email = textString;
            NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
            if(range.location != NSNotFound)
            {
                lblConfirmEmail.textColor = [UIColor lightGrayColor];
                txtConfirmEmail.textColor = [UIColor blackColor];
            }
        }
    }
    else if ([textField isEqual:txtPassword])
    {
        if(textString.length == 1 && !lblPassword){
            txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblPassword = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblPassword.textAlignment = NSTextAlignmentRight;
            lblPassword.textColor = [UIColor lightGrayColor];
            lblPassword.font = [UIFont systemFontOfSize:12.0];
            lblPassword.text = @"Password";
            [txtPassword addSubview:lblPassword];
        }
        
        
        if(textField.text.length >= 5 && textString.length > 5){
            UIImageView *check = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 30.0, 15.0)];
            check.image = [UIImage imageNamed:@"icon_checkmark"];
            check.contentMode = UIViewContentModeScaleAspectFit;
            txtPassword.rightView = check;
            txtPassword.rightViewMode = UITextFieldViewModeAlways;
            lblPassword.textColor = [UIColor lightGrayColor];
        }
        else{
            txtPassword.rightView = nil;
            txtConfirmPassword.rightView = nil;
        }
        
        if(![txtPassword.text isEqualToString:txtConfirmPassword.text]){
            txtConfirmPassword.rightView = nil;
        }
        else if (textField.text.length >= 5 && textString.length > 5){
            UIImageView *check = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 30.0, 15.0)];
            check.image = [UIImage imageNamed:@"icon_checkmark"];
            check.contentMode = UIViewContentModeScaleAspectFit;
            txtConfirmPassword.rightView = check;
            txtConfirmPassword.rightViewMode = UITextFieldViewModeAlways;
            lblConfirmPassword.textColor = [UIColor lightGrayColor];
            //[self canSubmit];
        }
    }
    else if ([textField isEqual:txtConfirmPassword])
    {
        if(textString.length == 1 && !lblConfirmPassword){
            txtConfirmPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblConfirmPassword = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 130.0, 14.0)];
            //lblConfirmPassword.textAlignment = NSTextAlignmentRight;
            lblConfirmPassword.textColor = [UIColor lightGrayColor];
            lblConfirmPassword.font = [UIFont systemFontOfSize:12.0];
            lblConfirmPassword.text = @"Confirm Password";
            [txtConfirmPassword addSubview:lblConfirmPassword];
        }
        
        if([textString isEqualToString:txtPassword.text] && txtPassword.text.length > 5){
            UIImageView *check = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 30.0, 15.0)];
            check.image = [UIImage imageNamed:@"icon_checkmark"];
            check.contentMode = UIViewContentModeScaleAspectFit;
            txtConfirmPassword.rightView = check;
            txtConfirmPassword.rightViewMode = UITextFieldViewModeAlways;
            lblConfirmPassword.textColor = [UIColor lightGrayColor];
            //[self canSubmit];
            //[self performSelector:@selector(canSubmit) withObject:nil afterDelay:0.1];
        }
    }
    
    BOOL shouldCheckRegex = YES;
    if(txtFirstName.text.length < 1 || txtLastName.text.length < 1)
        shouldCheckRegex = NO;
    if(txtDOB.text.length < 4)
        shouldCheckRegex = NO;
    if(txtEmail.text.length < 6 || txtConfirmEmail.text.length < 6)
        shouldCheckRegex = NO;
    if(txtPhone.text.length < 7)
        shouldCheckRegex = NO;
    if(txtPassword.text.length < 1 || txtConfirmPassword.text.length < 1)
        shouldCheckRegex = NO;
    if(![txtPassword.text isEqualToString: txtConfirmPassword.text])
        shouldCheckRegex = NO;
    
    if(shouldCheckRegex){
        [self performSelector:@selector(canSubmit) withObject:nil afterDelay:0.1];
    }
    
    return YES;
}

-(void)focusPhone{
    [txtPhone becomeFirstResponder];
}

-(void)focusEmail{
    [txtEmail becomeFirstResponder];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [scroller setContentOffset:CGPointMake(0, textField.frame.origin.y - 110.0) animated:YES];
    
    confirmPasswordBottomMargin.constant = 216.0;
    
    if([textField isEqual:txtDOB]){
        if(!lblDOB){
            txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblDOB = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 100.0, 14.0)];
            //lblDOB.textAlignment = NSTextAlignmentRight;
            lblDOB.textColor = [UIColor lightGrayColor];
            lblDOB.font = [UIFont systemFontOfSize:12.0];
            lblDOB.text = @"Date of Birth";
            [txtDOB addSubview:lblDOB];
        }
        [self setDOBMask:textField];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    /*if([textField isEqual:txtFirstName]){
        if(txtFirstName.text.length < 1){
            lblFirstName.textColor = [UIColor redColor];
            txtFirstName.textColor = [UIColor redColor];
        }
    }
    if([textField isEqual:txtLastName]){
        if(txtLastName.text.length < 1){
            lblLastName.textColor = [UIColor redColor];
            txtLastName.textColor = [UIColor redColor];
        }
    }
    else */
    if([textField isEqual:txtDOB]){
        if(![self hasAttemptedDOB:textField] && lblDOB){
            [lblDOB removeFromSuperview];
            lblDOB = nil;
            txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        [self removeDOBMask:textField];
    }
    else if([textField isEqual:txtEmail]){
        NSString *email = txtEmail.text;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            lblEmail.textColor = [UIColor redColor];
            txtEmail.textColor = [UIColor redColor];
        }
        else{
            NSDictionary *params = @{@"email":txtEmail.text};
            [PFCloud callFunctionInBackground:@"emailCheck" withParameters:params block:^(id object, NSError *error) {
                if(error == nil){
                    
                }
                else{
                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate showModal:@"A login with that email address already exists."];
                    ////NSLog(@"User with that email already exists");
                }
            }];
        }
    }
    else if([textField isEqual:txtConfirmEmail]){
        NSString *email = txtConfirmEmail.text;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            lblEmail.textColor = [UIColor redColor];
            lblConfirmEmail.textColor = [UIColor redColor];
        }
    }
    else if([textField isEqual:txtPhone]){
        if(txtPhone.text.length < 14)
        {
            lblPhone.textColor = [UIColor redColor];
            txtPhone.textColor = [UIColor redColor];
        }
    }
    else if([textField isEqual:txtPassword]){
        if(txtPassword.text.length < 6)
        {
            lblPassword.textColor = [UIColor redColor];
            txtPassword.textColor = [UIColor redColor];
        }
    }
    else if([textField isEqual:txtConfirmPassword]){
        if(txtConfirmPassword.text.length < 6 || ![txtPassword.text isEqualToString:txtConfirmPassword.text])
        {
            lblConfirmPassword.textColor = [UIColor redColor];
            txtConfirmPassword.textColor = [UIColor redColor];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtFirstName])
        [txtLastName becomeFirstResponder];
    else if([textField isEqual:txtLastName])
        [txtDOB becomeFirstResponder];
    else if([textField isEqual:txtDOB])
        [txtPhone becomeFirstResponder];
    else if([textField isEqual:txtPhone])
        [txtEmail becomeFirstResponder];
    else if([textField isEqual:txtEmail]){
        NSString *email = txtEmail.text;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            lblEmail.textColor = [UIColor redColor];
            txtEmail.textColor = [UIColor redColor];
            return NO;
        }
        else{
           [txtConfirmEmail becomeFirstResponder];
        }
        
        
    }
    else if([textField isEqual:txtConfirmEmail]){
        NSString *email = txtConfirmEmail.text;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            lblConfirmEmail.textColor = [UIColor redColor];
            txtConfirmEmail.textColor = [UIColor redColor];
            return NO;
        }
        else{
            if([txtEmail.text isEqualToString:txtConfirmEmail.text])
            {
                [txtPassword becomeFirstResponder];
            }
            else{
                lblConfirmEmail.textColor = [UIColor redColor];
                txtConfirmEmail.textColor = [UIColor redColor];
            }
        }
        
        
    }
    else if([textField isEqual:txtPassword])
        [txtConfirmPassword becomeFirstResponder];
    else if([textField isEqual:txtConfirmPassword]){
        [txtConfirmPassword resignFirstResponder];
        [self hideKeyboard];
    }
    return YES;
}

-(BOOL)canSubmit{
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    //UIColor *required = [UIColor colorWithRed:247.0/255.0 green:221.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *required = [UIColor redColor];
    BOOL shouldCheckRegex = YES;
    if(txtFirstName.text.length < 1 || txtLastName.text.length < 1){
        shouldCheckRegex = NO;
        
        if(txtFirstName.text.length == 0){
            //txtFirstName.backgroundColor = required;
            txtFirstName.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtFirstName.placeholder attributes:@{NSForegroundColorAttributeName:required}];
            
        }
        if (txtLastName.text.length == 0) {
            //txtLastName.backgroundColor = required;
            txtLastName.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtLastName.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
            
    }
    if(txtDOB.text.length > 14 || [txtDOB.text hasSuffix:@"Y"]){
        shouldCheckRegex = NO;
        //txtDOB.backgroundColor = required;
        if(txtDOB.text.length == 0){
            txtDOB.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtDOB.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblDOB.textColor = required;
            txtDOB.attributedText = [[NSMutableAttributedString alloc] initWithString:txtDOB.text attributes:@{NSForegroundColorAttributeName:required}];
        }
    }
    if(txtEmail.text.length < 6){
        shouldCheckRegex = NO;
        //txtEmail.backgroundColor = required;
        if (txtEmail.text.length == 0) {
           txtEmail.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtEmail.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblEmail.textColor = required;
            txtEmail.textColor = required;
        }
        
    }
    if (!editMode && (txtConfirmEmail.text.length < 6 || ![txtEmail.text isEqualToString:txtConfirmEmail.text])) {
        shouldCheckRegex = NO;
        if (txtConfirmEmail.text.length == 0) {
            txtConfirmEmail.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtConfirmEmail.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else if (![txtEmail.text isEqualToString:txtConfirmEmail.text]){
            lblEmail.textColor = required;
            txtEmail.textColor = required;
            lblConfirmEmail.textColor = required;
            txtConfirmEmail.textColor = required;
        }
        else{
            lblConfirmEmail.textColor = required;
            txtConfirmEmail.textColor = required;
        }
        
    }
    if(txtPhone.text.length < 14){
        shouldCheckRegex = NO;
        //txtPhone.backgroundColor = required;
        if(txtPhone.text.length == 0){
            txtPhone.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtPhone.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblPhone.textColor = required;
            txtPhone.textColor = required;
        }
    }
    if(!editMode && (txtPassword.text.length < 1 || txtConfirmPassword.text.length < 1))
    {
        shouldCheckRegex = NO;
        if(txtPassword.text.length == 0){
            //txtPassword.backgroundColor = required;
            txtPassword.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtPassword.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        if(txtConfirmPassword.text.length == 0){
            //txtConfirmPassword.backgroundColor = required;
            txtConfirmPassword.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtConfirmPassword.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
    }
    else if(![txtPassword.text isEqualToString: txtConfirmPassword.text]){
        shouldCheckRegex = NO;
        lblPassword.textColor = required;
        txtPassword.textColor = required;
        lblConfirmPassword.textColor = required;
        txtConfirmPassword.textColor = required;
    }
    
    //Add a default profile image if none is supplied.  Dec 1 release.
    
    if(shouldCheckRegex && ((!profileImage || profileImage.length == 0) && !editMode)){
        //shouldCheckRegex = NO;
        //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        //[appDelegate showModal:@"Please choose a profile image."];
        UIImage *imageCopy = [UIImage imageNamed:@"defaultProfile.png"];
        profileImage = [UIImagePNGRepresentation(imageCopy) base64EncodedStringWithOptions:kNilOptions];
        
    }
    
    if(shouldCheckRegex){
        BOOL allowedToSubmit = YES;
        
        //check regex here
        NSString *email = txtEmail.text;
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            allowedToSubmit = NO;
            //txtEmail.backgroundColor = required;
            lblEmail.textColor = required;
            txtEmail.textColor = required;
        }
        
        
        if(editMode){
            if(allowedToSubmit){
                //[self.navigationController popViewControllerAnimated:YES];
                lblFirstName.textColor = [UIColor lightGrayColor];
                lblLastName.textColor = [UIColor lightGrayColor];
                lblDOB.textColor = [UIColor lightGrayColor];
                lblPhone.textColor = [UIColor lightGrayColor];
                lblEmail.textColor = [UIColor lightGrayColor];
                lblPassword.textColor = [UIColor lightGrayColor];
                lblConfirmPassword.textColor = [UIColor lightGrayColor];
                
                txtFirstName.textColor = [UIColor blackColor];
                txtLastName.textColor = [UIColor blackColor];
                txtDOB.attributedText = [[NSMutableAttributedString alloc] initWithString:txtDOB.text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                txtPhone.textColor = [UIColor blackColor];
                txtEmail.textColor = [UIColor blackColor];
                txtPassword.textColor = [UIColor blackColor];
                txtConfirmPassword.textColor = [UIColor blackColor];
                //[self performSelector:@selector(hideKeyboard) withObject:nil afterDelay:0.1];
                return YES;
            }
            else{
                
            }
        }
        else{
            if(allowedToSubmit){
                lblFirstName.textColor = [UIColor lightGrayColor];
                lblLastName.textColor = [UIColor lightGrayColor];
                lblDOB.textColor = [UIColor lightGrayColor];
                lblPhone.textColor = [UIColor lightGrayColor];
                lblEmail.textColor = [UIColor lightGrayColor];
                lblPassword.textColor = [UIColor lightGrayColor];
                lblConfirmPassword.textColor = [UIColor lightGrayColor];
                
                txtFirstName.textColor = [UIColor blackColor];
                txtLastName.textColor = [UIColor blackColor];
                txtDOB.attributedText = [[NSMutableAttributedString alloc] initWithString:txtDOB.text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
                txtPhone.textColor = [UIColor blackColor];
                txtEmail.textColor = [UIColor blackColor];
                txtPassword.textColor = [UIColor blackColor];
                txtConfirmPassword.textColor = [UIColor blackColor];
                nextView.backgroundColor = [UIColor blackColor];
                //[self performSelector:@selector(hideKeyboard) withObject:nil afterDelay:0.1];
            }
            else{
                nextView.backgroundColor = [UIColor lightGrayColor];
            }
        }
        
        return allowedToSubmit;
    }
    else{
        nextView.backgroundColor = [UIColor lightGrayColor];
        return NO;
    }
    
}

-(IBAction)editProfileImage:(id)sender{
    ////NSLog(@"Editing profile image");
    if([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)]){
        //ios 8
        UIAlertController * view=   [UIAlertController
                                     alertControllerWithTitle:@"Profile Image"
                                     message:@"Choose Image Source"
                                     preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* camera = [UIAlertAction
                                 actionWithTitle:@"Camera"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here
                                     //camera
                                     @try
                                     {
                                         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                         picker.sourceType = UIImagePickerControllerCameraDeviceFront;
                                         picker.delegate = self;
                                         
                                         [self presentViewController:picker animated:YES completion:nil];
                                         //[picker release];
                                     }
                                     @catch (NSException *exception)
                                     {
                                         //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"Camera is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                         //[alert show];
                                         AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                         [appDelegate showModal:@"Camera is not available."];
                                         //[alert release];
                                     }
                                     //[view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        UIAlertAction* roll = [UIAlertAction
                               actionWithTitle:@"Camera Roll"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Do some thing here
                                   //camera roll
                                   @try
                                   {
                                       UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                       picker.delegate = self;
                                       
                                       [self presentViewController:picker animated:YES completion:nil];
                                       //[picker release];
                                   }
                                   @catch (NSException *exception)
                                   {
                                       //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Roll" message:@"Camera roll is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                       //[alert show];
                                       //[alert release];
                                       AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                       [appDelegate showModal:@"Camera roll is not available."];
                                   }
                                   //[view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        
        [view addAction:camera];
        [view addAction:roll];
        [view addAction:cancel];
        [self presentViewController:view animated:YES completion:nil];
        
    }
    else{
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Camera Roll", nil];
        [sheet showInView:self.view];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientPaymentInfoViewController class]]){
        PatientPaymentInfoViewController *vc = (PatientPaymentInfoViewController *)segue.destinationViewController;
        vc.signUpInfo = signUpInfo;
        vc.profileImage = profileImage;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([self canSubmit])
    {
        if(!editMode){
            signUpInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"firstName":txtFirstName.text,
                    @"lastName":txtLastName.text,
                    @"email":txtEmail.text,
                    @"username":txtEmail.text,
                    @"password":txtPassword.text,
                    @"phone":txtPhone.text,
                    @"dob":txtDOB.text
                    }];
            
            [signUpInfo setValue:[TOSInfo objectForKey:@"tosAccepted"] forKey:@"tosAccepted"];
            [signUpInfo setValue:[TOSInfo objectForKey:@"tosTimestamp"] forKey:@"tosTimestamp"];
            [signUpInfo setValue:[NPPInfo objectForKey:@"nppAccepted"] forKey:@"nppAccepted"];
            [signUpInfo setValue:[NPPInfo objectForKey:@"nppTimestamp"] forKey:@"nppTimestamp"];
        
            return YES;
        }
        else{
            //editing
            //update information here
            if(txtEmail.text.length > 5 && [txtEmail.text isEqualToString:txtConfirmEmail.text])
            {
                [PFUser currentUser].email = txtEmail.text;
                [PFUser currentUser].username = txtEmail.text;
            }
            
            [PFUser currentUser][@"firstName"] = txtFirstName.text;
            [PFUser currentUser][@"lastName"] = txtLastName.text;
            [PFUser currentUser][@"phone"] = txtPhone.text;
            
            if(txtPassword.text.length > 5 && [txtPassword.text isEqualToString:txtConfirmPassword.text])
            {
                [PFUser currentUser].password = txtPassword.text;
            }
            
            [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    
                    [User sharedUser].email = txtEmail.text;
                    [User sharedUser].username = txtEmail.text;
                    [User sharedUser].firstName = txtFirstName.text;
                    [User sharedUser].lastName = txtLastName.text;
                    [User sharedUser].phone = txtPhone.text;
                    [[User sharedUser] save];
                    
                    if([User sharedUser].customerID != nil){
                        PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
                        [custQuery getObjectInBackgroundWithId:[User sharedUser].customerID block:^(PFObject *customer, NSError *error2) {
                            if(!error2){
                                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                                [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                                [format setDateFormat:@"MM / dd / yyyy"];
                                customer[@"dob"] = [format dateFromString:txtDOB.text];
                                [customer saveInBackgroundWithBlock:^(BOOL succeeded2, NSError *error3) {
                                    if(succeeded2){
                                        //popup?
                                        [User sharedUser].dob = txtDOB.text;
                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataChanged" object:nil]; //we only call this because it does everything we want
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }
                                    else{
                                        ////NSLog(@"error: %@", error3);
                                        
                                    }
                                }];
                            }
                            else{
                                ////NSLog(@"error: %@", error2);
                            }
                            
                        }];
                    }
                    else if ([User sharedUser].providerID != nil){
                        PFQuery *physQuery = [PFQuery queryWithClassName:@"Provider"];
                        [physQuery getObjectInBackgroundWithId:[User sharedUser].providerID block:^(PFObject *physician, NSError *error2) {
                            if(!error2){
                                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                                [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                                [format setDateFormat:@"MM / dd / yyyy"];
                                physician[@"dob"] = [format dateFromString:txtDOB.text];
                                [physician saveInBackgroundWithBlock:^(BOOL succeeded2, NSError *error3) {
                                    if(succeeded2){
                                        //popup?
                                        [User sharedUser].dob = txtDOB.text;
                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataChanged" object:nil]; //we only call this because it does everything we want
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }
                                    else{
                                        ////NSLog(@"error: %@", error3);
                                        
                                    }
                                }];
                            }
                            else{
                                ////NSLog(@"error: %@", error2);
                            }
                            
                        }];
                    }
                    
                    
                    
                    
                }
                else{
                    ////NSLog(@"error: %@", error);
                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate showModal:@"Unable to update your profile at this time."];
                }
            }];
            
            return NO;
        }
    }
    else{
        return NO;
    }
    //return [self canSubmit];
    //return YES;
}

#pragma mark - UIButton methods
- (IBAction)addPhoto:(id)sender {
    ////NSLog(@"Add photo");
    
    if([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)]){
        //ios 8
        UIAlertController * view=   [UIAlertController
                                     alertControllerWithTitle:@"Profile Image"
                                     message:@"Choose Image Source"
                                     preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:@"Camera"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 //camera
                                 @try
                                 {
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.sourceType = UIImagePickerControllerCameraDeviceFront;
                                     picker.delegate = self;
                                     
                                     [self presentViewController:picker animated:YES completion:nil];
                                     //[picker release];
                                 }
                                 @catch (NSException *exception)
                                 {
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"Camera is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                     [alert show];
                                     //[alert release];
                                 }
                                 //[view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        UIAlertAction* roll = [UIAlertAction
                                 actionWithTitle:@"Camera Roll"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here
                                     //camera roll
                                     @try
                                     {
                                         
                                         [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:209.0/255.0 green:28.0/255.0 blue:24.0/255.0 alpha:1.0];
                                         //[UINavigationBar appearance].barTintColor = [UIColor redColor];
                                         [UINavigationBar appearance].translucent = NO;
                                         
                                         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                         picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                         picker.delegate = self;
                                         
                                         [self presentViewController:picker animated:YES completion:nil];
                                         //[picker release];
                                     }
                                     @catch (NSException *exception)
                                     {
                                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Roll" message:@"Camera roll is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                         [alert show];
                                         //[alert release];
                                     }
                                     //[view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        
        [view addAction:camera];
        [view addAction:roll];
        [view addAction:cancel];
        [self presentViewController:view animated:YES completion:nil];
        
    }
    else{
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Camera Roll", nil];
        [sheet showInView:self.view];
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != actionSheet.cancelButtonIndex){
        switch (buttonIndex) {
            case 0:
            {
                //camera
                @try
                {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
                    picker.delegate = self;
                    
                    [self presentViewController:picker animated:YES completion:nil];
                    //[picker release];
                }
                @catch (NSException *exception)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"Camera is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    //[alert release];
                }
                break;
            }
            case 1:
            {
                //camera roll
                @try
                {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    picker.delegate = self;
                    
                    [self presentViewController:picker animated:YES completion:nil];
                    //[picker release];
                }
                @catch (NSException *exception)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera Roll" message:@"Camera roll is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    //[alert release];
                }
                break;
            }
            default:
                break;
        }
    }
}

UIImage* UIImageCrop(UIImage* img, CGRect rect)
{
    CGAffineTransform rectTransform;
    switch (img.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(90 / 180.0 * M_PI), 0, -img.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-90 / 180.0 * M_PI), -img.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-180 / 180.0 * M_PI), -img.size.width, -img.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    rectTransform = CGAffineTransformScale(rectTransform, img.scale, img.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectApplyAffineTransform(rect, rectTransform));
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:img.scale orientation:img.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

-(UIImage*)imageCrop:(UIImage*)original
{
    UIImage *ret = nil;
    
    // This calculates the crop area.
    
    float originalWidth  = original.size.width;
    float originalHeight = original.size.height;
    
    float edge = fminf(originalWidth, originalHeight);
    
    float posX = (originalWidth   - edge) / 2.0f;
    float posY = (originalHeight  - edge) / 2.0f;
    
    CGRect cropSquare;
    
    if(original.imageOrientation == UIImageOrientationLeft ||
       original.imageOrientation == UIImageOrientationRight)
    {
        cropSquare = CGRectMake(posY, posX,
                                edge, edge);
        
    }
    else
    {
        cropSquare = CGRectMake(posX, posY,
                                edge, edge);
    }
    
    
    // This performs the image cropping.
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([original CGImage], cropSquare);
    
    ret = [UIImage imageWithCGImage:imageRef
                              scale:1.0
                        orientation:original.imageOrientation];
    
    CGImageRelease(imageRef);
    
    return ret;
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
    UIImage *cameraImage =[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    cameraImage = [self imageCrop:cameraImage];
    
    NSData *jpgData = UIImageJPEGRepresentation(cameraImage, 0.4);
    
    UIImage *jpegImage = [[UIImage alloc] initWithData:jpgData];
    
    //jpegImage = UIImageCrop(jpegImage, CGRectMake(0, 0, 95, 95));
    
    //jpegImage = [self imageCrop:jpegImage];
    
    UIGraphicsBeginImageContext(CGSizeMake(95, 95));
    [jpegImage drawInRect:CGRectMake(0.0, 0.0, 95, 95)];
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [btnAddPhoto setImage:imageCopy forState:UIControlStateNormal];
    
    btnAddPhoto.layer.cornerRadius = btnAddPhoto.frame.size.width/2;
    btnAddPhoto.clipsToBounds = YES;
    
    profileImage = [UIImagePNGRepresentation(imageCopy) base64EncodedStringWithOptions:kNilOptions];
    
    if(!editMode){
        lblAddPhoto.text = @"EDIT PHOTO";
        if(txtFirstName.text.length > 0 && txtLastName.text.length > 0 && txtDOB.text.length > 0 && txtEmail.text.length > 0 && txtConfirmEmail.text.length > 0 && txtPhone.text.length > 0 && txtPassword.text.length > 0 && txtConfirmPassword.text.length > 0){
            [self canSubmit];
        }
    }
    else{
        //edit mode
        //upload image here
        NSDictionary *avatarDict;
        
        if([User sharedUser].customerID != nil){
            avatarDict = @{@"id":[User sharedUser].customerID, @"type":@"customer", @"avatar":@{@"name":[User sharedUser].email ,@"file":profileImage}};
        }
        else if([User sharedUser].providerID != nil){
            avatarDict = @{@"id":[User sharedUser].providerID, @"type":@"provider", @"avatar":@{@"name":[User sharedUser].email ,@"file":profileImage}};
        }
        ////NSLog(@"avatar dictionary: %@", avatarDict);
        [PFCloud callFunctionInBackground:@"avatarUpload" withParameters:avatarDict block:^(id object, NSError *error) {
            if(error == nil){
                imgProfile.image = imageCopy;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDataChanged" object:nil];
                //good status code???
                //[self.navigationController popViewControllerAnimated:YES];
                //[self dismissViewControllerAnimated:YES completion:nil];
            }
            else{
                //nserror should have content
                ////NSLog(@"error: %@", error.description);
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to change your profile picture at this time."];
            }
        }];
    }

    
}

@end
