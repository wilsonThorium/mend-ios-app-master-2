//
//  ChargePatientViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ChargePatientViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSMutableDictionary *appointment;
@property (nonatomic,strong) UIImage *imgPatient;
@end
