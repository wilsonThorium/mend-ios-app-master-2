//
//  PatientLoginViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/1/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientLoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>




@interface PatientLoginViewController ()
{
    BOOL stayLoggedIn;
    UILabel *lblEmail;
    UILabel *lblPassword;
    IBOutlet UIImageView *logo;
    IBOutlet UILabel *motto;
    IBOutlet NSLayoutConstraint *logoTopPadding;
    IBOutlet NSLayoutConstraint *forgotTopPadding;
    BOOL keyboardUp;
    IBOutlet UILabel *lblNoAccount;
    IBOutlet UIView *footer;
}
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnStayLoggedIn;
- (IBAction)login:(id)sender;
- (IBAction)stayLoggedIn:(id)sender;
//- (IBAction)forgotPassword:(id)sender;

@end

@implementation PatientLoginViewController
@synthesize txtEmail, txtPassword, btnStayLoggedIn;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Patient Login";
    
    self.navigationController.navigationBarHidden = YES;
    
    if(self.view.frame.size.height < 500 || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
    {
        logoTopPadding.constant = -20;
    }
    

    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"ProviderLoggedIn" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    stayLoggedIn = YES;
    
    //DEC 1 Added Google Login
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    //DEC 1 Added Facebook Login
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = self.view.center;
    loginButton.readPermissions = @[@"public_profile", @"email"];
    [self.view addSubview:loginButton];
    
    if([FBSDKAccessToken currentAccessToken])
    {
        BOOL a = true;
        
    }
    
    
    
 
    
    txtEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtEmail.layer.borderWidth = 1.0f;
    txtEmail.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
    
    txtPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtPassword.layer.borderWidth = 1.0f;
    
    UIView *txtEmailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtEmail.leftView = txtEmailPadding;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *txtPasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    txtPassword.leftView = txtPasswordPadding;
    txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToTOS)];
    singleTap.numberOfTapsRequired = 1;
    [lblNoAccount addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToTOS)];
    singleTap2.numberOfTapsRequired = 1;
    [footer addGestureRecognizer:singleTap2];
    
    /*UITapGestureRecognizer *bgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    bgTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:bgTap];*/
    
    self.title = @"LOGIN";
    
    //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //[appDelegate showProcessingModal:@"Registering..."];
    // Do any additional setup after loading the view.
}

-(IBAction)unwindToLogin:(UIStoryboardSegue *)unwindSegue{
    
}


-(void)hideKeyboard{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    //[UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
    if(keyboardUp){
        logoTopPadding.constant += 60.0;
        forgotTopPadding.constant -= 60.0;
    //} completion:^(BOOL finished) {
        keyboardUp = NO;
    }
    //}];
}

-(void)goToTOS{
    [self performSegueWithIdentifier:@"segueShowTOS" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark = Google SignIn methods

-(void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:nil];

}
-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    [self presentViewController:viewController animated: YES completion:nil];
}


#pragma mark - UITextField methods


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(!keyboardUp){
        //[UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            logoTopPadding.constant -= 60.0;
            forgotTopPadding.constant += 60.0;
        //} completion:^(BOOL finished) {
            keyboardUp = YES;
        //}];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    /*[UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
        logoTopPadding.constant += 60.0;
        forgotTopPadding.constant -= 60.0;
    } completion:^(BOOL finished) {
        keyboardUp = NO;
    }];*/
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtEmail]){
        [txtPassword becomeFirstResponder];
    }
    else{
        [txtPassword resignFirstResponder];
        //[UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            logoTopPadding.constant += 60.0;
            forgotTopPadding.constant -= 60.0;
        //} completion:^(BOOL finished) {
            keyboardUp = NO;
        //}];
        [self login:self];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *textString = @"";
    if(string.length > 0)
    {
        //typing
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) {
            return NO;
        }
        
        textString = [NSString stringWithFormat:@"%@%@", textField.text, string];
        //////NSLog(@"Full string: %@", textString);
    }
    else{
        //backspace
        textString = [textField.text substringToIndex:textField.text.length - 1];
        //////NSLog(@"Full string: %@", textString);
        
        if([textField isEqual:txtEmail]){
            
        }
    }
    
    if([textField isEqual:txtEmail])
    {
        if(textString.length == 1 && !lblEmail){
            txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblEmail.textColor = [UIColor lightGrayColor];
            lblEmail.font = [UIFont systemFontOfSize:12.0];
            lblEmail.text = @"Email";
            [txtEmail addSubview:lblEmail];
        }
        else if (textString.length == 0 && lblEmail){
            [lblEmail removeFromSuperview];
            lblEmail = nil;
            txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
        
        if(lblEmail.textColor == [UIColor redColor]){
            NSString *email = [textString stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
            NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
            if(range.location != NSNotFound)
            {
                lblEmail.textColor = [UIColor lightGrayColor];
                txtEmail.textColor = [UIColor blackColor];
            }
        }
    }
    else if ([textField isEqual:txtPassword])
    {
        if(textString.length == 1 && !lblPassword){
            txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 5, 0);
            lblPassword = [[UILabel alloc] initWithFrame:CGRectMake(5.0, -2.0, 150.0, 14.0)];
            lblPassword.textColor = [UIColor lightGrayColor];
            lblPassword.font = [UIFont systemFontOfSize:12.0];
            lblPassword.text = @"Password";
            [txtPassword addSubview:lblPassword];
        }
        else if (textString.length == 0 && lblPassword){
            [lblPassword removeFromSuperview];
            lblPassword = nil;
            txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0);
        }
    }
    
    return YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}


-(BOOL)canSubmit{
    [NSThread cancelPreviousPerformRequestsWithTarget:self];
    
    //UIColor *required = [UIColor colorWithRed:247.0/255.0 green:221.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *required = [UIColor redColor];
    BOOL shouldCheckRegex = YES;
    if(txtEmail.text.length < 6){
        shouldCheckRegex = NO;
        //txtEmail.backgroundColor = required;
        if (txtEmail.text.length == 0) {
            txtEmail.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtEmail.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        }
        else{
            lblEmail.textColor = required;
            txtEmail.textColor = required;
        }
        
    }
    if(txtPassword.text.length < 1)
    {
        shouldCheckRegex = NO;
        
            //txtPassword.backgroundColor = required;
        txtPassword.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:txtPassword.placeholder attributes:@{NSForegroundColorAttributeName:required}];
        
    }
    
    if(shouldCheckRegex){
        BOOL allowedToSubmit = YES;
        
        //check regex here
        NSString *email = [txtEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *expression = @"^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:0 error:&error];
        NSRange range = [regex rangeOfFirstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
        if(range.location == NSNotFound)
        {
            allowedToSubmit = NO;
            //txtEmail.backgroundColor = required;
            lblEmail.textColor = required;
            txtEmail.textColor = required;
        }
    
    
        if(allowedToSubmit){
            lblEmail.textColor = [UIColor lightGrayColor];
            lblPassword.textColor = [UIColor lightGrayColor];
            txtEmail.textColor = [UIColor blackColor];
            txtPassword.textColor = [UIColor blackColor];
        }
        return allowedToSubmit;
    }
    else{
        return NO;
    }
}

- (IBAction)login:(id)sender {
    
    //PFQuery *query = [PFUser query];
    //[query whereKey:@"email" equalTo:txtEmail.text];
    //[query whereKey:@"password" equalTo:txtPassword.text];
    if([self canSubmit]){
        NSString *email = [txtEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [PFUser logInWithUsernameInBackground:email password:txtPassword.text block:^(PFUser *user, NSError *error) {
            
            if(!error){
                ////NSLog(@"Successfully retrieved the object: %@", user);
                if ([user objectForKey:@"softDelete"] != nil && [user[@"softDelete"] isEqual:@1]) {
                    //deleted account
                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate showModal:@"Your account has been deleted.  Please register again or contact Customer Service at (469) 458-6363."];
                }
                else{
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    
                    if(stayLoggedIn)
                    {
                        [defaults setValue:@1 forKey:@"stayloggedin"];
                    }
                    else{
                        [defaults setValue:@0 forKey:@"stayloggedin"];
                    }
                    
                    [defaults setValue:@1 forKey:@"temporarylogin"];
                    
                    [defaults synchronize];
                    
                    [User sharedUser].objectID = user.objectId;
                    [User sharedUser].email = user.email;
                    [User sharedUser].username = user.username;
                    [User sharedUser].firstName = user[@"firstName"];
                    [User sharedUser].lastName = user[@"lastName"];
                    [User sharedUser].phone = user[@"phone"];
                    [[User sharedUser] save];
                    
                    PFQuery *userQuery = [PFUser query];
                    [userQuery whereKey:@"objectId" equalTo:user.objectId];
                    
                    PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
                    [custQuery whereKey:@"user" matchesQuery:userQuery];
                    [custQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                        if (error) {
                            ////NSLog(@"The getFirstObject request failed.");
                            
                            //try and see if they are a provider
                            PFQuery *providerQuery = [PFQuery queryWithClassName:@"Provider"];
                            [providerQuery whereKey:@"user" matchesQuery:userQuery];
                            [providerQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                if (error) {
                                    ////NSLog(@"The getFirstObject request failed.");
                                    
                                    //they are neither a customer or a provider
                                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                    [appDelegate showModal:@"Account is in a bad state.  Please contact us to resolve this issue."];
                                    
                                } else {
                                    // The find succeeded.
                                    //this is a provider
                                    [User sharedUser].providerID = object.objectId;
                                    [[User sharedUser] save];
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ProviderLoggedIn" object:nil];
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }
                            }];
                            
                        } else {
                            // The find succeeded.
                            //this is a customer
                            [User sharedUser].customerID = object.objectId;
                            NSDateFormatter *format = [[NSDateFormatter alloc] init];
                            [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                            [format setDateFormat:@"MM / dd / yyyy"];
                            [User sharedUser].dob = [format stringFromDate:[object objectForKey:@"dob"]];
                            [[User sharedUser] save];
                            [User sharedUser].loggedIn = true;
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerLoggedIn" object:nil];
                        }
                    }];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                
            }
            else{
                ////NSLog(@"Error: %@", error.description);
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Your login was unsuccessful.  Please try again."];
            }
        }];
    }
}

- (IBAction)stayLoggedIn:(id)sender {
    if(!stayLoggedIn)
        [btnStayLoggedIn setImage:[UIImage imageNamed:@"icon_checked"] forState:UIControlStateNormal];
    else
        [btnStayLoggedIn setImage:[UIImage imageNamed:@"icon_unchecked"] forState:UIControlStateNormal];
    
    stayLoggedIn = !stayLoggedIn;
}
@end
