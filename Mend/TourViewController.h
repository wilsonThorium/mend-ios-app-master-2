//
//  TourViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/18/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface TourViewController : GAITrackedViewController <UIScrollViewDelegate>
{
    
}
@property(nonatomic,assign) BOOL fromProfile;
@end
