//
//  PhysicianDashboardViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/2/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PhysicianDashboardViewController.h"
#import "Arc.h"
#import "PatientActionViewController.h"
#import "ChargePatientViewController.h"

@interface PhysicianDashboardViewController ()
{
    NSMutableArray *todaysDisplayAppointments;
    NSMutableArray *todaysAppointments;
    NSMutableArray *tomorrowsDisplayAppointments;
    NSMutableArray *tomorrowsAppointments;
    IBOutlet UIView *chargeView;
    IBOutlet UILabel *lblChargePatientName;
    IBOutlet UILabel *lblChargePatientTime;
    IBOutlet MKMapView *dashboardMap;
    IBOutlet UITableView *appointmentTable;
    IBOutlet UIButton *btnCharge;
    IBOutlet UIImageView *imgPatient;
    IBOutlet UIView *tableCircle;
    
    IBOutlet UIImageView *nextPatientProfileImage;
    IBOutlet UIView *nextPatientView;
    IBOutlet UILabel *lblNextPatientName;
    
    IBOutlet UILabel *lblNextAppointmentDistance;
    IBOutlet UILabel *lblNextAppointmentTime;
    CLLocation *lastLocation;
    
    id selectedAppointment;
    
    IBOutlet UISegmentedControl *segDay;
    
    NSDate *tomorrowDate;
    
    NSInteger selectedSegment;
    
    NSString *nextPhone;
    
    id chargeAppointment;
    id nextAppointment;
    
    BOOL hasReceivedFirstPoint;
    
    NSTimer *getAppointmentsTimer;
}
@end

@implementation PhysicianDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    getAppointmentsTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(getAppointments) userInfo:nil repeats:YES];
    
    hasReceivedFirstPoint = NO;
    
    selectedSegment = 0;
    nextPhone = 0;
    
    [GPS sharedGPS].delegate = self;
    
    //nextPhone = @"5012073399";
    //[self callNextCustomer:self];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    todaysDisplayAppointments = [[NSMutableArray alloc] init];
    todaysAppointments = [[NSMutableArray alloc] init];
    tomorrowsDisplayAppointments = [[NSMutableArray alloc] init];
    tomorrowsAppointments = [[NSMutableArray alloc] init];
    //2.1, 1.1, 4.15
    tableCircle.layer.cornerRadius = tableCircle.frame.size.width/4.15;
    //tableCircle.layer.zPosition =  10000;
    tableCircle.backgroundColor = [UIColor whiteColor];
    
    nextPatientView.layer.cornerRadius = 3;
    nextPatientView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    nextPatientView.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    nextPatientView.layer.shadowOpacity = 0.7;
    nextPatientView.layer.shadowRadius = 1;
    
    nextPatientProfileImage.layer.cornerRadius = nextPatientProfileImage.frame.size.width/2;
    nextPatientProfileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    nextPatientProfileImage.layer.borderWidth = 3.0;
    nextPatientProfileImage.clipsToBounds = YES;
    
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.043;
    span.longitudeDelta=0.043;
    //32.782572, -96.802236
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(32.865482, -96.786903);
    
    region.span=span;
    region.center=coordinate;
    
    [dashboardMap setRegion:region animated:TRUE];
    [dashboardMap regionThatFits:region];
    dashboardMap.userInteractionEnabled = NO;
    
    UIButton *btnSettings = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 20.0, 20.0)];
    [btnSettings setImage:[UIImage imageNamed:@"icon_gear"] forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    
    [btnCharge setBackgroundColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    btnCharge.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    btnCharge.layer.borderColor = [UIColor whiteColor].CGColor;
    btnCharge.layer.borderWidth = 0.5f;
    btnCharge.layer.cornerRadius = 5.0f;
    
    imgPatient.layer.cornerRadius = imgPatient.frame.size.width/2;
    imgPatient.clipsToBounds = YES;
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    tomorrowDate = [gregorian dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    //[self getAppointments];
    
    
    // Do any additional setup after loading the view.
    
    appointmentTable.layer.zPosition = 1000;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AppointmentCancelled" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        selectedSegment = 0;
        [self getAppointments];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UserLoggedOut" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if(getAppointmentsTimer.isValid){
            [getAppointmentsTimer invalidate];
            getAppointmentsTimer = nil;
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CustomerCharged" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        selectedSegment = 0;
        [self getAppointments];
    }];
}

-(void)newLocation:(CLLocation *)location{
    if(!hasReceivedFirstPoint){
        hasReceivedFirstPoint = YES;
        [self getAppointments];
    }
}


-(void)getAppointments{
    BOOL testing = NO;
    if(testing){
        todaysDisplayAppointments = [[NSMutableArray alloc] initWithArray:@[@{@"time":@"12:30 PM",@"name":@"Andrew Goodwin",@"phone":@"(501) 207-2336",@"distance":@"4.21 mi"},@{@"time":@"12:30 PM",@"name":@"Andrew Goodwin",@"phone":@"(501) 207-2336",@"distance":@"4.21 mi"},@{@"time":@"12:30 PM",@"name":@"Andrew Goodwin",@"phone":@"(501) 207-2336",@"distance":@"4.21 mi"}]];
    }
    else{
        PFQuery *providerQuery = [PFQuery queryWithClassName:@"Provider"];
        [providerQuery whereKey:@"objectId" equalTo:[User sharedUser].providerID];
        
        PFQuery *apptQuery = [PFQuery queryWithClassName:@"Appointment"];
        [apptQuery whereKey:@"provider" matchesQuery:providerQuery];
        [apptQuery whereKeyDoesNotExist:@"softDelete"];
        [apptQuery includeKey:@"provider"];
        [apptQuery includeKey:@"customer.user"];
        
        NSDate *today = [NSDate date];
        NSDate *twoDaysAgo = [today dateByAddingTimeInterval:-60 * 60 * 24 * 2];
        
        [apptQuery whereKey:@"time" greaterThanOrEqualTo:twoDaysAgo];
        
        [apptQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                ////NSLog(@"The getObjects request failed.");
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to get appointments at this time."];
            } else {
                // The find succeeded.
                ////NSLog(@"objects: %@", objects);
                ////NSLog(@"results[0].provider: %@", [[[objects objectAtIndex:0] objectForKey:@"provider"] objectForKey:@"user"]);
                //time, name, phone
                NSMutableArray *allAppointments = [[NSMutableArray alloc] initWithArray:objects];
                
                todaysDisplayAppointments = [[NSMutableArray alloc] init];
                todaysAppointments = [[NSMutableArray alloc] init];
                tomorrowsDisplayAppointments = [[NSMutableArray alloc] init];
                tomorrowsAppointments = [[NSMutableArray alloc] init];
                
                NSDateComponents *todayComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                
                NSDateComponents *tomorrowComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tomorrowDate];
                
                NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
                //[df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                [df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
                
                NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
                [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
                [df_local setDateFormat:@"yyyy.MM.dd HH:mm:ss zzz"];
                
                NSDateFormatter *time = [[NSDateFormatter alloc] init];
                [time setDateFormat:@"h:mm"];
                
                NSDateFormatter *ampm = [[NSDateFormatter alloc] init];
                [ampm setDateFormat:@"a"];
                
                
                [allAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    //convert all dates to local time from UTC
                    NSDate *date = [obj objectForKey:@"time"];
                    //NSString *s = [(NSString *)theDate stringByReplacingOccurrencesOfString:@":" withString:@""];
                    //NSDate *date = [df_utc dateFromString:s];
                    if(date){
                        NSString *localDateTime = [df_local stringFromDate:date];
                        NSDate *localDate = [df_local dateFromString:localDateTime];
                        NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:localDate];
                        
                        NSDateFormatter *appointmentDate = [[NSDateFormatter alloc] init];
                        [appointmentDate setDateFormat:@"yyyy-MM-dd HH:mm:sszzz"]; //2014-12-12 00:00:00CST
                        //NSString *apptDateString = [appointmentDate stringFromDate:date];
                        ////NSLog(@"Date: %@", date.description);
                        ////NSLog(@"Local Date: %@", localDate.description);
                        ////NSLog(@"Local Date String: %@", localDateTime);
                        //dateComps.timeZone = [NSTimeZone timeZoneWithName:@"CST"];
                        
                        NSDateFormatter* scheduleFormat = [[NSDateFormatter alloc] init];
                        [scheduleFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                        [scheduleFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.'000Z'"];
                        //seperate by day
                        
                        __block PFGeoPoint *userLocation = [PFGeoPoint geoPointWithLocation:[GPS sharedGPS].lastLocation];
                        
                        if(todayComps.day == dateComps.day && todayComps.month == dateComps.month && todayComps.year == dateComps.year)
                        {
                            //NSDictionary *appt = @{@"date":[scheduleFormat stringFromDate:localDate], @"time":[time stringFromDate:localDate], @"ampm":[ampm stringFromDate:localDate], @"day":@"TODAY"};
                            
                            [todaysAppointments addObject:obj];
                            
                            
                            //[objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                id user = [[obj objectForKey:@"customer"] objectForKey:@"user"];
                                NSString *name = [NSString stringWithFormat:@"%@ %@",[user objectForKey:@"firstName"], [user objectForKey:@"lastName"]];
                                //NSLog(@"name: %@", name);
                                NSString *phone = [user objectForKey:@"phone"];
                                NSString *dateString = [self getLocalDate:[obj objectForKey:@"time"]];
                                
                                PFGeoPoint *apptLocation = [obj objectForKey:@"location"];
                                
                                double miles = [userLocation distanceInMilesTo:apptLocation];
                                
                                
                            id charged = [obj objectForKey:@"stripeCharge"];
                            if(charged == nil){
                                [todaysDisplayAppointments addObject:@{@"time":dateString,@"name":name,@"distance":[NSString stringWithFormat:@"%.1f mi", miles],@"phone":phone,@"sortDate":[obj objectForKey:@"time"],@"sortCharge":@NO, @"id":[obj valueForKey:@"objectId"]}];
                            }
                            else{
                                [todaysDisplayAppointments addObject:@{@"time":dateString,@"name":name,@"distance":[NSString stringWithFormat:@"%.1f mi", miles],@"phone":phone,@"sortDate":[obj objectForKey:@"time"],@"sortCharge":@YES,@"id":[obj valueForKey:@"objectId"]}];
                            }
                            
                            
                            //might need to add customerId and userId
                            //}];
                        }
                        else if (tomorrowComps.day == dateComps.day && tomorrowComps.month == dateComps.month && tomorrowComps.year == dateComps.year){
                            
                            [tomorrowsAppointments addObject:obj];
                            
                            id user = [[obj objectForKey:@"customer"] objectForKey:@"user"];
                            NSString *name = [NSString stringWithFormat:@"%@ %@",[user objectForKey:@"firstName"], [user objectForKey:@"lastName"]];
                            //NSLog(@"name: %@", name);
                            NSString *phone = [user objectForKey:@"phone"];
                            NSString *dateString = [self getLocalDate:[obj objectForKey:@"time"]];
                            
                            PFGeoPoint *apptLocation = [obj objectForKey:@"location"];
                            
                            double miles = [userLocation distanceInMilesTo:apptLocation];
                            
                            id charged = [obj objectForKey:@"stripeCharge"];
                            if(charged == nil){
                                [tomorrowsDisplayAppointments addObject:@{@"time":dateString,@"name":name,@"distance":[NSString stringWithFormat:@"%.1f mi", miles],@"phone":phone,@"sortDate":[obj objectForKey:@"time"],@"sortCharge":@NO,@"id":[obj valueForKey:@"objectId"]}];
                            }
                            else{
                                [tomorrowsDisplayAppointments addObject:@{@"time":dateString,@"name":name,@"distance":[NSString stringWithFormat:@"%.1f mi", miles],@"phone":phone,@"sortDate":[obj objectForKey:@"time"],@"sortCharge":@YES,@"id":[obj valueForKey:@"objectId"]}];
                            }
                        }
                    }
                    
                    //convert from utc to cst
                    //store time and ampm in dictionary
                    [appointmentTable reloadData];
                }];
                //TODO:sort array based on charged or not and then time
                
                NSMutableArray *bufferArray = [[NSMutableArray alloc] initWithArray:todaysDisplayAppointments];
                NSSortDescriptor *sortCharge = [[NSSortDescriptor alloc] initWithKey:@"sortCharge" ascending:YES];
                NSSortDescriptor *sortDate = [[NSSortDescriptor alloc] initWithKey:@"sortDate" ascending:YES];
                
                NSSortDescriptor *sortStripe = [[NSSortDescriptor alloc] initWithKey:@"stripeCharge" ascending:YES];
                NSSortDescriptor *sortTime = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
                
                
                [bufferArray sortUsingDescriptors:@[sortCharge, sortDate]];
                todaysDisplayAppointments = bufferArray;
                
                bufferArray = [[NSMutableArray alloc] initWithArray:tomorrowsDisplayAppointments];
                [bufferArray sortUsingDescriptors:@[sortCharge, sortDate]];
                tomorrowsDisplayAppointments = bufferArray;
                
                bufferArray = [[NSMutableArray alloc] initWithArray:todaysAppointments];
                [bufferArray sortUsingDescriptors:@[sortStripe, sortTime]];
                todaysAppointments = bufferArray;
                
                bufferArray = [[NSMutableArray alloc] initWithArray:tomorrowsAppointments];
                [bufferArray sortUsingDescriptors:@[sortStripe, sortTime]];
                tomorrowsAppointments = bufferArray;
                
                [appointmentTable reloadData];
                
                chargeView.hidden = YES;
                
                __block BOOL foundNext = NO;
                [todaysAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if(!foundNext){
                        id Appointment = obj;
                        
                        id customer = [obj objectForKey:@"customer"];
                        id user = [customer objectForKey:@"user"];
                        NSString *name = [NSString stringWithFormat:@"%@ %@",[user objectForKey:@"firstName"], [user objectForKey:@"lastName"]];
                        //NSLog(@"name: %@", name);
                        //NSString *phone = [user objectForKey:@"phone"];
                        NSString *dateString = [self getLocalDate:[obj objectForKey:@"time"]];
                        
                        NSDate *appointmentTime = [Appointment objectForKey:@"time"];
                        if([[NSDate date] compare:appointmentTime] == NSOrderedDescending){
                            //now is later than appointment
                            if([Appointment objectForKey:@"stripeCharge"] == nil)
                            {
                                //old visit needs to be charged
                               chargeAppointment = Appointment;
                               chargeView.hidden = NO;
                               lblChargePatientName.text = name;
                               lblChargePatientTime.text = dateString;
                               //call for patient image
                                PFQuery *query = [PFQuery queryWithClassName:@"Customer"];
                                
                                // Retrieve the object by id
                                [query getObjectInBackgroundWithId:[customer valueForKey:@"objectId"] block:^(PFObject *customer, NSError *error) {
                                    if(!error){
                                        PFFile *userImageFile = customer[@"avatar"];
                                        [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                                            if (!error) {
                                                imgPatient.image = [UIImage imageWithData:imageData];
                                            }
                                        }];
                                        
                                    }
                                    else{
                                        ////NSLog(@"Error retrieving profile image");
                                    }
                                }];
                            }
                        }
                        else if([Appointment objectForKey:@"stripeCharge"] == nil)
                        {
                            nextAppointment = Appointment;
                            foundNext = YES;
                            lblNextPatientName.text = name;
                            lblNextAppointmentTime.text = dateString;
                            
                            id todaysDisplay = [todaysDisplayAppointments objectAtIndex:idx];
                            
                            lblNextAppointmentDistance.text = [todaysDisplay objectForKey:@"distance"];
                            nextPhone = [todaysDisplay objectForKey:@"phone"];
                            
                            PFQuery *query = [PFQuery queryWithClassName:@"Customer"];
                            
                            // Retrieve the object by id
                            [query getObjectInBackgroundWithId:[customer valueForKey:@"objectId"] block:^(PFObject *customer, NSError *error) {
                                if(!error){
                                    PFFile *userImageFile = customer[@"avatar"];
                                    [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                                        if (!error) {
                                            nextPatientProfileImage.image = [UIImage imageWithData:imageData];
                                        }
                                    }];
                                    
                                }
                                else{
                                    ////NSLog(@"Error retrieving profile image");
                                }
                            }];
                            *stop = YES;
                        }
                    }
                    else{
                        //*stop = YES;
                    }
                }];
                
                if(!foundNext){
                    nextPatientView.hidden = YES;
                    nextPatientProfileImage.hidden = YES;
                }
                else{
                    nextPatientView.hidden = NO;
                    nextPatientProfileImage.hidden = NO;
                }
                
            }
        }];
    }
}

-(NSString *)getLocalDate:(NSDate *)utc{
    
    //NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //[df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"h:mm a"];
    
    NSString *localDateTime = [df_local stringFromDate:utc];
    /*NSDate *localDate = [df_local dateFromString:localDateTime];
    
    NSDateFormatter* timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [timeFormat setDateFormat:@"h:mm a"];*/
    
    return localDateTime;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Physician Dashboard";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //self.title = @"MEND";
    //[self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToProfile:(id)sender{
    [self performSegueWithIdentifier:@"showProfileSegue" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(segDay == nil)
        return 0;
    if(segDay.selectedSegmentIndex == 0 && todaysDisplayAppointments.count > 0)
        return todaysDisplayAppointments.count;
    else if(segDay.selectedSegmentIndex == 1 && tomorrowsDisplayAppointments.count > 0)
        return tomorrowsDisplayAppointments.count;
    else
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(segDay.selectedSegmentIndex == 0 && todaysDisplayAppointments.count > 0)
        return 60.0;
    else if(segDay.selectedSegmentIndex == 1 && tomorrowsDisplayAppointments.count > 0)
        return 60.0;
    else
        return tableView.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((segDay.selectedSegmentIndex == 0 && todaysDisplayAppointments.count == 0) || (segDay.selectedSegmentIndex == 1 && tomorrowsDisplayAppointments.count == 0)){
        static NSInteger const kNoAppointments = 100;
        UILabel *lblNoAppointments = nil;
        
        static NSString *CellIdentifier = @"noAppointments";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        else{
            lblNoAppointments = (UILabel *)[cell viewWithTag:kNoAppointments];
        }
        
        lblNoAppointments.text = @"No appointments scheduled.";
        
        return cell;
    }
    else{
        static NSInteger const kTime = 100;
        static NSInteger const kName = 200;
        static NSInteger const kPhone = 300;
        static NSInteger const kDistance = 400;
        static NSInteger const kCheck = 400;
        
        UILabel *lblTime = nil;
        UILabel *lblName = nil;
        UILabel *lblPhone = nil;
        UILabel *lblDistance = nil;
        UIImageView *imgCheck = nil;
        
        id Appointment;
        if(segDay == nil || segDay.selectedSegmentIndex == 0)
        {
            Appointment = [todaysDisplayAppointments objectAtIndex:indexPath.row];
        }
        else{
            Appointment = [tomorrowsDisplayAppointments objectAtIndex:indexPath.row];
        }
        
        
        //if appointment is in the past
        id charged = [Appointment objectForKey:@"sortCharge"];
        if(charged != nil && [charged isEqual:@1]){
            static NSString *CellIdentifier = @"pastAppointment";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            else{
                lblTime = (UILabel *)[cell viewWithTag:kTime];
                lblName = (UILabel *)[cell viewWithTag:kName];
                lblPhone = (UILabel *)[cell viewWithTag:kPhone];
                imgCheck = (UIImageView *)[cell viewWithTag:kCheck];
            }
            
            //configure row here
            lblTime.text = [Appointment objectForKey:@"time"];
            lblName.text = [Appointment objectForKey:@"name"];
            lblPhone.text = [Appointment objectForKey:@"phone"];
            //lblDistance.text = [Appointment objectForKey:@"distance"];
            
            return cell;
        }
        else{
            static NSString *CellIdentifier = @"futureAppointment";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            else{
                lblTime = (UILabel *)[cell viewWithTag:kTime];
                lblName = (UILabel *)[cell viewWithTag:kName];
                lblPhone = (UILabel *)[cell viewWithTag:kPhone];
                lblDistance = (UILabel *)[cell viewWithTag:kDistance];
            }
            
            //configure row here
            lblTime.text = [Appointment objectForKey:@"time"];
            lblName.text = [Appointment objectForKey:@"name"];
            lblPhone.text = [Appointment objectForKey:@"phone"];
            lblDistance.text = [Appointment objectForKey:@"distance"];
            
            return cell;
        }
    }
    
    
    
    
    /*id History = [appointments objectAtIndex:indexPath.row];
    
    lblDate.text = [History objectForKey:@"date"];
    lblLocation.text = [History objectForKey:@"location"];
    lblPrice.text = [History objectForKey:@"price"];*/
    
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width * 3, 50.0)];
    header.backgroundColor = [UIColor whiteColor];
    //header.layer.opacity = 0;
    
    UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width * 3, 30.0)];
    circle.backgroundColor = [UIColor whiteColor];
    circle.layer.cornerRadius = circle.frame.size.width/2;
    circle.clipsToBounds = YES;
    //[header addSubview:circle];
    
    segDay = [[UISegmentedControl alloc] initWithItems:@[@"TODAY",@"TOMORROW"]];
    segDay.frame = CGRectMake(10.0, 5.0, tableView.frame.size.width - 20.0, 45.0);
    segDay.tintColor = [UIColor blackColor];
    segDay.selectedSegmentIndex = selectedSegment;
    [segDay addTarget:self action:@selector(selectedDay:) forControlEvents:UIControlEventValueChanged];
    [header addSubview:segDay];
    
    return header;
}*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //id Appointment;
    if(segDay == nil || (segDay.selectedSegmentIndex == 0 && todaysDisplayAppointments.count > 0))
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        id appt = [todaysDisplayAppointments objectAtIndex:indexPath.row];
        __block NSUInteger index = -1;
        [todaysAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj valueForKey:@"objectId"] == [appt valueForKey:@"id"]){
                index = idx;
            }
        }];
        selectedAppointment = [todaysAppointments objectAtIndex:index];
        if([selectedAppointment objectForKey:@"stripeCharge"])
        {
            
        }
        else{
            [self performSegueWithIdentifier:@"segueShowVisit" sender:self];
        }
    }
    else if (segDay.selectedSegmentIndex == 1 && tomorrowsDisplayAppointments.count > 0){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        id appt = [tomorrowsDisplayAppointments objectAtIndex:indexPath.row];
        __block NSUInteger index = -1;
        [tomorrowsAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj valueForKey:@"objectId"] == [appt valueForKey:@"id"]){
                index = idx;
            }
        }];
        selectedAppointment = [tomorrowsAppointments objectAtIndex:index];
        if([selectedAppointment objectForKey:@"stripeCharge"])
        {
            
        }
        else{
            [self performSegueWithIdentifier:@"segueShowVisit" sender:self];
        }
    }
    else{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
   
}

-(IBAction)selectedDay:(id)sender{
    if(segDay.selectedSegmentIndex == 0)
    {
        //today's appointments
    }
    else{
        //tomorrow's appointments
    }
    selectedSegment = segDay.selectedSegmentIndex;
    [appointmentTable reloadData];
}

- (IBAction)chargeCustomer:(id)sender {
    //selectedAppointment = chargeAppointment;
    //[self performSegueWithIdentifier:@"segueChargePatient" sender:self];
}

-(IBAction)goToVisit:(id)sender{
    selectedAppointment = nextAppointment;
    [self performSegueWithIdentifier:@"segueShowVisit" sender:self];
}

- (IBAction)callNextCustomer:(id)sender {
    //NSString *phNo = nextPhone;
    NSMutableString *phNo = [[NSMutableString alloc] initWithString:nextPhone];
    [phNo replaceOccurrencesOfString:@"(" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@")" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@"-" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:[NSString  stringWithFormat:@"*67%@",phNo]];
        exit(-1);
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:"]];
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"This device does not support making phone calls." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)logout:(id)sender {
    [[User sharedUser] logout];
    [self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientActionViewController class]])
    {
        PatientActionViewController *vc = (PatientActionViewController *)segue.destinationViewController;
        if(selectedAppointment != nil){
            vc.appointment = selectedAppointment;
            vc.imgPatient = nil;
        }
        else{
            vc.appointment = nextAppointment;
            vc.imgPatient = nextPatientProfileImage.image;
        }
    }
    else if([segue.destinationViewController isKindOfClass:[ChargePatientViewController class]])
    {
        ChargePatientViewController *vc = (ChargePatientViewController *)segue.destinationViewController;
        vc.appointment = chargeAppointment;
        vc.imgPatient = imgPatient.image;
    }
}


@end
