//
//  PatientLocationViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "GPS.h"
#import "GAITrackedViewController.h"

@interface PatientLocationViewController : GAITrackedViewController <UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    
}
@property(nonatomic, assign) BOOL modal;
@property(nonatomic, assign) BOOL editMode;
@property (nonatomic, strong) NSString *profileImage;
@property (nonatomic, strong) NSDictionary *signUpInfo;
@property (nonatomic, strong) NSString *stripeID;
@property (nonatomic, strong) id AddressToEdit;
@end
