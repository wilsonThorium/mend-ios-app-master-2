//
//  PatientLocationIntermediateViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/16/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GAITrackedViewController.h"

@interface PatientLocationIntermediateViewController : GAITrackedViewController
@property (nonatomic, strong) NSString *profileImage;
@property (nonatomic, strong) NSDictionary *signUpInfo;
@property (nonatomic, strong) NSString *stripeID;
@end
