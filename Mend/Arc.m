//
//  Arc.m
//  Mend
//
//  Created by Andrew Goodwin on 2/6/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "Arc.h"

@implementation Arc


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //CGContextSetLineWidth(context, 2.0);
    
    //CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextMoveToPoint(context, 0, rect.origin.y);
    
    //CGContextAddQuadCurveToPoint(context, 160, 10, 320, 200);
    
    CGContextAddQuadCurveToPoint(context, rect.size.width/2, 10, 320, rect.origin.y);
    
    CGContextClosePath(context);
    
    //CGContextStrokePath(context);
    
    
    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillPath(context);
    CGContextScaleCTM(context, 1.0, -1.0);
}


@end
