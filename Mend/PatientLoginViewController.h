//
//  PatientLoginViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/1/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import <Google/SignIn.h>


@interface PatientLoginViewController : GAITrackedViewController <UITextFieldDelegate, GIDSignInUIDelegate>
@property (weak, nonatomic) IBOutlet GIDSignInButton *signInButtonGoogle;
-(IBAction)unwindToLogin:(UIStoryboardSegue *)unwindSegue;
@end
