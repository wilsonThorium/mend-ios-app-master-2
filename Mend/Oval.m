//
//  Oval.m
//  Mend
//
//  Created by Andrew Goodwin on 2/6/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "Oval.h"

@implementation Oval


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGRect rectangle = CGRectMake(60,170,200,80);
    
    CGContextAddEllipseInRect(context, rectangle);
    
    CGContextStrokePath(context);
}


@end
