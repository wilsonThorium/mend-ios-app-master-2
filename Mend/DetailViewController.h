//
//  DetailViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 12/14/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

