//
//  GPS.h
//  Soteria
//
//  Created by Andrew Goodwin on 11/9/14.
//  Copyright (c) 2014 joebell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol GPSDelegate <NSObject>

@optional
-(void)newLocation:(CLLocation *)location;
-(void)GPSError:(NSString *)error;

@end

@interface GPS : NSObject <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    
    id __unsafe_unretained delegate;
    PFObject *tracker;
    BOOL hasBeenOffline;
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    }
@property(nonatomic, assign) id __unsafe_unretained delegate;
@property(nonatomic, assign) BOOL isDriving;
@property(nonatomic, strong) CLLocation *lastLocation;
@property(nonatomic, strong) CLPlacemark *setLocation;


-(void)increaseAccuracy;
-(void)decreaseAccuracy;

+(GPS *)sharedGPS;

@end
