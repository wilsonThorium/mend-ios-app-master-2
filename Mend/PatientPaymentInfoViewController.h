//
//  PatientPaymentInfoViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stripe.h"
#import "Stripe+ApplePay.h"
#import "GAITrackedViewController.h"

@interface PatientPaymentInfoViewController : GAITrackedViewController
{
   
}
@property (nonatomic, strong) NSString *profileImage;
@property (nonatomic, strong) NSDictionary *signUpInfo;
@property(nonatomic, assign) BOOL editMode;
@end
