//
//  Appointment.h
//  Mend
//
//  Created by Andrew Goodwin on 2/18/15.
//  Copyright (c) 2015 Few. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Appointment : NSObject
{
    NSTimer *countdownTimer;
}
@property(nonatomic, strong) NSString *appointmentID;
@property(nonatomic, strong) NSString *day;
@property(nonatomic, strong) NSString *time;
@property(nonatomic, strong) NSString *address1;
@property(nonatomic, strong) NSString *address2;
@property(nonatomic, strong) NSString *physician;
@property(nonatomic, strong) NSString *physicianTitle;
@property(nonatomic, strong) NSData   *physicianImage;
@property(nonatomic, strong) NSDate   *appointmentDate;
@property(nonatomic, strong) NSDate   *cancelByDate;
@property(nonatomic, strong) NSDate   *createdDate;
+(Appointment *)currentAppointment;
-(void)save;
-(void)cancel;
-(void)startTimer;
@end
