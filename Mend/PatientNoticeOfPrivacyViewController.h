//
//  PatientNoticeOfPrivacyViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/21/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PatientNoticeOfPrivacyViewController : GAITrackedViewController
@property(nonatomic, strong) id TOSInfo;
@end
