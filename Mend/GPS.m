//
//  GPS.m
//  Soteria
//
//  Created by Andrew Goodwin on 11/9/14.
//  Copyright (c) 2014 joebell. All rights reserved.
//

#import "GPS.h"

@implementation GPS@synthesize isDriving, delegate, lastLocation, setLocation;

- (id) init {
    self = [super init];
    if (self != nil) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [locationManager requestAlwaysAuthorization];
        [locationManager startUpdatingLocation];
        [locationManager startUpdatingHeading];
        geocoder = [[CLGeocoder alloc] init];
        
        
        
        isDriving = NO;
        
        
        
        hasBeenOffline = NO;
    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *loc = [locations firstObject];
    [delegate newLocation:loc];
    lastLocation = loc;
    isDriving = loc.speed > 0;

    
    [geocoder reverseGeocodeLocation:lastLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            setLocation = placemark;
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
    //[locationManager stopUpdatingLocation];
    
    
}

-(void)increaseAccuracy{
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
}

-(void)decreaseAccuracy{
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
}

+(GPS *)sharedGPS{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[GPS alloc] init];
    });
    return sharedInstance;
}
@end
