//
//  PatientDashboardViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 12/21/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import "PatientDashboardViewController.h"
#import "PatientLocationViewController.h"
#import "VisitConfirmationViewController.h"


@interface PatientDashboardViewController ()
{
    IBOutlet UIView *selectorBG;
    IBOutlet UISegmentedControl *segTime;
    IBOutlet UIView *nextVisit;
    IBOutlet UIView *secondVisit;
    IBOutlet UIButton *btnSchedule;
    IBOutlet NSLayoutConstraint *locationTableHeight;
    IBOutlet NSLayoutConstraint *segTimeUpperPadding;
    NSMutableArray *locations;
    UIImageView *imgArrow;
    NSUInteger selectedLocation;
    
    __block BOOL gettingLocations;
    
    NSMutableArray *appointments;
    NSMutableArray *todaysAppointments;
    NSMutableArray *tomorrowsAppointments;
    
    UIView *appointmentConfirmation;
    
    UIView *blackout;
    UIView *apptView;
    IBOutlet UIScrollView *scroller;
    
    double lastOffset;
    
    double maxWidth;
    double minWidth;
    double offset;
    double halfWidth;
    double ratio;
    double widthDiff;
    
    __block NSInteger selectedAppointment;
    __block NSInteger selectedIndex;
    
    UIView *deleteConfirmation;
    BOOL hasCalculatedTodaysScrollerLength;
    BOOL hasCalculatedTomorrowsScrollerLength;
    
    double todaysScrollerLength;
    double tomorrowsScrollerLength;
    
    id AddressToEdit;
    
    NSDate *tomorrowDate;
    
    id ReturnAppointment;
    
    UIView *socialView;
    
    BOOL adjustLocationTableHeight;
    
    UIView *scrollerOverlay;
    
    UIView *countdownView;
    UILabel *lblCountdown;
    
    UIView *noAppointmentsView;
    
    BOOL willBeCharged;
    
    UIView *chargeForCancelView;
    
    UILabel *lblDate;
    
    NSTimer *checkAppointmentsTimer;
    IBOutlet NSLayoutConstraint *centerYConstraint;
    IBOutlet NSLayoutConstraint *topConstraint;
    
    NSInteger numberOfAppointments;
}
@property (strong, nonatomic) IBOutlet MKMapView *dashboardMap;
@property (strong, nonatomic) IBOutlet UITableView *locationTable;
-(IBAction)changeDate:(id)sender;
@end

@implementation PatientDashboardViewController
@synthesize dashboardMap, locationTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    [Appointment currentAppointment];
    
    adjustLocationTableHeight = NO;
    
    hasCalculatedTodaysScrollerLength = NO;
    hasCalculatedTomorrowsScrollerLength = NO;
    
    selectedLocation = 0;
    selectedIndex = 0;
    selectedAppointment = 0;
    //[self callCustomer:@"(479) 219-3162"];
    
    /*UILocalNotification* localNotification = [[UILocalNotification alloc] init];
     localNotification.fireDate = [[NSDate date] dateByAddingTimeInterval:20];
     localNotification.alertBody = @"Your appointment is in an hour";
     localNotification.alertAction = @"Open MEND";
     localNotification.timeZone = [NSTimeZone defaultTimeZone];
     //localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
     
     [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];*/
    
    dashboardMap.userInteractionEnabled = NO;
    
    todaysAppointments = [[NSMutableArray alloc] init];
    tomorrowsAppointments = [[NSMutableArray alloc] init];
    
    locations = [[NSMutableArray alloc] init];
    
    //locations = [[NSMutableArray alloc] initWithArray:@[@{@"nickname":@"HOME",@"address":@"236 Meadow Rd"},@{@"nickname":@"RENT HOUSE",@"address":@"2005 Weems St"}]];
    
    
    
    
    /*[locQuery getObjectInBackgroundWithId:@"1" block:^(PFObject *object, NSError *error) {
     if (!object) {
     ////NSLog(@"The getFirstObject request failed.");
     } else {
     // The find succeeded.
     ////NSLog(@"object: %@", object);
     
     
     }
     }];*/
    /*[locQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
     if (!object) {
     ////NSLog(@"The getFirstObject request failed.");
     } else {
     // The find succeeded.
     ////NSLog(@"object: %@", object);
     
     
     }
     }];*/
    
    
    //todaysAppointments = [[NSMutableArray alloc] initWithArray:@[@{@"day":@"Today",@"time":@"9:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"10:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"11:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"9:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"10:30",@"ampm":@"AM"},@{@"day":@"Today",@"time":@"11:30",@"ampm":@"AM"}]];
    //[self reloadScrollView];
    
    /*NSDateFormatter *format = [[NSDateFormatter alloc] init];
     [format setDateFormat:@"EEEE, MMM dd"];
     
     NSString *today = [format stringFromDate:[NSDate date]];
     
     // set up date components
     NSDateComponents *components = [[NSDateComponents alloc] init];
     [components setDay:1];
     
     // create a calendar
     NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
     
     tomorrowDate = [gregorian dateByAddingComponents:components toDate:[NSDate date] options:0];
     NSString *tomorrow = [format stringFromDate:tomorrowDate];
     
     [segTime setTitle:[NSString stringWithFormat:@"TODAY\n%@", today] forSegmentAtIndex:0];
     [segTime setTitle:[NSString stringWithFormat:@"TOMORROW\n%@", tomorrow] forSegmentAtIndex:1];*/
    //[segTime setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:                                     [UIFont fontWithName:@"Avenir-Book" size:18.0], NSFontAttributeName,                                     nil] forState:UIControlStateNormal];
    
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.043;
    span.longitudeDelta=0.043;
    //32.782572, -96.802236
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(32.865482, -96.786903);
    
    region.span=span;
    region.center=coordinate;
    
    [dashboardMap setRegion:region animated:TRUE];
    [dashboardMap regionThatFits:region];
    
    locationTable.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    locationTable.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    locationTable.layer.shadowOpacity = 0.7f;
    locationTable.layer.shadowRadius = 0.1f;
    //locationTable.clipsToBounds = NO;
    locationTable.layer.masksToBounds = NO;
    locationTable.layer.zPosition = 10000;
    
    selectorBG.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    selectorBG.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    selectorBG.layer.shadowOpacity = 0.7f;
    selectorBG.layer.shadowRadius = 0.1f;
    //selectorBG.clipsToBounds = NO;
    selectorBG.layer.masksToBounds = NO;
    selectorBG.layer.zPosition = 1000;
    
    segTime.layer.zPosition = 1100;
    segTime.backgroundColor = [UIColor whiteColor];
    
    scroller.layer.zPosition = 1;
    //scroller.userInteractionEnabled = NO;
    
    /*NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
     [UIColor whiteColor], NSForegroundColorAttributeName,
     nil];
     [segTime setTitleTextAttributes:attributes forState:UIControlStateNormal];
     NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor lightGrayColor] forKey:NSForegroundColorAttributeName];
     [segTime setTitleTextAttributes:highlightedAttributes forState:UIControlStateHighlighted];
     
     
     [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
     [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor lightGrayColor]} forState:UIControlStateNormal];
     */
    
    //[self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
    //locationTable.backgroundColor = [UIColor orangeColor];
    // Do any additional setup after loading the view.
    UIButton *btnSettings = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 20.0, 20.0)];
    [btnSettings setImage:[UIImage imageNamed:@"icon_gear"] forState:UIControlStateNormal];
    [btnSettings addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSettings];
    
    UIButton *btnAccessory = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAccessory.frame = CGRectMake(20.0, 12.0, 20.0, 20.0);
    [btnAccessory setImage:[UIImage imageNamed:@"icon_mend"] forState:UIControlStateNormal];
    [btnSchedule addSubview:btnAccessory];
    
    /*nextVisit.layer.cornerRadius = nextVisit.frame.size.width / 2;
     nextVisit.layer.shadowColor = [UIColor darkGrayColor].CGColor;
     nextVisit.layer.shadowOffset = CGSizeMake(0.0, 0.5);
     nextVisit.layer.shadowOpacity = 0.7f;
     nextVisit.layer.shadowRadius = 1.0f;
     nextVisit.layer.borderColor = [UIColor darkGrayColor].CGColor;
     nextVisit.layer.borderWidth = 0.2f;
     //selectorBG.clipsToBounds = NO;
     nextVisit.layer.masksToBounds = NO;
     nextVisit.layer.zPosition = 100;
     
     secondVisit.layer.cornerRadius = secondVisit.frame.size.width / 2;
     secondVisit.layer.shadowColor = [UIColor darkGrayColor].CGColor;
     secondVisit.layer.shadowOffset = CGSizeMake(0.0, 0.5);
     secondVisit.layer.shadowOpacity = 0.7f;
     secondVisit.layer.shadowRadius = 1.0f;
     secondVisit.layer.borderColor = [UIColor darkGrayColor].CGColor;
     secondVisit.layer.borderWidth = 0.2f;
     //selectorBG.clipsToBounds = NO;
     secondVisit.layer.masksToBounds = NO;*/
    
    [locationTable setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AppointmentScheduled" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self showAppointmentView];
        [self showSocialView];
        [self showVisitInfo];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AppointmentCancelled" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        //[self cancelAppointment:self];
        locationTable.hidden = NO;
        nextVisit.hidden = NO;
        //secondVisit.hidden = YES;
        btnSchedule.hidden = NO;
        segTime.hidden = NO;
        selectorBG.hidden = NO;
        scroller.hidden = NO;
        [apptView removeFromSuperview];
        apptView = nil;
        [countdownView removeFromSuperview];
        countdownView = nil;
        [socialView removeFromSuperview];
        socialView = nil;
        willBeCharged = NO;
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CustomerLoggedIn" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        //adjustLocationTableHeight = YES;
        [self getLocations];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UserLoggedOut" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if(checkAppointmentsTimer.isValid){
            [checkAppointmentsTimer invalidate];
            checkAppointmentsTimer = nil;
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateUserLocations" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        adjustLocationTableHeight = YES;
        [self getLocations];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"AdjustCountdownTimer" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        lblCountdown.text = (NSString *)note.object;
        if([lblCountdown.text isEqualToString:@"Less than a minute remaining to cancel for free."])
        {
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Less than a minute remaining to cancel for free."];
            [attrString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Avenir-Book" size:12.0]
                               range:NSMakeRange(0, attrString.length - 1)];
            lblCountdown.attributedText = attrString;
        }
        
        lblDate.text = [Appointment currentAppointment].day;
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CustomerWillBeCharged" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        /*locationTable.hidden = NO;
         nextVisit.hidden = NO;
         //secondVisit.hidden = YES;
         btnSchedule.hidden = NO;
         segTime.hidden = NO;
         selectorBG.hidden = NO;
         scroller.hidden = NO;
         [apptView removeFromSuperview];
         apptView = nil;
         [socialView removeFromSuperview];
         socialView = nil;*/
        [countdownView removeFromSuperview];
        countdownView = nil;
        willBeCharged = YES;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UpdateAppointments" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"EEEE, MMM dd"];
        
        NSString *today = [format stringFromDate:[NSDate date]];
        
        // set up date components
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:1];
        
        // create a calendar
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        tomorrowDate = [gregorian dateByAddingComponents:components toDate:[NSDate date] options:0];
        NSString *tomorrow = [format stringFromDate:tomorrowDate];
        
        [segTime setTitle:[NSString stringWithFormat:@"TODAY\n%@", today] forSegmentAtIndex:0];
        [segTime setTitle:[NSString stringWithFormat:@"TOMORROW\n%@", tomorrow] forSegmentAtIndex:1];
    }];
    
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    if([Appointment currentAppointment].time == nil){
        noAppointmentsView.hidden = NO;
    }
    else{
        locationTable.hidden = YES;
        nextVisit.hidden = YES;
        //secondVisit.hidden = YES;
        btnSchedule.hidden = YES;
        segTime.hidden = YES;
        selectorBG.hidden = YES;
        scroller.hidden = YES;
        noAppointmentsView.hidden = YES;
    }
    
    
    checkAppointmentsTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkForNewAppointments) userInfo:nil repeats:YES];
    
    if (self.view.frame.size.height < 500 || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        centerYConstraint.priority = UILayoutPriorityDefaultLow;
        topConstraint.priority = UILayoutPriorityDefaultHigh;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAppointments) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)showAppointmentView{
    apptView = [[UIView alloc] initWithFrame:CGRectMake(locationTable.frame.origin.x, locationTable.frame.origin.y + 0.5, locationTable.frame.size.width, locationTable.frame.size.height)];
    apptView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0];
    apptView.layer.shadowOpacity = 1.0;
    apptView.layer.shadowColor = [UIColor whiteColor].CGColor;
    apptView.layer.shadowOffset = CGSizeMake(0, -1);
    apptView.layer.shadowRadius = 1.0;
    apptView.clipsToBounds = NO;
    /*self.navigationController.navigationBar.layer.shadowColor = [UIColor whiteColor].CGColor;
     self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0, 0.5);
     self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
     self.navigationController.navigationBar.layer.shadowRadius = 0.5f;
     self.navigationController.navigationBar.layer.masksToBounds = NO;*/
    apptView.layer.zPosition = 2000;
    [self.view addSubview:apptView];
    
    countdownView = [[UIView alloc] initWithFrame:CGRectMake(0.0, apptView.frame.origin.y + apptView.frame.size.height, apptView.frame.size.width, 30.0)];
    countdownView.backgroundColor = [UIColor blackColor];
    countdownView.layer.zPosition = 2000;
    [self.view addSubview:countdownView];
    
    lblCountdown = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 5.0, countdownView.frame.size.width, 20.0)];
    lblCountdown.textColor = [UIColor whiteColor];
    lblCountdown.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
    lblCountdown.textAlignment = NSTextAlignmentCenter;
    //lblCountdown.text = @"30 minutes remaining to cancel for free.";
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"30 minutes remaining to cancel for free."];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"Avenir-Heavy" size:14.0]
                       range:NSMakeRange(0, 2)];
    lblCountdown.attributedText = attrString;
    [countdownView addSubview:lblCountdown];
    
    [[Appointment currentAppointment] startTimer];
    
    willBeCharged = NO;
    
    locationTable.hidden = YES;
    nextVisit.hidden = YES;
    secondVisit.hidden = YES;
    btnSchedule.hidden = YES;
    segTime.hidden = YES;
    selectorBG.hidden = YES;
    scroller.hidden = YES;
    noAppointmentsView.hidden = YES;
    
    UIImageView *mender = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 5.0, 40.0, 40.0)];
    mender.image = [UIImage imageWithData:[Appointment currentAppointment].physicianImage];
    mender.layer.cornerRadius = mender.frame.size.width / 2;
    mender.clipsToBounds = YES;
    [apptView addSubview:mender];
    
    UITapGestureRecognizer *menderTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showVisitInfo)];
    menderTap.numberOfTapsRequired = 1;
    [apptView addGestureRecognizer:menderTap];
    
    UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 10.0, 200.0, 20.0)];
    lblTime.textColor = [UIColor whiteColor];
    lblTime.text = [Appointment currentAppointment].time;
    lblTime.font = [UIFont boldSystemFontOfSize:14.0];
    [apptView addSubview:lblTime];
    
    lblDate = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 25.0, 200.0, 20.0)];
    lblDate.textColor = [UIColor whiteColor];
    lblDate.text = [Appointment currentAppointment].day;
    lblDate.font = [UIFont systemFontOfSize:14.0];
    [apptView addSubview:lblDate];
    
    UIButton *btnCancelAppt = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancelAppt.frame = CGRectMake(apptView.frame.size.width - 80.0, 10.0, 70.0, 30.0);
    [btnCancelAppt setTitle:@"CANCEL" forState:UIControlStateNormal];
    [btnCancelAppt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCancelAppt setBackgroundColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    btnCancelAppt.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    btnCancelAppt.layer.borderColor = [UIColor whiteColor].CGColor;
    btnCancelAppt.layer.borderWidth = 0.5f;
    btnCancelAppt.layer.cornerRadius = 5.0f;
    [btnCancelAppt addTarget:self action:@selector(cancelAppointment:) forControlEvents:UIControlEventTouchUpInside];
    [apptView addSubview:btnCancelAppt];
}

-(void)showSocialView{
    /*blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
     blackout.backgroundColor = [UIColor blackColor];
     blackout.layer.opacity = 0.7;
     blackout.layer.zPosition = 2000;
     [self.view addSubview:blackout];*/
    if(self.view.frame.size.height < 500 || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
        socialView = [[UIView alloc] initWithFrame:CGRectMake(20.0, self.view.frame.size.height/2 - 105.0, self.view.frame.size.width - 40.0, 270.0)];
    else
        socialView = [[UIView alloc] initWithFrame:CGRectMake(20.0, self.view.frame.size.height/2 - 135.0, self.view.frame.size.width - 40.0, 270.0)];
    socialView.backgroundColor = [UIColor whiteColor];
    socialView.layer.zPosition = 2010;
    socialView.layer.shadowColor = [UIColor blackColor].CGColor;
    socialView.layer.shadowOffset = CGSizeMake(0.0, -1.0);
    socialView.layer.shadowOpacity = 0.7;
    socialView.layer.shadowRadius = 3;
    socialView.clipsToBounds = NO;
    [self.view addSubview:socialView];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_mend"]];
    logo.frame = CGRectMake(socialView.frame.size.width/2 - 30.0, 15.0, 60.0, 60.0);
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [socialView addSubview:logo];
    
    UILabel *scheduled = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 90.0, socialView.frame.size.width, 30.0)];
    scheduled.font = [UIFont fontWithName:@"Avenir-Heavy" size:16.0];
    scheduled.text = @"YOUR VISIT IS SCHEDULED";
    scheduled.textAlignment = NSTextAlignmentCenter;
    [socialView addSubview:scheduled];
    
    UILabel *mend = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 115.0, socialView.frame.size.width, 30.0)];
    mend.font = [UIFont fontWithName:@"Avenir-Book" size:12.0];
    mend.text = @"WE'LL HAVE YOU ON THE MEND SOON!";
    mend.textAlignment = NSTextAlignmentCenter;
    [socialView addSubview:mend];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0.0, socialView.frame.size.height/2 + 15.0, socialView.frame.size.width, 1.0)];
    line.backgroundColor = [UIColor colorWithWhite:0.929 alpha:1.000];
    [socialView addSubview:line];
    
    UILabel *friends = [[UILabel alloc] initWithFrame:CGRectMake(0.0, line.frame.origin.y + 15.0, socialView.frame.size.width, 30.0)];
    friends.font = [UIFont fontWithName:@"Avenir-Heavy" size:12.0];
    friends.text = @"SHARE MEND WITH FRIENDS";
    friends.textAlignment = NSTextAlignmentCenter;
    [socialView addSubview:friends];
    
    UIButton *facebook = [UIButton buttonWithType:UIButtonTypeCustom];
    facebook.frame = CGRectMake(55.0, friends.frame.origin.y + 35.0, 60.0, 60.0);
    [facebook setImage:[UIImage imageNamed:@"icon_share_facebook"] forState:UIControlStateNormal];
    [facebook addTarget:self action:@selector(shareWithFacebook:) forControlEvents:UIControlEventTouchUpInside];
    //[facebook setContentMode:UIViewContentModeScaleAspectFit];
    [socialView addSubview:facebook];
    
    UIButton *general = [UIButton buttonWithType:UIButtonTypeCustom];
    general.frame = CGRectMake(socialView.frame.size.width - 115.0, friends.frame.origin.y + 35.0, 60.0, 60.0);
    [general setImage:[UIImage imageNamed:@"icon_share_general"] forState:UIControlStateNormal];
    [general addTarget:self action:@selector(openShareDialog:) forControlEvents:UIControlEventTouchUpInside];
    [socialView addSubview:general];
    
    //btnSchedule.hidden = YES;
}

-(void)loadNoAppointmentsView{
    if(self.view.frame.size.height < 500 || (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
        noAppointmentsView = [[UIView alloc] initWithFrame:CGRectMake(30.0, self.view.frame.size.height/2 - 80.0, self.view.frame.size.width - 60.0, 270.0)];
    else
        noAppointmentsView = [[UIView alloc] initWithFrame:CGRectMake(30.0, self.view.frame.size.height/2 - 100.0, self.view.frame.size.width - 60.0, 270.0)];
    noAppointmentsView.backgroundColor = [UIColor whiteColor];
    noAppointmentsView.layer.zPosition = 10;
    noAppointmentsView.layer.shadowColor = [UIColor blackColor].CGColor;
    noAppointmentsView.layer.shadowOffset = CGSizeMake(0.0, -1.0);
    noAppointmentsView.layer.shadowOpacity = 0.7;
    noAppointmentsView.layer.shadowRadius = 3;
    noAppointmentsView.clipsToBounds = NO;
    noAppointmentsView.hidden = YES;
    noAppointmentsView.userInteractionEnabled = NO;
    [self.view addSubview:noAppointmentsView];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_mend"]];
    logo.frame = CGRectMake(noAppointmentsView.frame.size.width/2 - 30.0, 40.0, 60.0, 60.0);
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [noAppointmentsView addSubview:logo];
    
    UILabel *scheduled = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 120.0, noAppointmentsView.frame.size.width - 100.0, 60.0)];
    scheduled.font = [UIFont fontWithName:@"Avenir-Heavy" size:15.0];
    scheduled.text = @"NO APPOINTMENTS AVAILABLE";
    scheduled.textAlignment = NSTextAlignmentCenter;
    scheduled.numberOfLines = 0;
    [noAppointmentsView addSubview:scheduled];
    
    UILabel *mend = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 180.0, noAppointmentsView.frame.size.width - 60.0, 60.0)];
    mend.font = [UIFont fontWithName:@"Avenir-Book" size:13.0];
    mend.text = [NSString stringWithFormat:@"We're sorry, but there are no appointments available for %@", segTime.selectedSegmentIndex == 0 ? @"today" : @"tomorrow"];
    mend.textAlignment = NSTextAlignmentCenter;
    mend.numberOfLines = 0;
    [noAppointmentsView addSubview:mend];
    
    
}

-(IBAction)shareWithFacebook:(id)sender{
    
    //DEC 1 TODO
    //NSLog(@"sharing with facebook");
    /*FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"http://www.mendathome.com"];
    params.caption = @"Check out @mendathome. It's a #modernhousecall";
    //params.description = @"We'll have you on the MEND soon!";
    params.picture = [NSURL URLWithString:@"http://www.mendathome.com/wp-content/themes/bones/library/images/social_share.png"];
    params.linkDescription = @"We bring exceptional bedside manner to your bedside. It’s professional healthcare in the comfort and convenience of your home, office or hotel. No waiting rooms, no germ-filled offices, no outrageous bills or hidden fees.";
    BOOL canShare = [FBDialogs canPresentShareDialogWithParams:params];
    if (canShare) {
        // FBDialogs call to open Share dialog
        //NSURL* url = [NSURL URLWithString:@"http://stage.mendathome.com"];
        [FBDialogs presentShareDialogWithParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            if(!error){
                ////NSLog(@"Shared with facebook");
            }
        }];
    }
    else{
        // Show the feed dialog
        // Put together the dialog parameters
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          @"MEND", @"name",
                                          @"Check out @mendathome. It's a #modernhousecall", @"caption",
                                          @"We bring exceptional bedside manner to your bedside. It’s professional healthcare in the comfort and convenience of your home, office or hotel. No waiting rooms, no germ-filled offices, no outrageous bills or hidden fees.", @"description",
                                          @"http://www.mendathome.com", @"link",
                                          @"http://www.mendathome.com/wp-content/themes/bones/library/images/social_share.png", @"picture",
                                          nil];
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:paramDict
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          //NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              //NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  //NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  //NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  //NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }*/
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (void)callCustomer:(NSString *)phone {
    NSMutableString *phNo = [[NSMutableString alloc] initWithString:phone];
    [phNo replaceOccurrencesOfString:@"(" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@")" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@"-" withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    [phNo replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, phNo.length)];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"This device does not support making phone calls." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(IBAction)openShareDialog:(id)sender{
    ////NSLog(@"opening share dialog");
    
    NSMutableArray *sharingItems = [[NSMutableArray alloc] init];
    
    NSString *text = @"Check out @mendathome. It's a #modernhousecall";
    UIImage *image = [UIImage imageNamed:@"social_share"];
    NSURL *url = [NSURL URLWithString:@"http://www.mendathome.com"];
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

-(void)getLocations{
    
    if (gettingLocations) {
        return;
    }
    gettingLocations = true;
    if([User sharedUser].customerID){
        PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
        [custQuery whereKey:@"objectId" equalTo:[User sharedUser].customerID];
        
        PFQuery *locQuery = [PFQuery queryWithClassName:@"Location"];
        [locQuery whereKey:@"customer" matchesQuery:custQuery];
        [locQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                ////NSLog(@"The getFirstObject request failed.");
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to get user locations at this time."];
            } else {
                // The find succeeded.
                ////NSLog(@"objects: %@", objects);
                locations = [[NSMutableArray alloc] initWithArray:objects];
                selectedLocation = 0;
                
                /*** Issue 2218 - Duplicate locations
                 I was unable to find the root of the problem, and I was unable to reproduce
                 the bug, so I added this code to prevent any duplicate addresses from showing up.
                 
                 I have a feeling this is a server side problem.
                 */
                NSMutableIndexSet *remove = [NSMutableIndexSet indexSet];
                // remove duplicate locations
                [locations enumerateObjectsUsingBlock:^(PFObject *loc1, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([remove containsIndex:idx]) {
                        return;
                    }
                    NSInteger duplicateLocation = [locations indexOfObjectPassingTest:^BOOL(PFObject *loc2, NSUInteger idx2, BOOL * _Nonnull stop) {
                        if (idx != idx2
                            && [loc1[@"address1"] isEqualToString:loc2[@"address1"]]
                            && [loc1[@"address2"] isEqualToString:loc2[@"address2"]]
                            && [loc1[@"city"] isEqualToString:loc2[@"city"]]
                            && [loc1[@"nickname"] isEqualToString:loc2[@"nickname"]]
                            && [loc1[@"state"] isEqualToString:loc2[@"state"]]
                            && [[loc1 valueForKey:@"zip"] isEqualToString:[loc2 valueForKey:@"zip"]]) {
                            return true;
                            *stop = true;
                        }
                        
                        return false;
                    }];
                    if (duplicateLocation != NSNotFound) {
                        [remove addIndex:duplicateLocation];
                        NSLog(@"Duplicate locations found (%@).  Ignoring one.", loc1[@"address1"]);
                    }
                }];
                
                [locations removeObjectsAtIndexes:remove];
                
                
                if([User sharedUser].selectedLocationID != nil)
                {
                    [locations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        if([[obj valueForKey:@"objectId"] isEqualToString:[User sharedUser].selectedLocationID])
                        {
                            selectedLocation = idx;
                            *stop = YES;
                        }
                    }];
                }
                //dispatch_async(dispatch_get_main_queue(), ^{
                [locationTable reloadData];
                if(adjustLocationTableHeight)
                {
                    [self adjustLocationTableHeight];
                    adjustLocationTableHeight = NO;
                }
                
                id coordinates = [[locations objectAtIndex:selectedLocation] objectForKey:@"coordinates"];
                NSNumber *latitude = [coordinates valueForKey:@"latitude"];
                NSNumber *longitude = [coordinates valueForKey:@"longitude"];
                
                /*Region and Zoom*/
                MKCoordinateRegion region;
                MKCoordinateSpan span;
                span.latitudeDelta=0.043;
                span.longitudeDelta=0.043;
                //32.782572, -96.802236
                
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
                
                region.span=span;
                region.center=coordinate;
                
                [dashboardMap setRegion:region animated:TRUE];
                [dashboardMap regionThatFits:region];
                
                //});
                if([Appointment currentAppointment].appointmentID == nil)
                    [self getAppointments];
                else
                    [self checkAppointmentStatus];
            }
            gettingLocations = false;
        }];
    }
    else{
        locations = [[NSMutableArray alloc] init];
        //[locationTable reloadData];
        
        selectedLocation = 0;
        //[self getAppointments];
        gettingLocations = false;
    }
    //[self adjustLocationTableHeight];
}

-(void)getAppointments{
    if([User sharedUser].customerID)
    {
        todaysAppointments = [[NSMutableArray alloc] init];
        tomorrowsAppointments = [[NSMutableArray alloc] init];
        NSDictionary *address;
        if ([locations count] > selectedLocation) {
            id theLocation = locations[selectedLocation];
            address = @{@"address1":theLocation[@"address1"],
                        @"address2":theLocation[@"address2"],
                        @"city":theLocation[@"city"],
                        @"state":theLocation[@"state"],
                        @"zip":theLocation[@"zip"]};
        } else {
            address = @{};
        }
        NSDictionary *params = @{@"startdate" : [NSDate date],
                                 @"enddate" : tomorrowDate,
                                 @"address" : address
                                 };
        //NSDictionary *location = @{@"location":@{@"latitude":@32.840282, @"longitude":@-96.800691}};
        //[params setValue:location forKey:@"location"];
        
        //////NSLog(@"Dict: %@", params);
        
        //if([User sharedUser].objectID && locations != nil && locations.count > 0)
        //{
        [PFCloud callFunctionInBackground:@"getAppointments" withParameters:params block:^(id object, NSError *error) {
            if(error == nil){
                //good status code???
                //upload image now (BASE64 ENCODED!)
                //NSLog(@"response: %@", object);
                appointments = [[NSMutableArray alloc] initWithArray:object];
                NSDateComponents *todayComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                
                NSDateComponents *tomorrowComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:tomorrowDate];
                
                NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
                //[df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                [df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
                
                NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
                [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
                [df_local setDateFormat:@"yyyy.MM.dd HH:mm:ss zzz"];
                
                NSDateFormatter *time = [[NSDateFormatter alloc] init];
                [time setDateFormat:@"h:mm"];
                
                NSDateFormatter *ampm = [[NSDateFormatter alloc] init];
                [ampm setDateFormat:@"a"];
                
                
                
                [appointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    //convert all dates to local time from UTC
                    NSString *s = [(NSString *)obj stringByReplacingOccurrencesOfString:@":" withString:@""];
                    NSDate *date = [df_utc dateFromString:s];
                    if(date){
                        NSString *localDateTime = [df_local stringFromDate:date];
                        NSDate *localDate = [df_local dateFromString:localDateTime];
                        
                        if(localDate){
                            NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:localDate];
                            
                            NSDateFormatter *appointmentDate = [[NSDateFormatter alloc] init];
                            [appointmentDate setDateFormat:@"yyyy-MM-dd HH:mm:sszzz"]; //2014-12-12 00:00:00CST
                            //NSString *apptDateString = [appointmentDate stringFromDate:date];
                            //////NSLog(@"Date: %@", date.description);
                            //////NSLog(@"Local Date: %@", localDate.description);
                            //////NSLog(@"Local Date String: %@", localDateTime);
                            //dateComps.timeZone = [NSTimeZone timeZoneWithName:@"CST"];
                            
                            NSDateFormatter* scheduleFormat = [[NSDateFormatter alloc] init];
                            [scheduleFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                            [scheduleFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.'000Z'"];
                            
                            /*** Bug fix by James Perlman on 9/29/2015 ***
                             ---------------------------------------------
                             
                             The reported bug is causing appointments in the past to show up.
                             Another problem is that appointments might show up too close to
                             the present, and it would be unfeasable for a doctor to make it
                             out that soon.  This should really be dealt with on the server
                             side, but this client side code will at least prevent the
                             appointments from showing up to honest customers using the app.
                             
                             ***/
                            
                            int reqMinsUntilAppt = 30; // number of minutes required prior to a visible appointment
                            
                            NSTimeInterval reqSecondsUntilAppt = (NSTimeInterval)(reqMinsUntilAppt * 60);
                            
                            NSTimeInterval secondsUntilAppt = [localDate timeIntervalSinceDate:[NSDate date]];
                            
                            if (secondsUntilAppt < reqSecondsUntilAppt) {
                                
                                NSLog(@"\nIgnoring appointment found less than %d minutes from now (Appointment is %lu minutes from now, timestamp %@)", reqMinsUntilAppt, (long)(secondsUntilAppt / 60.0), localDateTime);
                                return;
                            }
                            
                            //seperate by day
                            if(todayComps.day == dateComps.day)
                            {
                                
                                NSDictionary *appt = @{@"date":[scheduleFormat stringFromDate:localDate], @"time":[time stringFromDate:localDate], @"ampm":[ampm stringFromDate:localDate], @"day":@"TODAY"};
                                
                                [todaysAppointments addObject:appt];
                            }
                            else if (tomorrowComps.day == dateComps.day){
                                NSDictionary *appt = @{@"date":[scheduleFormat stringFromDate:date], @"time":[time stringFromDate:localDate], @"ampm":[ampm stringFromDate:localDate], @"day":@"TOMORROW"};
                                
                                [tomorrowsAppointments addObject:appt];
                            }
                        }
                        
                    }
                    
                    //convert from utc to cst
                    //store time and ampm in dictionary
                }];
                
                //////NSLog(@"todays appts: %@", todaysAppointments);
                //////NSLog(@"tomorrows appts: %@", tomorrowsAppointments);
                
                //sort the arrays
                NSMutableArray *bufferArray = [[NSMutableArray alloc] initWithArray:todaysAppointments];
                NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
                [bufferArray sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
                todaysAppointments = bufferArray;
                
                bufferArray = [[NSMutableArray alloc] initWithArray:tomorrowsAppointments];
                [bufferArray sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
                tomorrowsAppointments = bufferArray;
                
                hasCalculatedTomorrowsScrollerLength = NO;
                hasCalculatedTodaysScrollerLength = NO;
                tomorrowsScrollerLength = 0;
                todaysScrollerLength = 0;
                
                [self reloadScrollView];
            }
            else{
                //nserror should have content
                ////NSLog(@"error: %@", error.description);
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to get appointments at this time."];
            }
        }];
    }
    
    //}
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName = @"Patient Dashboard";
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    //self.title = @"MEND";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //[defaults setValue:nil forKey:@"userid"];
    //[defaults setValue:nil forKey:@"stayloggedin"];
    //[defaults synchronize];
    if([[defaults objectForKey:@"hasSeenTour"] isEqual: @1] )
    {
        if([defaults objectForKey:@"stayloggedin"] == nil)
        {
            [self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
        }
        else{
            if([[defaults objectForKey:@"stayloggedin"] isEqual:@1] || [[defaults objectForKey:@"temporarylogin"] isEqual:@1]){
                //good to go
            }
            else{
                [[User sharedUser] logout];
                [self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
            }
        }
    }
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"EEEE, MMM dd"];
    
    NSString *today = [format stringFromDate:[NSDate date]];
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    tomorrowDate = [gregorian dateByAddingComponents:components toDate:[NSDate date] options:0];
    NSString *tomorrow = [format stringFromDate:tomorrowDate];
    
    [segTime setTitle:[NSString stringWithFormat:@"TODAY\n%@", today] forSegmentAtIndex:0];
    [segTime setTitle:[NSString stringWithFormat:@"TOMORROW\n%@", tomorrow] forSegmentAtIndex:1];
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //self.title = @"BACK";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getLocations];
    /*int i = 0;
     for (id segment in segTime.subviews){
     for (id label in [segment subviews])
     {
     if ([label isKindOfClass:[UILabel class]])
     {
     UILabel *titleLabel = (UILabel *) label;
     
     titleLabel.numberOfLines = 0;
     NSString *segText = titleLabel.text;
     NSMutableAttributedString *text =
     [[NSMutableAttributedString alloc]
     initWithString:segText];
     
     //titleLabel.text = @"";
     titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:10.0];
     
     if(i == 0){
     titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, 100.0, 45.0);
     [text addAttribute:NSFontAttributeName
     value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
     range:NSMakeRange(0, 8)];
     }
     else{
     titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, 60.0, 45.0);
     [text addAttribute:NSFontAttributeName
     value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
     range:NSMakeRange(0, 5)];
     }
     i++;
     //(the width us typically too low)
     
     
     
     titleLabel.attributedText = text;
     titleLabel.textColor = [UIColor lightGrayColor];
     }
     }
     }*/
    
    int i = 0;
    for (id segment in segTime.subviews){
        for (id label in [segment subviews])
        {
            if ([label isKindOfClass:[UILabel class]])
            {
                UILabel *titleLabel = (UILabel *) label;
                
                titleLabel.numberOfLines = 0;
                NSString *segText = titleLabel.text;
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithString:segText];
                
                //titleLabel.text = @"";
                titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:10.0];
                
                if(i == 0){
                    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x-60.0, titleLabel.frame.origin.y, 130.0, 45.0);
                    [text addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
                                 range:NSMakeRange(0, 8)];
                }
                else{
                    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x - 60.0, titleLabel.frame.origin.y, 130.0, 45.0);
                    [text addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
                                 range:NSMakeRange(0, 5)];
                }
                i++;
                //(the width us typically too low)
                
                
                
                titleLabel.attributedText = text;
                titleLabel.textColor = [UIColor lightGrayColor];
            }
        }
    }
    
    maxWidth = self.view.frame.size.width * 0.6;
    minWidth = self.view.frame.size.width * 0.25;
    //maxWidth = 200.0;
    //minWidth = 150.0;
    offset = self.view.frame.size.width / 3;
    halfWidth = scroller.frame.size.width / 2;
    ratio = minWidth/maxWidth;
    widthDiff = maxWidth - minWidth;
    //[self reloadScrollView];
    //[self showSocialView];
    
    //[Appointment currentAppointment];
    if ([User sharedUser].loggedIn) {
        [self checkAppointmentStatus];
    }
    //show tour
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"hasSeenTour"] isEqual: @0])
    {
        [self performSegueWithIdentifier:@"segueShowTour" sender:self];
        [defaults setValue:@1 forKey:@"hasSeenTour"];
        [defaults synchronize];
    }
    
    if(!noAppointmentsView)
        [self loadNoAppointmentsView];
    
}

- (void)checkAppointmentStatus {
    
    if([Appointment currentAppointment].time != nil)
    {
        //there is a current appointment
        if(apptView == nil)
        {
            apptView = [[UIView alloc] initWithFrame:CGRectMake(locationTable.frame.origin.x, locationTable.frame.origin.y + 0.5, locationTable.frame.size.width, locationTable.frame.size.height)];
            apptView.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0];
            /*self.navigationController.navigationBar.layer.shadowColor = [UIColor whiteColor].CGColor;
             self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0, 0.5);
             self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
             self.navigationController.navigationBar.layer.shadowRadius = 0.5f;
             self.navigationController.navigationBar.layer.masksToBounds = NO;*/
            apptView.layer.shadowOpacity = 1.0;
            apptView.layer.shadowColor = [UIColor whiteColor].CGColor;
            apptView.layer.shadowOffset = CGSizeMake(0, -1);
            apptView.layer.shadowRadius = 1.0;
            apptView.clipsToBounds = NO;
            apptView.layer.zPosition = 2000;
            [self.view addSubview:apptView];
            
            countdownView = [[UIView alloc] initWithFrame:CGRectMake(0.0, apptView.frame.origin.y + apptView.frame.size.height, apptView.frame.size.width, 30.0)];
            countdownView.backgroundColor = [UIColor blackColor];
            countdownView.layer.zPosition = 20001;
            [self.view addSubview:countdownView];
            
            lblCountdown = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 5.0, countdownView.frame.size.width, 20.0)];
            lblCountdown.textColor = [UIColor whiteColor];
            lblCountdown.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
            lblCountdown.textAlignment = NSTextAlignmentCenter;
            //lblCountdown.text = @"30 minutes remaining to cancel for free.";
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"30 minutes remaining to cancel for free."];
            [attrString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:@"Avenir-Heavy" size:14.0]
                               range:NSMakeRange(0, 2)];
            lblCountdown.attributedText = attrString;
            [countdownView addSubview:lblCountdown];
            
            [[Appointment currentAppointment] startTimer];
            
            locationTable.hidden = YES;
            nextVisit.hidden = YES;
            secondVisit.hidden = YES;
            btnSchedule.hidden = YES;
            segTime.hidden = YES;
            selectorBG.hidden = YES;
            scroller.hidden = YES;
            noAppointmentsView.hidden = YES;
            
            UIImageView *mender = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 5.0, 40.0, 40.0)];
            mender.image = [UIImage imageWithData:[Appointment currentAppointment].physicianImage];
            mender.layer.cornerRadius = mender.frame.size.width / 2;
            mender.clipsToBounds = YES;
            [apptView addSubview:mender];
            
            UITapGestureRecognizer *menderTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showVisitInfo)];
            menderTap.numberOfTapsRequired = 1;
            [apptView addGestureRecognizer:menderTap];
            
            UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 10.0, 200.0, 20.0)];
            lblTime.textColor = [UIColor whiteColor];
            lblTime.text = [Appointment currentAppointment].time;
            lblTime.font = [UIFont boldSystemFontOfSize:14.0];
            [apptView addSubview:lblTime];
            
            lblDate = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 25.0, 200.0, 20.0)];
            lblDate.textColor = [UIColor whiteColor];
            lblDate.text = [Appointment currentAppointment].day;
            lblDate.font = [UIFont systemFontOfSize:14.0];
            [apptView addSubview:lblDate];
            
            UIButton *btnCancelAppt = [UIButton buttonWithType:UIButtonTypeCustom];
            btnCancelAppt.frame = CGRectMake(apptView.frame.size.width - 80.0, 10.0, 70.0, 30.0);
            [btnCancelAppt setTitle:@"CANCEL" forState:UIControlStateNormal];
            [btnCancelAppt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCancelAppt setBackgroundColor:[UIColor colorWithRed:219.0/255.0 green:38.0/255.0 blue:34.0/255.0 alpha:1.0]];
            btnCancelAppt.titleLabel.font = [UIFont systemFontOfSize:12.0f];
            btnCancelAppt.layer.borderColor = [UIColor whiteColor].CGColor;
            btnCancelAppt.layer.borderWidth = 0.5f;
            btnCancelAppt.layer.cornerRadius = 5.0f;
            [btnCancelAppt addTarget:self action:@selector(cancelAppointment:) forControlEvents:UIControlEventTouchUpInside];
            [apptView addSubview:btnCancelAppt];
            [self showSocialView];
        }
        
        //check if appointment is good
        PFQuery *apptQuery = [PFQuery queryWithClassName:@"Appointment"];
        [apptQuery whereKey:@"objectId" equalTo:[Appointment currentAppointment].appointmentID];
        [apptQuery whereKeyDoesNotExist:@"softDelete"];
        [apptQuery whereKeyDoesNotExist:@"stripeCharge"];
        [apptQuery orderByDescending:@"createdAt"];
        [apptQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (error) {
                //NSLog(@"The getFirstObject request failed.");
                [[Appointment currentAppointment] cancel];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancelled" object:nil];
                [self getAppointments];
                //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                //[appDelegate showModal:@"Unable to get charge history at this time."];
            } else {
                
            }
        }];
    }
    else{
        //TODO:call DB to see if there really is an appointment, if so, set it locally
        if([User sharedUser].customerID != nil)
        {
            PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
            [custQuery whereKey:@"objectId" equalTo:[User sharedUser].customerID];
            
            PFQuery *apptQuery = [PFQuery queryWithClassName:@"Appointment"];
            [apptQuery whereKey:@"customer" matchesQuery:custQuery];
            [apptQuery includeKey:@"customer"];
            [apptQuery includeKey:@"provider"];
            [apptQuery whereKeyDoesNotExist:@"softDelete"];
            [apptQuery whereKeyDoesNotExist:@"stripeCharge"];
            [apptQuery orderByDescending:@"createdAt"];
            [apptQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (error) {
                    //NSLog(@"The getFirstObject request failed.");
                    [[Appointment currentAppointment] cancel];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancelled" object:nil];
                    [self getAppointments];
                    //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    //[appDelegate showModal:@"Unable to get charge history at this time."];
                } else {
                    ReturnAppointment = object;
                    [self performSelectorOnMainThread:@selector(saveAppointment:) withObject:@NO waitUntilDone:NO];
                }
            }];
        }
        
    }
    
}

-(void)checkForNewAppointments{
    if([User sharedUser].customerID != nil)
    {
        PFQuery *custQuery = [PFQuery queryWithClassName:@"Customer"];
        [custQuery whereKey:@"objectId" equalTo:[User sharedUser].customerID];
        
        PFQuery *apptQuery = [PFQuery queryWithClassName:@"Appointment"];
        [apptQuery whereKey:@"customer" matchesQuery:custQuery];
        [apptQuery includeKey:@"customer"];
        [apptQuery includeKey:@"provider"];
        [apptQuery whereKeyDoesNotExist:@"softDelete"];
        [apptQuery whereKeyDoesNotExist:@"stripeCharge"];
        
        [apptQuery orderByDescending:@"createdAt"];
        [apptQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (error) {
                //NSLog(@"The getFirstObject request failed.");
                if([Appointment currentAppointment].time != nil)
                {
                    [[Appointment currentAppointment] cancel];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancelled" object:nil];
                    [self getAppointments];
                }
                //AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                //[appDelegate showModal:@"Unable to get charge history at this time."];
            } else {
                if([Appointment currentAppointment].time == nil)
                {
                    /*NSDate *appointmentDate = [object objectForKey:@"time"];
                     NSDate *now = [NSDate date];
                     NSDate *hourAfterAppointment = [appointmentDate dateByAddingTimeInterval:60*60];*/
                    
                    ReturnAppointment = object;
                    [self performSelectorOnMainThread:@selector(saveAppointment:) withObject:@NO waitUntilDone:NO];
                }
            }
        }];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if([scrollView isEqual:scroller]){
        BOOL scrollingLeft = YES;
        if (scroller.contentOffset.x > lastOffset) {
            scrollingLeft = YES;
        }
        else{
            scrollingLeft = NO;
        }
        
        lastOffset = scroller.contentOffset.x;
        
        //////NSLog(@"Scroller offset: %@", NSStringFromCGPoint(scroller.contentOffset));
        
        __block int highestZIndex = 0;
        
        [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIImageView *circle = (UIImageView *)obj;
            
            double distanceCtoC = ((circle.frame.origin.x + circle.frame.size.width / 2.0) - scroller.contentOffset.x) - scroller.frame.size.width / 2.0;
            
            if(fabs(distanceCtoC) < halfWidth + 60)
            {
                CGPoint centerPoint = circle.center;
                
                double diameter = minWidth +  (halfWidth - fabs(distanceCtoC)) * (widthDiff / halfWidth);
                
                circle.frame = CGRectMake(circle.frame.origin.x, circle.frame.origin.y, diameter, diameter);
                circle.center = centerPoint;
                
                circle.layer.cornerRadius = circle.frame.size.width / 2;
                circle.layer.zPosition = 1000.0 - fabs(distanceCtoC);
                
                if (circle.layer.zPosition > (CGFloat)highestZIndex) {
                    highestZIndex = circle.layer.zPosition;
                    selectedAppointment = circle.tag;
                    selectedIndex = idx;
                }
                circle.hidden = false;
            }
            else{
                //off screen
                circle.hidden = true;
            }
            
            /*if (segTime.selectedSegmentIndex == 0 && idx == scroller.subviews.count - 1 && fabs(distanceCtoC) <= 5.0 && !hasCalculatedTodaysScrollerLength)
             {
             //last appointment
             scroller.contentSize = CGSizeMake(scroller.contentOffset.x + scroller.frame.size.width, scroller.frame.size.height);
             hasCalculatedTodaysScrollerLength = YES;
             todaysScrollerLength = scroller.contentSize.width;
             
             }
             else if(segTime.selectedSegmentIndex == 1 && idx == scroller.subviews.count - 1 && fabs(distanceCtoC) <= 5.0 && !hasCalculatedTomorrowsScrollerLength){
             //last appointment
             scroller.contentSize = CGSizeMake(scroller.contentOffset.x + scroller.frame.size.width, scroller.frame.size.height);
             hasCalculatedTomorrowsScrollerLength = YES;
             tomorrowsScrollerLength = scroller.contentSize.width;
             }
             */
            
            
        }];
        
        /*if(!hasCalculatedScrollerLength){
         [self performSelector:@selector(adjustScrollerWidth) withObject:nil afterDelay:0.3];
         hasCalculatedScrollerLength = YES;
         }*/
        
        ////NSLog(@"scroller content size: %@", NSStringFromCGSize(scroller.contentSize));
        ////NSLog(@"selected appointment index: %li", (long)selectedAppointment);
    }
}

-(void)adjustScrollerWidth{
    __block double lengthOfScroller = 0;
    __block UIView *lastCircle;
    [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *circle = (UIView *)obj;
        if(lengthOfScroller == 0){
            lengthOfScroller += circle.frame.origin.x + circle.frame.size.width / 2; //center of first circle
            
        }
        else{
            lengthOfScroller += (circle.frame.origin.x + circle.frame.size.width/2) - (lastCircle.frame.origin.x + lastCircle.frame.size.width/2); //distance between two circle centers
        }
        lastCircle = circle;
    }];
    lengthOfScroller += halfWidth;
    scroller.contentSize = CGSizeMake(lengthOfScroller, scroller.frame.size.height);
}
/*
 -(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
 [self performSelector:@selector(adjustScrollerOffset) withObject:nil afterDelay:0.1];
 //[scroller setContentOffset:CGPointMake(selectedAppointment * offset, scroller.contentOffset.y) animated:YES];
 }
 
 -(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
 [self performSelector:@selector(adjustScrollerOffset) withObject:nil afterDelay:0.1];
 //[scroller setContentOffset:CGPointMake(selectedAppointment * offset, scroller.contentOffset.y) animated:YES];
 }
 
 -(void)adjustScrollerOffset{
 [NSThread cancelPreviousPerformRequestsWithTarget:self];
 [scroller setContentOffset:CGPointMake(selectedIndex * offset, scroller.contentOffset.y) animated:YES];
 }*/

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView != scroller) return;
    // Determine which table cell the scrolling will stop on.
    
    
    CGFloat cellWidth = offset;//scrollView.contentSize.width / (CGFloat)numberOfAppointments;
    NSInteger cellIndex = floor(targetContentOffset->x / cellWidth);
    
    // Round to the next cell if the scrolling will stop over halfway to the next cell.
    //NSLog(@"%lf", (targetContentOffset->x - (floor(targetContentOffset->x / cellWidth) * cellWidth)));
    if ((targetContentOffset->x - (floor(targetContentOffset->x / cellWidth) * cellWidth)) > cellWidth/2.0) {
        cellIndex++;
    }
    
    // Adjust stopping point to exact beginning of cell.
    CGFloat targetX = MIN(cellIndex * cellWidth, scrollView.contentSize.width-scrollView.frame.size.width);
    
    //NSLog(@"%lf / %lf", targetX, scrollView.contentSize.width);
    targetContentOffset->x = targetX;
}

-(void)reloadScrollView{
    scroller.contentOffset = CGPointMake(0, 0);
    //hasCalculatedTomorrowsScrollerLength = NO;
    //hasCalculatedTodaysScrollerLength = NO;
    //tomorrowsScrollerLength = 0;
    //todaysScrollerLength = 0;
    for(UIView *subview in scroller.subviews) {
        [subview removeFromSuperview];
    }
    
    if (!appointments || appointments.count == 0) {
        noAppointmentsView.hidden = false;
        return;
    }
    //if(segTime.selectedSegmentIndex == 0)
    //scroller.contentSize = CGSizeMake(scroller.frame.size.width * 2, scroller.frame.size.height);
    //else
    //scroller.contentSize = CGSizeMake(tomorrowsAppointments.count * maxWidth, scroller.frame.size.height);
    numberOfAppointments = 0;
    
    if(segTime.selectedSegmentIndex == 0)
    {
        if(todaysAppointments.count == 0 || [Appointment currentAppointment].time != nil)
        {
            if([Appointment currentAppointment].time != nil)
                noAppointmentsView.hidden = YES;
            else
                noAppointmentsView.hidden = NO;
            btnSchedule.hidden = YES;
        }
        else{
            noAppointmentsView.hidden = YES;
            btnSchedule.hidden = NO;
            
            __block NSString *lastTime = @"";
            __block NSString *lastAMPM = @"";
            [todaysAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UIView *circle = nil;
                
                UILabel *lblNextAvailable = nil;
                UILabel *lblTime = nil;
                UILabel *lblAMPM = nil;
                
                
                double diameter = maxWidth;
                circle = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, diameter, diameter)];
                circle.backgroundColor = [UIColor whiteColor];
                circle.layer.cornerRadius = circle.frame.size.width/2;
                circle.tag = idx;
                circle.layer.zPosition = 1000 - idx;
                //[scroller addSubview:circle];
                
                lblNextAvailable = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 25.0, circle.frame.size.width, 50.0)];
                lblNextAvailable.backgroundColor = [UIColor clearColor];
                lblNextAvailable.textAlignment = NSTextAlignmentCenter;
                lblNextAvailable.font = [UIFont fontWithName:@"Avenir-Light" size:10.0];
                lblNextAvailable.tag = 1;
                [circle addSubview:lblNextAvailable];
                
                lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height / 2 - 25.0, circle.frame.size.width, 50.0)];
                lblTime.backgroundColor = [UIColor clearColor];
                lblTime.textAlignment = NSTextAlignmentCenter;
                lblTime.font = [UIFont fontWithName:@"Avenir-Book" size:50.0];
                lblTime.tag = 2;
                [circle addSubview:lblTime];
                
                lblAMPM = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height - 75.0, circle.frame.size.width, 50.0)];
                lblAMPM.backgroundColor = [UIColor clearColor];
                lblAMPM.textAlignment = NSTextAlignmentCenter;
                lblAMPM.font = [UIFont fontWithName:@"Avenir-Book" size:30.0];
                lblAMPM.tag = 3;
                [circle addSubview:lblAMPM];
                
                id Appointment = [todaysAppointments objectAtIndex:idx];
                
                lblNextAvailable.text = @"NEXT AVAILABLE VISIT";
                if(idx == 0)
                    lblNextAvailable.hidden = NO;
                else
                    lblNextAvailable.hidden = YES;
                
                
                
                lblTime.text = [Appointment objectForKey:@"time"];
                lblAMPM.text = [Appointment objectForKey:@"ampm"];
                
                if([lblTime.text isEqualToString:lastTime] && [lblAMPM.text isEqualToString:lastAMPM])
                {
                    //same so do not add
                }
                else{
                    //take picture of view and add it to scroller
                    //UIGraphicsBeginImageContext(CGSizeMake(diameter, diameter));
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(diameter, diameter), NO, 0.0);
                    [circle.layer renderInContext:UIGraphicsGetCurrentContext()];
                    UIImage *circleImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    UIImageView *img = [[UIImageView alloc] initWithImage:circleImage];
                    img.clipsToBounds = NO;
                    img.layer.shadowColor = [UIColor blackColor].CGColor;
                    img.layer.shadowOffset = CGSizeMake(0, 1);
                    img.layer.shadowOpacity = 0.7;
                    img.layer.shadowRadius = 2;
                    img.tag = idx;
                    img.userInteractionEnabled = YES;
                    [scroller addSubview:img];
                    
                    ++numberOfAppointments;
                    
                    UITapGestureRecognizer *apptTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseAppointment)];
                    apptTap.numberOfTapsRequired = 1;
                    [img addGestureRecognizer:apptTap];
                }
                
                
                
                lastTime = lblTime.text;
                lastAMPM = lblAMPM.text;
                
            }];
        }
    }
    else{
        if(tomorrowsAppointments.count == 0 || [Appointment currentAppointment].time != nil)
        {
            if([Appointment currentAppointment].time != nil)
                noAppointmentsView.hidden = YES;
            else
                noAppointmentsView.hidden = NO;
            btnSchedule.hidden = YES;
        }
        else{
            noAppointmentsView.hidden = YES;
            btnSchedule.hidden = NO;
            __block NSString *lastTime = @"";
            __block NSString *lastAMPM = @"";
            [tomorrowsAppointments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UIView *circle = nil;
                
                //UILabel *lblNextAvailable = nil;
                UILabel *lblTime = nil;
                UILabel *lblAMPM = nil;
                
                
                double diameter = maxWidth;
                circle = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, diameter, diameter)];
                circle.backgroundColor = [UIColor whiteColor];
                circle.layer.cornerRadius = circle.frame.size.width/2;
                //circle.clipsToBounds = NO;
                circle.tag = idx;
                /*circle.layer.shadowColor = [UIColor blackColor].CGColor;
                 circle.layer.shadowOffset = CGSizeMake(0, 1);
                 circle.layer.shadowOpacity = 0.7;
                 circle.layer.shadowRadius = 2;*/
                circle.layer.zPosition = 1000 - idx;
                //[scroller addSubview:circle];
                
                /*lblNextAvailable = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 25.0, circle.frame.size.width, 50.0)];
                 lblNextAvailable.backgroundColor = [UIColor clearColor];
                 lblNextAvailable.textAlignment = NSTextAlignmentCenter;
                 lblNextAvailable.font = [UIFont fontWithName:@"Avenir-Light" size:10.0];
                 lblNextAvailable.tag = 1;
                 [circle addSubview:lblNextAvailable];*/
                
                lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height / 2 - 25.0, circle.frame.size.width, 50.0)];
                lblTime.backgroundColor = [UIColor clearColor];
                lblTime.textAlignment = NSTextAlignmentCenter;
                lblTime.font = [UIFont fontWithName:@"Avenir-Book" size:50.0];
                lblTime.tag = 2;
                [circle addSubview:lblTime];
                
                lblAMPM = [[UILabel alloc] initWithFrame:CGRectMake(0.0, circle.frame.size.height - 75.0, circle.frame.size.width, 50.0)];
                lblAMPM.backgroundColor = [UIColor clearColor];
                lblAMPM.textAlignment = NSTextAlignmentCenter;
                lblAMPM.font = [UIFont fontWithName:@"Avenir-Book" size:30.0];
                lblAMPM.tag = 3;
                [circle addSubview:lblAMPM];
                
                id Appointment = Appointment = [tomorrowsAppointments objectAtIndex:idx];
                
                lblTime.text = [Appointment objectForKey:@"time"];
                lblAMPM.text = [Appointment objectForKey:@"ampm"];
                
                if([lblTime.text isEqualToString:lastTime] && [lblAMPM.text isEqualToString:lastAMPM])
                {
                    //same so do not add
                }
                else{
                    //take picture of view and add it to scroller
                    //UIGraphicsBeginImageContext(CGSizeMake(diameter, diameter));
                    UIGraphicsBeginImageContextWithOptions(CGSizeMake(diameter, diameter), NO, 0.0);
                    [circle.layer renderInContext:UIGraphicsGetCurrentContext()];
                    UIImage *circleImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    UIImageView *img = [[UIImageView alloc] initWithImage:circleImage];
                    img.clipsToBounds = NO;
                    img.layer.shadowColor = [UIColor blackColor].CGColor;
                    img.layer.shadowOffset = CGSizeMake(0, 1);
                    img.layer.shadowOpacity = 0.7;
                    img.layer.shadowRadius = 2;
                    img.tag = idx;
                    img.userInteractionEnabled = YES;
                    [scroller addSubview:img];
                    ++numberOfAppointments;
                    
                    UITapGestureRecognizer *apptTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseAppointment)];
                    apptTap.numberOfTapsRequired = 1;
                    [img addGestureRecognizer:apptTap];
                }
                
                lastTime = lblTime.text;
                lastAMPM = lblAMPM.text;
            }];
        }
        
    }
    
    
    __block int circleFound = 0;
    [scroller.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *circle = (UIImageView *)obj;
            if (circleFound > 0) {
                double diameter = minWidth +  (halfWidth - offset) * (widthDiff / halfWidth);
                circle.frame = CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter);
                //NSLog(@"%@", NSStringFromCGRect(circle.frame));
            }
            else{
                double diameter = maxWidth;
                circle.frame = CGRectMake((scroller.frame.size.width / 2 - diameter / 2) + (idx * offset), scroller.frame.size.height / 2 - diameter / 2, diameter, diameter);
                //NSLog(@">%@", NSStringFromCGRect(circle.frame));
            }
            circle.layer.zPosition = 1000 - idx * 10;
            circle.layer.cornerRadius = circle.frame.size.width/2;
            circleFound++;
        }
    }];
    scroller.contentSize = CGSizeMake((CGFloat)(numberOfAppointments+2) * offset, scroller.frame.size.height);
    //scroller.contentSize = CGSizeMake(scroller.frame.size.width * 2, scroller.frame.size.height);
    /*
     if(segTime.selectedSegmentIndex == 0 && hasCalculatedTodaysScrollerLength)
     {
     scroller.contentSize = CGSizeMake(todaysScrollerLength, scroller.frame.size.height);
     }
     else if(segTime.selectedSegmentIndex == 0 && !hasCalculatedTodaysScrollerLength)
     {
     scroller.contentSize = CGSizeMake((todaysAppointments.count * maxWidth), scroller.frame.size.height);
     }
     else if(segTime.selectedSegmentIndex == 1 && hasCalculatedTomorrowsScrollerLength){
     scroller.contentSize = CGSizeMake(tomorrowsScrollerLength, scroller.frame.size.height);
     }
     else if(segTime.selectedSegmentIndex == 1 && !hasCalculatedTomorrowsScrollerLength){
     scroller.contentSize = CGSizeMake((tomorrowsAppointments.count * maxWidth), scroller.frame.size.height);
     }*/
    //NSLog(@"%@", NSStringFromCGSize(scroller.contentSize));
    
    
}

-(void)chooseAppointment{
    [self scheduleAppointment:self]; //this is just like pressing the button
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    //appointmentCarousel = nil;
}

-(IBAction)goToProfile:(id)sender{
    [self performSegueWithIdentifier:@"showProfileSegue" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return locations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSInteger const kLocation = 100;
    static NSInteger const kAddress = 200;
    static NSInteger const kSelected = 300;
    
    UILabel *lblLocation = nil;
    UILabel *lblAddress = nil;
    UIImageView *imgSelected = nil;
    UIButton *btnEdit = nil;
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    lblLocation = (UILabel*)[cell.contentView viewWithTag:kLocation];
    lblAddress = (UILabel*)[cell.contentView viewWithTag:kAddress];
    imgSelected = (UIImageView *)[cell.contentView viewWithTag:kSelected];
    btnEdit = (UIButton *)[cell.contentView viewWithTag:0];
    
    id Location = [locations objectAtIndex:indexPath.row];
    
    lblLocation.text = [Location objectForKey:@"nickname"];
    lblAddress.text = [Location objectForKey:@"address1"];
    
    if(indexPath.row == selectedLocation)
        imgSelected.image = [UIImage imageNamed:@"icon_checked"];
    else
        imgSelected.image = [UIImage imageNamed:@"icon_unchecked"];
    
    btnEdit.tag = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 50.0)];
    header.backgroundColor = [UIColor whiteColor];
    if(locations.count > 0){
        id Location = [locations objectAtIndex:selectedLocation];
        
        UILabel *lblNickname = [[UILabel alloc] initWithFrame:CGRectMake(14.0, 9.0, 300.0, 20.0)];
        lblNickname.font = [UIFont boldSystemFontOfSize:12.0];
        lblNickname.text = [Location objectForKey:@"nickname"];
        [header addSubview:lblNickname];
        
        UILabel *lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(14.0, 25.0, 300.0, 20.0)];
        lblAddress.font = [UIFont systemFontOfSize:12.0];
        lblAddress.text = [Location objectForKey:@"address1"];
        [header addSubview:lblAddress];
        
        imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_down"]];
        imgArrow.frame = CGRectMake(tableView.frame.size.width - 30.0, 20.0, 15.0, 15.0);
        imgArrow.contentMode = UIViewContentModeScaleAspectFit;
        [header addSubview:imgArrow];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleLocationTable)];
        singleTap.numberOfTapsRequired = 1;
        [header addGestureRecognizer:singleTap];
    }
    else{
        //do nothing
    }
    
    
    return header;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50.0)];
    footer.backgroundColor = [UIColor blackColor];
    
    /*UILabel *lblAddAddress = [[UILabel alloc] initWithFrame:CGRectMake(14.0, 15.0, 300.0, 20.0)];
     lblAddAddress.font = [UIFont boldSystemFontOfSize:14.0];
     lblAddAddress.textColor = [UIColor whiteColor];
     lblAddAddress.text = @"ADD ADDRESS";
     [footer addSubview:lblAddAddress];*/
    
    UIButton *btnAddAddress = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAddAddress setTitle:@"ADD ADDRESS" forState:UIControlStateNormal];
    [btnAddAddress setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddAddress.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
    btnAddAddress.frame = CGRectMake(14.0, 15.0, 300.0, 20.0);
    //btnAddAddress.titleLabel.textAlignment = NSTextAlignmentLeft;
    btnAddAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //btnAddAddress.contentEdgeInsets = UIEdgeInsetsMake(0, 14, 0, 0);
    [btnAddAddress addTarget:self action:@selector(addAddress) forControlEvents:UIControlEventTouchUpInside];
    [footer addSubview:btnAddAddress];
    
    UIButton *btnArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnArrow setImage:[UIImage imageNamed:@"icon_arrow"] forState:UIControlStateNormal];
    btnArrow.frame = CGRectMake(tableView.frame.size.width - 40.0, 16.0, 26.0, 22.0);
    [btnArrow addTarget:self action:@selector(addAddress) forControlEvents:UIControlEventTouchUpInside];
    /*UIImageView *imgAddressArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_arrow"]];
     imgAddressArrow.frame = ;
     imgAddressArrow.contentMode = UIViewContentModeScaleAspectFit;*/
    [footer addSubview:btnArrow];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addAddress)];
    singleTap.numberOfTapsRequired = 1;
    [footer addGestureRecognizer:singleTap];
    
    return footer;
}

-(void)toggleLocationTable{
    ////NSLog(@"toggling table");
    [UIView animateWithDuration:0.3 animations:^{
        if(locationTableHeight.constant == 50){
            locationTableHeight.constant = locations.count * 50 + 100;
            imgArrow.image = [UIImage imageNamed:@"icon_up"];
            //segTimeUpperPadding.constant += (locations.count) * 50 + 50;
            
            
        }
        else{
            locationTableHeight.constant = 50;
            imgArrow.image = [UIImage imageNamed:@"icon_down"];
            //segTimeUpperPadding.constant -= (locations.count) * 50 + 50;
            
            
        }
        
    } completion:^(BOOL finished) {
        if(locationTableHeight.constant == 50)
        {
            //scroller.userInteractionEnabled = YES;
            [scrollerOverlay removeFromSuperview];
            scrollerOverlay = nil;
            scroller.layer.opacity = 1.0;
            scroller.userInteractionEnabled = YES;
            btnSchedule.enabled = YES;
            segTime.userInteractionEnabled = YES;
            selectorBG.userInteractionEnabled = NO;
            //noAppointmentsView.userInteractionEnabled = YES;
            //btnSchedule.layer.opacity = 1.0;
            //noAppointmentsView.layer.opacity = 1.0;
        }
        else{
            if(scrollerOverlay == nil)
            {
                scrollerOverlay = [[UIView alloc] initWithFrame:CGRectMake(0.0, locationTableHeight.constant, self.view.frame.size.width, dashboardMap.frame.size.height)];
            }
            scrollerOverlay.backgroundColor = [UIColor blackColor];
            scrollerOverlay.layer.opacity = 0.7;
            scrollerOverlay.layer.zPosition = 9000;
            
            UITapGestureRecognizer *overlayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeScrollerOverlay)];
            overlayTap.numberOfTapsRequired = 1;
            [scrollerOverlay addGestureRecognizer:overlayTap];
            //scrollerOverlay.frame = dashboardMap.frame;
            [self.view addSubview:scrollerOverlay];
            //[self.view insertSubview:scrollerOverlay aboveSubview:scroller];
            //scroller.layer.opacity = 0.4;
            scroller.userInteractionEnabled = NO;
            btnSchedule.enabled = NO;
            segTime.userInteractionEnabled = NO;
            selectorBG.userInteractionEnabled = YES;
            [segTime.superview sendSubviewToBack:segTime];
            //noAppointmentsView.userInteractionEnabled = NO;
            //btnSchedule.layer.opacity = 0.4;
            //noAppointmentsView.layer.opacity = 0.4;
        }
    }];
}

-(void)removeScrollerOverlay{
    [self toggleLocationTable];
}

-(void)adjustLocationTableHeight{
    //[UIView animateWithDuration:0.3 animations:^{
    //if(locationTableHeight.constant == 50){
    locationTableHeight.constant = locations.count * 50 + 100;
    imgArrow.image = [UIImage imageNamed:@"icon_up"];
    segTimeUpperPadding.constant += (locations.count) * 50 + 50;
    //}
    //else{
    //  locationTableHeight.constant = 50;
    // imgArrow.image = [UIImage imageNamed:@"icon_down"];
    // segTimeUpperPadding.constant -= (locations.count) * 50 + 50;
    //}
    
    //} completion:^(BOOL finished) {
    
    //}];
}

-(void)addAddress{
    ////NSLog(@"Adding address");
    [self performSegueWithIdentifier:@"addAddressSegue" sender:self];
}

-(IBAction)editAddress:(UIButton *)sender{
    ////NSLog(@"Editing address for location at index: %i", sender.tag);
    AddressToEdit = [locations objectAtIndex:sender.tag];
    
    [self performSegueWithIdentifier:@"editAddressSegue" sender:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //////NSLog(@"Being called");
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    selectedLocation = indexPath.row;
    [tableView reloadData];
    [self toggleLocationTable];
    id coordinates = [[locations objectAtIndex:selectedLocation] objectForKey:@"coordinates"];
    
    [User sharedUser].selectedLocationID = [[locations objectAtIndex:selectedLocation] valueForKey:@"objectId"];
    [[User sharedUser] save];
    
    NSNumber *latitude = [coordinates valueForKey:@"latitude"];
    NSNumber *longitude = [coordinates valueForKey:@"longitude"];
    
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.043;
    span.longitudeDelta=0.043;
    //32.782572, -96.802236
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue);
    
    region.span=span;
    region.center=coordinate;
    
    [dashboardMap setRegion:region animated:TRUE];
    [dashboardMap regionThatFits:region];
    
    [self getAppointments];
}

-(IBAction)changeDate:(id)sender{
    /*int i = 0;
     for (id segment in segTime.subviews){
     for (id label in [segment subviews])
     {
     if ([label isKindOfClass:[UILabel class]])
     {
     UILabel *titleLabel = (UILabel *) label;
     
     i++;
     titleLabel.textColor = [UIColor lightGrayColor];
     }
     }
     }*/
    int i = 0;
    for (id segment in segTime.subviews){
        for (id label in [segment subviews])
        {
            if ([label isKindOfClass:[UILabel class]])
            {
                
                UILabel *titleLabel = (UILabel *) label;
                //titleLabel.hidden = YES;
                
                titleLabel.numberOfLines = 0;
                NSString *segText = titleLabel.text;
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc]
                 initWithString:segText];
                
                //titleLabel.text = @"";
                titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:10.0];
                
                if(i == 0){
                    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x-60.0, titleLabel.frame.origin.y, 130.0, 45.0);
                    [text addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
                                 range:NSMakeRange(0, 8)];
                }
                else{
                    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x-60.0, titleLabel.frame.origin.y, 130.0, 45.0);
                    [text addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Avenir-Book" size:14.0]
                                 range:NSMakeRange(0, 5)];
                }
                i++;
                //(the width us typically too low)
                
                
                
                titleLabel.attributedText = text;
                titleLabel.textColor = [UIColor lightGrayColor];
            }
        }
    }
    
    
    //hasCalculatedScrollerLength = NO;
    
    [self reloadScrollView];
}

-(IBAction)cancelAppointment:(id)sender{
    
    blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
    blackout.backgroundColor = [UIColor blackColor];
    blackout.layer.opacity = 0.7;
    blackout.layer.zPosition = 3000;
    [self.view.window addSubview:blackout];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeDeletionConfirmation)];
    singleTap.numberOfTapsRequired = 1;
    [blackout addGestureRecognizer:singleTap];
    
    
    //willBeCharged = YES;
    
    if(willBeCharged){
        chargeForCancelView = [[UIView alloc] initWithFrame:CGRectMake(20.0, self.view.window.frame.size.height/2 - 135.0, self.view.frame.size.width - 40.0, 270.0)];
        chargeForCancelView.backgroundColor = [UIColor whiteColor];
        chargeForCancelView.layer.shadowColor = [UIColor blackColor].CGColor;
        chargeForCancelView.layer.shadowOffset = CGSizeMake(0, 1);
        chargeForCancelView.layer.shadowOpacity = 0.7;
        chargeForCancelView.clipsToBounds = NO;
        chargeForCancelView.layer.shadowRadius = 4;
        chargeForCancelView.layer.zPosition = 3001;
        [self.view.window addSubview:chargeForCancelView];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, chargeForCancelView.frame.size.width - 40.0, 30.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:16];
        lblTitle.text = @"CONFIRM";
        [chargeForCancelView addSubview:lblTitle];
        
        UILabel *lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 45.0, chargeForCancelView.frame.size.width - 40.0, 90.0)];
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
        lblMessage.numberOfLines = 0;
        [chargeForCancelView addSubview:lblMessage];
        
        //set amount based on time away from appointment
        //NSDate *appointmentDate = [Appointment currentAppointment].appointmentDate;
        
        //NSTimeInterval interval = [appointmentDate timeIntervalSinceDate:[NSDate date]];
        
        //if(interval > (60*60*3)){ //seconds * minutes * hours
        lblMessage.text = @"If you cancel, a charge of $75 will automatically be processed.";
        /*}
         else{
         NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
         
         if (dateComps.weekday == 7 || dateComps.weekday == 1) { //saturday or sunday
         //weekend
         lblMessage.text = @"If you cancel, a charge of $249 will automatically be processed.";
         }
         else{
         lblMessage.text = @"If you cancel, a charge of $199 will automatically be processed.";
         }
         }*/
        
        
        
        UILabel *lblSure = [[UILabel alloc] initWithFrame:CGRectMake(40.0, 125.0, chargeForCancelView.frame.size.width - 80.0, 60.0)];
        lblSure.textAlignment = NSTextAlignmentCenter;
        lblSure.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblSure.text = @"Are you sure you want to cancel this visit?";
        lblSure.numberOfLines = 0;
        [chargeForCancelView addSubview:lblSure];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(chargeForCancelView.frame.size.width/2 - 60.0 - 10.0, 190.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeDeletionConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [chargeForCancelView addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(chargeForCancelView.frame.size.width/2 + 10, 190.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
        [chargeForCancelView addSubview:btnConfirm];
    }
    else{
        deleteConfirmation = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.view.window.frame.size.height/2 - 80, self.view.window.frame.size.width - 100.0, 175.0)];
        deleteConfirmation.backgroundColor = [UIColor whiteColor];
        deleteConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
        deleteConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
        deleteConfirmation.layer.shadowOpacity = 0.7;
        deleteConfirmation.clipsToBounds = NO;
        deleteConfirmation.layer.shadowRadius = 4;
        deleteConfirmation.layer.zPosition = 3001;
        [self.view.window addSubview:deleteConfirmation];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, deleteConfirmation.frame.size.width - 40.0, 60.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblTitle.text = @"Are you sure you want to cancel this visit?";
        lblTitle.numberOfLines = 0;
        [deleteConfirmation addSubview:lblTitle];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(deleteConfirmation.frame.size.width/2 - 60.0 - 10.0, 100.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeDeletionConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(deleteConfirmation.frame.size.width/2 + 10, 100.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnConfirm];
    }
    
}

-(void)closeDeletionConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    if(deleteConfirmation){
        [deleteConfirmation removeFromSuperview];
        deleteConfirmation = nil;
    }
    if(chargeForCancelView){
        [chargeForCancelView removeFromSuperview];
        chargeForCancelView = nil;
    }
}

-(void)confirmDeletion{
    [blackout removeFromSuperview];
    blackout = nil;
    if(deleteConfirmation){
        [deleteConfirmation removeFromSuperview];
        deleteConfirmation = nil;
    }
    if(chargeForCancelView){
        [chargeForCancelView removeFromSuperview];
        chargeForCancelView = nil;
    }
    ////NSLog(@"Cancelling appointment");
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[Appointment currentAppointment].appointmentID forKey:@"appointmentId"];
    
    ////NSLog(@"params: %@", params);
    
    [PFCloud callFunctionInBackground:@"cancelAppointment" withParameters:params block:^(id object, NSError *error) {
        if(error == nil){
            ////NSLog(@"return obj: %@", object);
            locationTable.hidden = NO;
            nextVisit.hidden = NO;
            //secondVisit.hidden = YES;
            btnSchedule.hidden = NO;
            segTime.hidden = NO;
            selectorBG.hidden = NO;
            scroller.hidden = NO;
            [apptView removeFromSuperview];
            apptView = nil;
            [countdownView removeFromSuperview];
            countdownView = nil;
            [socialView removeFromSuperview];
            socialView = nil;
            [[Appointment currentAppointment] cancel];
            [self getAppointments];
            //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
        }
        else{
            ////NSLog(@"Error: %@", error.description);
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to cancel an appointment at this time."];
        }
    }];
    
    
}



- (IBAction)scheduleAppointment:(id)sender {
    
    id Appointment = nil;
    
    if(segTime.selectedSegmentIndex == 0 && todaysAppointments != nil && todaysAppointments.count > selectedAppointment){
        //today
        Appointment = [todaysAppointments objectAtIndex:selectedAppointment];
    }
    else if (segTime.selectedSegmentIndex == 1 &&tomorrowsAppointments != nil && tomorrowsAppointments.count > selectedAppointment){
        //tomorrow
        Appointment = [tomorrowsAppointments objectAtIndex:selectedAppointment];
    }
    
    if (Appointment) {
        blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
        blackout.backgroundColor = [UIColor blackColor];
        blackout.layer.opacity = 0.7;
        blackout.layer.zPosition = 2000;
        [self.view.window addSubview:blackout];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeConfirmation)];
        singleTap.numberOfTapsRequired = 1;
        [blackout addGestureRecognizer:singleTap];
        
        appointmentConfirmation = [[UIView alloc] initWithFrame:CGRectMake(30.0, self.view.window.frame.size.height/2 - 115.0, self.view.window.frame.size.width - 60.0, 230.0)];
        appointmentConfirmation.backgroundColor = [UIColor whiteColor];
        appointmentConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
        appointmentConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
        appointmentConfirmation.layer.shadowOpacity = 0.7;
        appointmentConfirmation.clipsToBounds = NO;
        appointmentConfirmation.layer.shadowRadius = 4;
        appointmentConfirmation.layer.zPosition = 2001;
        [self.view.window addSubview:appointmentConfirmation];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 20.0, appointmentConfirmation.frame.size.width, 30.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:16];
        lblTitle.text = @"CONFIRM VISIT";
        [appointmentConfirmation addSubview:lblTitle];
        
        UILabel *lblDay = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 55.0, appointmentConfirmation.frame.size.width, 30.0)];
        lblDay.textAlignment = NSTextAlignmentCenter;
        lblDay.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
        lblDay.text = [NSString stringWithFormat:@"%@ AT %@ %@", [Appointment objectForKey:@"day"], Appointment[@"time"], Appointment[@"ampm"]];
        [appointmentConfirmation addSubview:lblDay];
        
        /*UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 80.0, appointmentConfirmation.frame.size.width, 30.0)];
         lblTime.textAlignment = NSTextAlignmentCenter;
         lblTime.font = [UIFont fontWithName:@"Avenir-Heavy" size:18];
         lblTime.text = [NSString stringWithFormat:@"%@ %@", [Appointment objectForKey:@"time"], [Appointment objectForKey:@"ampm"]];
         [appointmentConfirmation addSubview:lblTime];*/
        
        id Location = [locations objectAtIndex:selectedLocation];
        
        UILabel *lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 90.0, appointmentConfirmation.frame.size.width, 30.0)];
        lblAddress.textAlignment = NSTextAlignmentCenter;
        lblAddress.font = [UIFont fontWithName:@"Avenir-Heavy" size:14];
        lblAddress.text = [NSString stringWithFormat:@"%@ %@",[Location objectForKey:@"address1"], Location[@"address2"]];
        lblAddress.numberOfLines = 0;
        [appointmentConfirmation addSubview:lblAddress];
        
        UILabel *lblCityZip = nil;
        
        if([Location objectForKey:@"address2"] != nil && ((NSString *)Location[@"address2"]).length > 0){
            lblCityZip = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 125.0, appointmentConfirmation.frame.size.width, 30.0)];
        }
        else{
            lblCityZip = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 110.0, appointmentConfirmation.frame.size.width, 30.0)];
        }
        
        lblCityZip.textAlignment = NSTextAlignmentCenter;
        lblCityZip.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblCityZip.text = [NSString stringWithFormat:@"%@, %@ %@", [Location objectForKey:@"city"], Location[@"state"], Location[@"zip"]];
        [appointmentConfirmation addSubview:lblCityZip];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(appointmentConfirmation.frame.size.width/2 - 60.0 - 10.0, 155.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [appointmentConfirmation addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(appointmentConfirmation.frame.size.width/2 + 10, 155.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmAppointment) forControlEvents:UIControlEventTouchUpInside];
        [appointmentConfirmation addSubview:btnConfirm];
    }
    else{
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showModal:@"Please select an appointment time first."];
    }
}


-(void)closeConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    [appointmentConfirmation removeFromSuperview];
    appointmentConfirmation = nil;
}

-(void)confirmAppointment{
    [blackout removeFromSuperview];
    blackout = nil;
    [appointmentConfirmation removeFromSuperview];
    appointmentConfirmation = nil;
    
    id Appointment = nil;
    
    if(segTime.selectedSegmentIndex == 0 && todaysAppointments != nil && todaysAppointments.count > selectedAppointment){
        //today
        Appointment = [todaysAppointments objectAtIndex:selectedAppointment];
    }
    else if (segTime.selectedSegmentIndex == 1 &&tomorrowsAppointments != nil && tomorrowsAppointments.count > selectedAppointment){
        //tomorrow
        Appointment = [tomorrowsAppointments objectAtIndex:selectedAppointment];
    }
    
    if (Appointment) {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        NSDictionary *customerDict = @{@"__type":@"Object",@"className":@"Customer",@"objectId":[User sharedUser].customerID};
        [params setValue:customerDict forKey:@"customer"];
        
        id Location = [locations objectAtIndex:selectedLocation];
        
        NSDictionary *locationDict = @{@"address1":[Location objectForKey:@"address1"],@"address2":[Location objectForKey:@"address2"],@"city":[Location objectForKey:@"city"], @"state":[Location objectForKey:@"state"], @"zip":[Location objectForKey:@"zip"]};
        [params setValue:locationDict forKey:@"address"];
        
        //id Location = [locations objectAtIndex:selectedLocation];
        //[params setValue:Location forKey:@"address"];
        
        [params setValue:[Appointment objectForKey:@"date"] forKey:@"appointmentTime"];
        //[params setValue:@"2015-02-19T08:00:00.000Z" forKey:@"appointmentTime"];
        ////NSLog(@"params: %@", params);
        
        
        
        [PFCloud callFunctionInBackground:@"postAppointment" withParameters:params block:^(id object, NSError *error) {
            if(error == nil){
                ////NSLog(@"return obj: %@", object);
                ReturnAppointment = object;
                [self performSelectorOnMainThread:@selector(saveAppointment:) withObject:@YES waitUntilDone:NO];
            }
            else{
                ////NSLog(@"Error: %@", error.description);
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate showModal:@"Unable to make an appointment at this time."];
                [self getAppointments];
            }
        }];
    }
    else{
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showModal:@"Please select an appointment time first."];
    }
    
}

-(void)saveAppointment:(NSNumber *)showConfirmation{
    [Appointment currentAppointment].appointmentID = [ReturnAppointment valueForKey:@"objectId"];
    
    NSDate *createdAt = [ReturnAppointment createdAt];
    
    [Appointment currentAppointment].createdDate = createdAt;
    
    if([ReturnAppointment objectForKey:@"address2"] != nil && ((NSString *)[ReturnAppointment objectForKey:@"address2"]).length > 0){
        [Appointment currentAppointment].address1 = [NSString stringWithFormat:@"%@, %@",[ReturnAppointment objectForKey:@"address1"], [ReturnAppointment objectForKey:@"address2"]];
    }
    else{
        [Appointment currentAppointment].address1 = [ReturnAppointment objectForKey:@"address1"];
    }
    
    [Appointment currentAppointment].address2 = [NSString stringWithFormat:@"%@, %@ %@", [ReturnAppointment objectForKey:@"city"], [ReturnAppointment objectForKey:@"state"], [ReturnAppointment objectForKey:@"zip"]];
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM. dd"];
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    
    NSDateComponents *todayComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"yyyy.MM.dd HH:mm:ss zzz"];
    
    
    NSDate *date = [ReturnAppointment objectForKey:@"time"];
    if(date){
        NSString *localDateTime = [df_local stringFromDate:date];
        NSDate *localDate = [df_local dateFromString:localDateTime];
        NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:localDate];
        //dateComps.timeZone = [NSTimeZone timeZoneWithName:@"CST"];
        
        NSDateFormatter *time = [[NSDateFormatter alloc] init];
        [time setDateFormat:@"h:mm a"];
        
        //seperate by day
        if(todayComps.day == dateComps.day)
        {
            [Appointment currentAppointment].day = @"TODAY";
        }
        else{
            [Appointment currentAppointment].day = @"TOMORROW";
        }
        
        [Appointment currentAppointment].time = [time stringFromDate:date];
        
        [Appointment currentAppointment].appointmentDate = date;
    }
    
    NSDate *cancelBy = [ReturnAppointment objectForKey:@"cancelBy"];
    
    if(cancelBy != nil)
    {
        [Appointment currentAppointment].cancelByDate = cancelBy;
    }
    
    id Provider = [ReturnAppointment objectForKey:@"provider"];
    
    [Appointment currentAppointment].physicianTitle = [Provider objectForKey:@"title"];
    
    
    PFQuery *userQuery = [PFUser query];
    [userQuery getObjectInBackgroundWithId:[[Provider valueForKey:@"user"] valueForKey:@"objectId"] block:^(PFObject *object, NSError *error) {
        if(!error){
            NSString *name = [NSString stringWithFormat:@"%@ %@", [object objectForKey:@"firstName"], [object objectForKey:@"lastName"]];
            [Appointment currentAppointment].physician = name;
            [[Appointment currentAppointment] save];
            
            PFFile *userImageFile = Provider[@"avatar"];
            [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    [Appointment currentAppointment].physicianImage = imageData;
                    [[Appointment currentAppointment] save];
                    if([showConfirmation isEqual:@YES])
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentScheduled" object:nil];
                    else{
                        [self showAppointmentView];
                        [self showSocialView];
                    }
                }
            }];
            //[Appointment currentAppointment].physicianImage = [userImageFile getData:nil];
            //[[Appointment currentAppointment] save];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentScheduled" object:nil];
            
            /**/
        }
    }];
    
    [PFCloud callFunctionInBackground:@"getTime" withParameters:@{} block:^(id object, NSError *error) {
        if(error == nil){
            //set drift here
            NSDate *serverDate = (NSDate *)object;
            NSTimeInterval difference = [serverDate timeIntervalSinceDate:[NSDate date]];
            [Appointment currentAppointment].cancelByDate = [[Appointment currentAppointment].cancelByDate dateByAddingTimeInterval:difference];
            [[Appointment currentAppointment] save];
        }
        else{
            //just leave the current time on the device
            //NSLog(@"error: %@", error);
        }
    }];
}

-(void)showVisitInfo{
    [self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
}

- (IBAction)logout:(id)sender {
    [[User sharedUser] logout];
    locationTable.hidden = NO;
    nextVisit.hidden = NO;
    //secondVisit.hidden = YES;
    btnSchedule.hidden = NO;
    segTime.hidden = NO;
    selectorBG.hidden = NO;
    scroller.hidden = NO;
    [apptView removeFromSuperview];
    apptView = nil;
    [countdownView removeFromSuperview];
    countdownView = nil;
    [socialView removeFromSuperview];
    socialView = nil;
    willBeCharged = NO;
    [self performSegueWithIdentifier:@"patientLoginSegue" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[UINavigationController class]]){
        UINavigationController *nav = (UINavigationController *)segue.destinationViewController;
        if([[nav.viewControllers objectAtIndex:0] isKindOfClass:[PatientLocationViewController class]]){
            PatientLocationViewController *vc = (PatientLocationViewController *)[nav.viewControllers objectAtIndex:0];
            vc.modal = YES;
            
            if ([segue.identifier isEqualToString:@"editAddressSegue"]) {
                vc.editMode = YES;
                vc.AddressToEdit = AddressToEdit;
            }
        }
    }
    else if([segue.destinationViewController isKindOfClass:[VisitConfirmationViewController class]]){
        
        VisitConfirmationViewController *vc = (VisitConfirmationViewController *)segue.destinationViewController;
        vc.theAppointment = ReturnAppointment;
        vc.countdownString = lblCountdown.text;
        vc.willBeCharged = willBeCharged;
    }
    //[Appointment currentAppointment];
}


@end
