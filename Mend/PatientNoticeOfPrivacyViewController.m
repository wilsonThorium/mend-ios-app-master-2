//
//  PatientNoticeOfPrivacyViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/21/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientNoticeOfPrivacyViewController.h"
#import "PatientSignUpViewController.h"

@interface PatientNoticeOfPrivacyViewController ()
{
    NSString *privacyURL;
    IBOutlet UIWebView *browser;
    BOOL accepted;
}
@end

@implementation PatientNoticeOfPrivacyViewController
@synthesize TOSInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Patient Privacy Notice";
    privacyURL = @"http://www.mendathome.com/mobile-notice-of-privacy-practices/";
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [NSURL URLWithString: privacyURL] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 60];
    [browser loadRequest: request];
    
    /*UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfile)];
    singleTap.numberOfTapsRequired = 1;
    [footer addGestureRecognizer:singleTap];*/
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    accepted = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)accept:(id)sender {
    accepted = YES;
}

-(void)goToProfile{
    [self performSegueWithIdentifier:@"segueShowProfile" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientSignUpViewController class]]){
        PatientSignUpViewController *vc = (PatientSignUpViewController *)segue.destinationViewController;
        vc.TOSInfo = TOSInfo;
        vc.NPPInfo = @{@"nppAccepted":[NSNumber numberWithBool:accepted],@"nppTimestamp":[NSDate date]};
    }
    else{
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate showModal:@"Please contact Customer Service at (469) 458-6363 for information about our Privacy Policy."];
    }
}


@end
