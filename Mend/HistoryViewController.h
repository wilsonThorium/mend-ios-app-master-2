//
//  HistoryViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/2/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface HistoryViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@end
