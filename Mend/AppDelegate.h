//
//  AppDelegate.h
//  Mend
//
//  Created by Andrew Goodwin on 12/14/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stripe.h"
#import <Google/SignIn.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)showModal:(NSString *)message;
-(void)showModalWithSuccess:(NSString *)message;
-(void)showProcessingModal:(NSString *)message;
-(void)hideModal;
@end

