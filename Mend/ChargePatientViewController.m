//
//  ChargePatientViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "ChargePatientViewController.h"
#import "PhysicianDashboardViewController.h"

@interface ChargePatientViewController ()
{
    NSMutableArray *chargeOptions;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblDateTime;
    IBOutlet UITableView *chargeTable;
    NSMutableArray *selections;
    
    UIView *blackout;
    UIView *chargeConfirmation;
    
    __block double runningTotal;
}
@end

@implementation ChargePatientViewController
@synthesize appointment, imgPatient;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Charge Patient";
    runningTotal = 0;
    [self getPrices];
    //chargeOptions = [[NSMutableArray alloc] initWithArray:@[@{@"option":@"House Call (Monday - Friday)", @"price":@"$250"},@{@"option":@"House Call (Saturday)", @"price":@"$350"},@{@"option":@"Oral Medication", @"price":@"$25"},@{@"option":@"Injectable Medication", @"price":@"$75"},@{@"option":@"House Call (Monday - Friday)", @"price":@"$250"},@{@"option":@"House Call (Saturday)", @"price":@"$350"},@{@"option":@"Oral Medication", @"price":@"$25"},@{@"option":@"Injectable Medication", @"price":@"$75"},@{@"option":@"House Call (Monday - Friday)", @"price":@"$250"},@{@"option":@"House Call (Saturday)", @"price":@"$350"},@{@"option":@"Oral Medication", @"price":@"$25"},@{@"option":@"Injectable Medication", @"price":@"$75"}]];
    
    id customer = [appointment objectForKey:@"customer"];
    id user = [customer objectForKey:@"user"];
    NSString *name = [NSString stringWithFormat:@"%@ %@",[user objectForKey:@"firstName"], [user objectForKey:@"lastName"]];
    
    NSString *dateString = [self getLocalDate:[appointment objectForKey:@"time"]];
    
    lblDateTime.text = dateString;
    lblName.text = name;
    
    selections = [[NSMutableArray alloc] init];
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = YES;
    imgProfile.image = imgPatient;
    
    // Do any additional setup after loading the view.
}

-(NSString *)getLocalDate:(NSDate *)utc{
    
    //NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //[df_utc setDateFormat:@"yyyy-MM-dd'T'HHmmss.zzzZ"];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone timeZoneWithName:@"CST"]];
    [df_local setDateFormat:@"h:mm a"];
    
    NSString *localDateTime = [df_local stringFromDate:utc];
    /*NSDate *localDate = [df_local dateFromString:localDateTime];
     
     NSDateFormatter* timeFormat = [[NSDateFormatter alloc] init];
     [timeFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
     [timeFormat setDateFormat:@"h:mm a"];*/
    
    return localDateTime;
}

-(void)getPrices{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[User sharedUser].providerID forKey:@"providerId"];
    [PFCloud callFunctionInBackground:@"getPricing" withParameters:params block:^(id objects, NSError *error) {
        if (error) {
            ////NSLog(@"The getObjects request failed.");
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to get prices at this time."];
        } else {
            // The find succeeded.
            ////NSLog(@"objects: %@", objects);
            chargeOptions = [[NSMutableArray alloc] initWithArray:objects];
            [chargeTable reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return chargeOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSInteger const kSelected = 100;
    static NSInteger const kChoice = 200;
    static NSInteger const kPrice = 300;
    
    UIImageView *imgSelected = nil;
    UILabel *lblChoice = nil;
    UILabel *lblPrice = nil;
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    lblChoice = (UILabel*)[cell.contentView viewWithTag:kChoice];
    lblPrice = (UILabel*)[cell.contentView viewWithTag:kPrice];
    imgSelected = (UIImageView *)[cell.contentView viewWithTag:kSelected];
    
    id Option = [chargeOptions objectAtIndex:indexPath.row];
    
    if([selections indexOfObject:[NSNumber numberWithInt:indexPath.row]] == NSNotFound)
    {
        imgSelected.image = [UIImage imageNamed:@"icon_unchecked"];
    }
    else{
        imgSelected.image = [UIImage imageNamed:@"icon_checked"];
    }
    lblChoice.text = [Option objectForKey:@"option"];
    lblPrice.text = [NSString stringWithFormat:@"$%@",[Option objectForKey:@"price"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *chargeView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 50.0)];
    chargeView.backgroundColor = [UIColor blackColor];
    
    runningTotal = 0;
    [selections enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id chargeOption = [chargeOptions objectAtIndex:((NSNumber *)obj).intValue];
        runningTotal += ((NSNumber *)[chargeOption objectForKey:@"price"]).doubleValue;
    }];
    
    UIButton *btnCharge = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCharge.frame = chargeView.frame;
    [btnCharge setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCharge setTitle:[NSString stringWithFormat:@"CHARGE ($%.0f)", runningTotal] forState:UIControlStateNormal];
    [btnCharge addTarget:self action:@selector(chargePatient:) forControlEvents:UIControlEventTouchUpInside];
    [chargeView addSubview:btnCharge];
    
    return chargeView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([selections indexOfObject:[NSNumber numberWithInt:indexPath.row]] == NSNotFound)
    {
        //it's not selected
        [selections addObject:[NSNumber numberWithInt:indexPath.row]];
    }
    else{
        [selections removeObject:[NSNumber numberWithInt:indexPath.row]];
    }
    
    [chargeTable reloadData];
}

-(IBAction)chargePatient:(id)sender{

    blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
    blackout.backgroundColor = [UIColor blackColor];
    blackout.layer.opacity = 0.7;
    blackout.layer.zPosition = 2000;
    [self.view.window addSubview:blackout];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeConfirmation)];
    singleTap.numberOfTapsRequired = 1;
    [blackout addGestureRecognizer:singleTap];

    chargeConfirmation = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.view.window.frame.size.height/2 - 80, self.view.window.frame.size.width - 100.0, 175.0)];
    chargeConfirmation.backgroundColor = [UIColor whiteColor];
    chargeConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
    chargeConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
    chargeConfirmation.layer.shadowOpacity = 0.7;
    chargeConfirmation.clipsToBounds = NO;
    chargeConfirmation.layer.shadowRadius = 4;
    chargeConfirmation.layer.zPosition = 2001;
    [self.view.window addSubview:chargeConfirmation];

    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 5.0, chargeConfirmation.frame.size.width - 40.0, 60.0)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:14];
    lblTitle.text = @"TOTAL CHARGE";
    [chargeConfirmation addSubview:lblTitle];
    
    UILabel *lblChargeAmount = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 40.0, chargeConfirmation.frame.size.width - 40.0, 60.0)];
    lblChargeAmount.textAlignment = NSTextAlignmentCenter;
    lblChargeAmount.font = [UIFont fontWithName:@"Avenir-Heavy" size:28];
    lblChargeAmount.text = [NSString stringWithFormat:@"$%.0f", runningTotal];
    [chargeConfirmation addSubview:lblChargeAmount];

    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(chargeConfirmation.frame.size.width/2 - 60.0 - 10.0, 100.0, 60.0, 60.0);
    [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    [btnCancel setTitle:@"" forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(closeConfirmation) forControlEvents:UIControlEventTouchUpInside];
    [chargeConfirmation addSubview:btnCancel];

    UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
    btnConfirm.frame = CGRectMake(chargeConfirmation.frame.size.width/2 + 10, 100.0, 60.0, 60.0);
    [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
    [btnConfirm setTitle:@"" forState:UIControlStateNormal];
    [btnConfirm addTarget:self action:@selector(confirmCharge) forControlEvents:UIControlEventTouchUpInside];
    [chargeConfirmation addSubview:btnConfirm];
}

-(void)closeConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    [chargeConfirmation removeFromSuperview];
    chargeConfirmation = nil;
}

-(void)confirmCharge{
    ////NSLog(@"Charging Patient");
    [blackout removeFromSuperview];
    blackout = nil;
    [chargeConfirmation removeFromSuperview];
    chargeConfirmation = nil;
    //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
    
    id customer = [appointment objectForKey:@"customer"];
    //id user = [customer objectForKey:@"user"];
    
    NSString *stripeCustomerID = [customer objectForKey:@"stripeCustomerId"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    //[params setValue:[appointment objectForKey:@"objectId"] forKey:@"appointmentId"];
    [params setValue:stripeCustomerID forKey:@"stripeCustomerId"];
    [params setValue:[NSNumber numberWithDouble:runningTotal * 100] forKey:@"amount"]; // * 100 because it is in cents
    
    __block NSMutableArray *charges = [[NSMutableArray alloc] init];
    [chargeOptions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([selections indexOfObject:[NSNumber numberWithInt:idx]] != NSNotFound)
        {
            [charges addObject:[obj valueForKey:@"objectId"]];
        }
    }];
    if(charges.count == 0)
    {
        [charges addObject:@-1];
    }
    [params setValue:charges forKey:@"items"];
    [params setValue:[appointment valueForKey:@"objectId"] forKey:@"appointmentId"];
    
    [PFCloud callFunctionInBackground:@"postStripeCharge" withParameters:params block:^(id object, NSError *error) {
        if(error == nil){
            //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModalWithSuccess:@"Customer Charged"];
            __block BOOL foundView = NO;
            [self.navigationController.viewControllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UIViewController *viewController = (UIViewController *)obj;
                if([viewController.nibName isEqualToString:@"PhysicianDashboardViewController"])
                {
                    foundView = YES;
                    [self.navigationController popToViewController:viewController animated:YES];
                    *stop = YES;
                }
            }];
            
            if(!foundView)
                [self.navigationController popToRootViewControllerAnimated:YES];
            //[self.navigationController popViewControllerAnimated:YES];
            //[self.navigationController popToViewController:vc animated:YES];
        }
        else{
            ////NSLog(@"Error: %@", error.description);
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to charge customer at this time."];
        }
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
