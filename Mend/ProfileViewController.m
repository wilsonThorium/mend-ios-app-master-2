//
//  ProfileViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/20/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "ProfileViewController.h"
#import "PatientSignUpViewController.h"
#import "PatientPaymentInfoViewController.h"
#import "TermsWebViewController.h"
#import "TourViewController.h"

@interface ProfileViewController ()
{
    NSArray *options;
    IBOutlet UIImageView *imgProfile;
    UIView *blackout;
    UIView *deleteConfirmation;
    IBOutlet UILabel *lblName;
    NSString *termTitle;
    NSString *url;
}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    options = @[@"PRICE LIST", @"CHANGE PAYMENT INFO", @"MY VISIT HISTORY", @"TOUR", @"FAQ", @"TERMS OF USE", @"PRIVACY NOTICE", @"CONTACT US", @"COMPLAINTS", @"DELETE ACCOUNT"];
    // Do any additional setup after loading the view.
    /*[self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];*/
    
    //[[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDataChanged" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self loadUserData];
    }];
    
    [self loadUserData];
}

-(void)loadUserData{
    //TODO: set profile image
    if([User sharedUser].customerID != nil){
        PFQuery *query = [PFQuery queryWithClassName:@"Customer"];
        
        // Retrieve the object by id
        [query getObjectInBackgroundWithId:[User sharedUser].customerID block:^(PFObject *customer, NSError *error) {
            if(!error){
                PFFile *userImageFile = customer[@"avatar"];
                [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                    if (!error) {
                        imgProfile.image = [UIImage imageWithData:imageData];
                    }
                }];
                
            }
            else{
                ////NSLog(@"Error retrieving profile image");
            }
        }];
    }
    else if ([User sharedUser].providerID != nil){
        PFQuery *query = [PFQuery queryWithClassName:@"Provider"];
        
        // Retrieve the object by id
        [query getObjectInBackgroundWithId:[User sharedUser].providerID block:^(PFObject *customer, NSError *error) {
            if(!error){
                PFFile *userImageFile = customer[@"avatar"];
                [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                    if (!error) {
                        imgProfile.image = [UIImage imageWithData:imageData];
                    }
                }];
                
            }
            else{
                ////NSLog(@"Error retrieving profile image");
            }
        }];
    }
    
    
    lblName.text = [NSString stringWithFormat:@"%@ %@", [User sharedUser].firstName, [User sharedUser].lastName];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName = @"Patient Profile";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBarHidden = NO;
    /*self.navigationController.navigationBar.layer.borderWidth = 0;
    self.navigationController.navigationBar.layer.shadowOpacity = 0;
    self.view.layer.shadowOpacity = 0;*/
    //[self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    //[self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    id Option = [options objectAtIndex:indexPath.row];
    
    cell.textLabel.text = Option;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id Option = [options objectAtIndex:indexPath.row];
    if([Option isEqualToString:@"MY VISIT HISTORY"]){
        [self performSegueWithIdentifier:@"segueShowHistory" sender:self];
    }
    else if([Option isEqualToString:@"DELETE ACCOUNT"]){
        blackout = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.window.frame.size.width, self.view.window.frame.size.height)];
        blackout.backgroundColor = [UIColor blackColor];
        blackout.layer.opacity = 0.7;
        blackout.layer.zPosition = 2000;
        [self.view.window addSubview:blackout];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeConfirmation)];
        singleTap.numberOfTapsRequired = 1;
        [blackout addGestureRecognizer:singleTap];
        
        deleteConfirmation = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.view.window.frame.size.height/2 - 80, self.view.window.frame.size.width - 100.0, 175.0)];
        deleteConfirmation.backgroundColor = [UIColor whiteColor];
        deleteConfirmation.layer.shadowColor = [UIColor blackColor].CGColor;
        deleteConfirmation.layer.shadowOffset = CGSizeMake(0, 1);
        deleteConfirmation.layer.shadowOpacity = 0.7;
        deleteConfirmation.clipsToBounds = NO;
        deleteConfirmation.layer.shadowRadius = 4;
        deleteConfirmation.layer.zPosition = 2001;
        [self.view.window addSubview:deleteConfirmation];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 20.0, deleteConfirmation.frame.size.width - 40.0, 60.0)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont fontWithName:@"Avenir-Book" size:14];
        lblTitle.text = @"Are you sure you want to delete your account?";
        lblTitle.numberOfLines = 0;
        [deleteConfirmation addSubview:lblTitle];
        
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(deleteConfirmation.frame.size.width/2 - 60.0 - 10.0, 100.0, 60.0, 60.0);
        [btnCancel setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [btnCancel setTitle:@"" forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(closeConfirmation) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnCancel];
        
        UIButton *btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        btnConfirm.frame = CGRectMake(deleteConfirmation.frame.size.width/2 + 10, 100.0, 60.0, 60.0);
        [btnConfirm setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
        [btnConfirm setTitle:@"" forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(confirmDeletion) forControlEvents:UIControlEventTouchUpInside];
        [deleteConfirmation addSubview:btnConfirm];
    }
    else if([Option isEqualToString:@"PRICE LIST"]){
        [self performSegueWithIdentifier:@"showPriceListSegue" sender:self];
    }
    else if([Option isEqualToString:@"CHANGE PAYMENT INFO"]){
        [self performSegueWithIdentifier:@"segueShowPayment" sender:self];
    }
    else if([Option isEqualToString:@"TOUR"]){
        [self performSegueWithIdentifier:@"segueShowTour" sender:self];
    }
    else if([Option isEqualToString:@"FAQ"]){
        termTitle = @"FAQ";
        url = @"http://www.mendathome.com/mobile-faq/";
        [self performSegueWithIdentifier:@"segueShowTerms" sender:self];
    }
    else if([Option isEqualToString:@"TERMS OF USE"]){
        termTitle = @"TERMS OF USE";
        url = @"http://www.mendathome.com/mobile-terms-of-use/";
        [self performSegueWithIdentifier:@"segueShowTerms" sender:self];
        //http://stage.mendathome.com/mobile-notice-of-privacy-practices/
        //
    }
    else if([Option isEqualToString:@"PRIVACY NOTICE"]){
        termTitle = @"PRIVACY NOTICE";
        url = @"http://www.mendathome.com/mobile-notice-of-privacy-practices/";
        [self performSegueWithIdentifier:@"segueShowTerms" sender:self];
        //http://stage.mendathome.com/mobile-notice-of-privacy-practices/
        //
    }
    else if([Option isEqualToString:@"COMPLAINTS"]){
        termTitle = @"COMPLAINTS";
        url = @"http://www.mendathome.com/mobile-complaints/";
        [self performSegueWithIdentifier:@"segueShowTerms" sender:self];
        //http://stage.mendathome.com/mobile-notice-of-privacy-practices/
        //
    }
    else if([Option isEqualToString:@"CONTACT US"]){
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
        if (mailClass != nil)
        {
            if ([mailClass canSendMail])
            {
                [self displayComposerSheet];
            }
        }
    }
}

-(void)displayComposerSheet
{
    //alloc and init the MFMailComposeViewController
    [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:209.0/255.0 green:28.0/255.0 blue:24.0/255.0 alpha:1.0];
    //[UINavigationBar appearance].barTintColor = [UIColor redColor];
    [UINavigationBar appearance].translucent = NO;
    //[self.navigationController.navigationBar setTranslucent:NO];
    //[UINavigationBar appearance].tintColor = [UIColor whiteColor];
    
    MFMailComposeViewController *email = [[MFMailComposeViewController alloc] init];
    //set the delegate to the current View controller
    email.mailComposeDelegate = self;
    
    //Set the subject of the email
    //[email setSubject:@"Please review this modem"];
    
    //set the to: address
    NSArray *toRecipients = [NSArray arrayWithObject:@"info@mendathome.com"];
    [email setToRecipients:toRecipients];
    
    // body text
    /*NSString *emailBody = ];
     [email setMessageBody:emailBody isHTML:NO];*/
    [self presentViewController:email animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{	
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)closeConfirmation{
    [blackout removeFromSuperview];
    blackout = nil;
    [deleteConfirmation removeFromSuperview];
    deleteConfirmation = nil;
}

-(void)confirmDeletion{
    [blackout removeFromSuperview];
    blackout = nil;
    [deleteConfirmation removeFromSuperview];
    deleteConfirmation = nil;
    
    [PFUser currentUser][@"softDelete"] = [NSNumber numberWithBool:YES];
    
    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Your account has been deleted.  We're sorry to see you go."];
            [[User sharedUser] logout];
            //[[Appointment currentAppointment] cancel];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        else{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate showModal:@"Unable to delete your account at this time."];
        }
    }];
    //[self performSegueWithIdentifier:@"segueShowVisitConfirmation" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientSignUpViewController class]]){
        PatientSignUpViewController *vc = (PatientSignUpViewController *)segue.destinationViewController;
        vc.editMode = YES;
    }
    else if([segue.destinationViewController isKindOfClass:[PatientPaymentInfoViewController class]]){
        PatientPaymentInfoViewController *vc = (PatientPaymentInfoViewController *)segue.destinationViewController;
        vc.editMode = YES;
    }
    else if([segue.destinationViewController isKindOfClass:[TermsWebViewController class]]){
        TermsWebViewController *vc = (TermsWebViewController *)segue.destinationViewController;
        vc.title = termTitle;
        vc.url = url;
    }
    else if([segue.destinationViewController isKindOfClass:[TourViewController class]]){
        TourViewController *vc = (TourViewController *)segue.destinationViewController;
        vc.fromProfile = YES;
    }
}

- (IBAction)debugPhysician:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ProviderLoggedIn" object:nil];
}

@end
