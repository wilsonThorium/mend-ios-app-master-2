//
//  PriceListViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/16/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PriceListViewController : GAITrackedViewController

@end
