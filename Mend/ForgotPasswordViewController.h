//
//  ForgotPasswordViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/17/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface ForgotPasswordViewController : GAITrackedViewController <UITextFieldDelegate>

@end
