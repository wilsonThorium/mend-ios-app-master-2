//
//  PatientLocationIntermediateViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 1/16/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "PatientLocationIntermediateViewController.h"
#import "PatientLocationViewController.h"

@interface PatientLocationIntermediateViewController ()
{
    IBOutlet MKMapView *intermediateMap;
    IBOutlet UIView *popup;
}
@end

@implementation PatientLocationIntermediateViewController
@synthesize profileImage, stripeID, signUpInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    popup.layer.shadowColor = [UIColor blackColor].CGColor;
    popup.layer.shadowOffset = CGSizeMake(0.0,-1.0);
    popup.layer.shadowOpacity = 0.7;
    popup.layer.shadowRadius = 4.0;
    
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.043;
    span.longitudeDelta=0.043;
    //32.782572, -96.802236
    //32.792572, -96.794236
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(32.865482, -96.786903);
    
    region.span=span;
    region.center=coordinate;
    
    [intermediateMap setRegion:region animated:TRUE];
    [intermediateMap regionThatFits:region];
    intermediateMap.userInteractionEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName = @"Patient Location (Intermediate)";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[PatientLocationViewController class]]){
        PatientLocationViewController *vc = (PatientLocationViewController *)segue.destinationViewController;
        vc.signUpInfo = signUpInfo;
        vc.profileImage = profileImage;
        vc.stripeID = stripeID;
    }
}

@end
