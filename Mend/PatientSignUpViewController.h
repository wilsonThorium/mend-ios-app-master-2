//
//  PatientSignUpViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 1/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PatientSignUpViewController : GAITrackedViewController <UITextFieldDelegate, UIScrollViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{

}
@property(nonatomic, assign) BOOL editMode;
@property(nonatomic, strong) id TOSInfo;
@property(nonatomic, strong) id NPPInfo;
@end
