//
//  PhysicianDashboardViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/2/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GPS.h"
#import "GAITrackedViewController.h"

@interface PhysicianDashboardViewController : GAITrackedViewController <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, GPSDelegate>

@end
