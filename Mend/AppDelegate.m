//
//  AppDelegate.m
//  Mend
//
//  Created by Andrew Goodwin on 12/14/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import <Parse/Parse.h>
#import "PatientDashboardViewController.h"
#import "PhysicianDashboardViewController.h"
#import "GAI.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AppDelegate ()
{
    UINavigationController *navigationController;
    UIView *blackout;
    UIView *bg;
    UILabel *lblError;
    UIButton *btnDismiss;
    UIActivityIndicatorView *activity;
}

@end
NSString * const StripePublishableKey = @"pk_test_Dbs60g1iciywgNtD25mbJgPf";
//NSString * const StripePublishableKey = @"pk_live_UvuA3GZX8fVe8Ex8ZblOqykN";

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    //Enable Google sign-in Dec 1
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError,@"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
  
    
    
    //Enable Facebook sign-in Dec 1
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
   
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setValue:nil forKey:@"temporarylogin"];
    //[defaults setValue:nil forKey:@"hasSeenTour"];
    
    if(![defaults objectForKey:@"hasSeenTour"])
    {
        [defaults setValue:@0 forKey:@"hasSeenTour"];
    }
    
    [defaults synchronize];
    
    [Parse enableLocalDatastore];
    
    // Initialize Parse.
    //mend (live)
    //[Parse setApplicationId:@"hB0mX3nAkuEgMfwc34KJxepSmkpOTA4VyxXayXma" clientKey:@"6DMETnyQp2yGVCPCe8zwbbHztgkBqYFQCA6T8W2A"];
    
    //mend-stage appid and clientkey
    [Parse setApplicationId:@"Gjw62RYfkE9ie96YZ9LV9WUW7umEROM7OVxtRI2N" clientKey:@"RQNB80RH63GweiGTkhdnDfflbY6pgdltXmJbD5NE"];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [Stripe setDefaultPublishableKey:StripePublishableKey];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-68634221-1"];// changed oct 21 2015 from: @"UA-60324414-2"];
    
    /*PFACL *defaultACL = [PFACL ACL];
    // Everybody can read objects created by this user
    [defaultACL setPublicReadAccess:YES];
    [defaultACL setPublicWriteAccess:YES];
    // And the user can read and modify its own objects
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];*/
    
    //test update
    // Override point for customization after application launch.
    
    /*//UIColor *aColor = [UIColor colorWithRed:0.118 green:0.659 blue:0.323 alpha:1.000];
     //[[UINavigationBar appearance] setBackgroundColor:aColor];
     //[[UINavigationBar appearance] setTintColor:aColor];*/
     
     NSShadow *shadow = [[NSShadow alloc] init];
     shadow.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.4];
     shadow.shadowOffset = CGSizeMake(1, 0);
     
     [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,
     [UIFont fontWithName:@"Avenir-Book" size:18.0], NSFontAttributeName,
     nil]];
    
    //[[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
     
     [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont fontWithName:@"Avenir-Book" size:14.0], NSFontAttributeName,
     [UIColor whiteColor], NSForegroundColorAttributeName,
     nil]                               forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    __weak __typeof(self) wself = self;
    
    void (^showCustomerDashboard)(void) = ^{
        __strong __typeof(self) sself = wself;
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PatientDashboardViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PatientDashboard"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        navigationController.delegate = sself;
        sself.window.rootViewController = navigationController;
        [navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
        [navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
        [sself.window makeKeyAndVisible];
    };
    
    void (^showProviderDashboard)(void) = ^{
        __strong __typeof(self) sself = wself;
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PhysicianDashboardViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PhysicianDashboard"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        navigationController.delegate = sself;
        sself.window.rootViewController = navigationController;
        [navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
        [navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
        [sself.window makeKeyAndVisible];
    };
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"ProviderLoggedIn" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        showProviderDashboard();
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"CustomerLoggedIn" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        showCustomerDashboard();
    }];
    
    if ([User sharedUser].providerID){
        showProviderDashboard();
    }
    else{
        showCustomerDashboard();
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    NSString *urlString = [url absoluteString];
    
    
    BOOL urlWasHandled;

    /*BOOL urlWasHandled = [FBAppCall handleOpenURL:url
                                sourceApplication:sourceApplication
                                  fallbackHandler:^(FBAppCall *call) {
                                      ////NSLog(@"Unhandled deep link: %@", url);
     
                                  }];*/
    if([urlString containsString:@"com.googleusercontent.apps.662579582283-21ajucmcb522co9fvq5ijj5cii29914l"]){
        urlWasHandled = [[GIDSignIn sharedInstance] handleURL:url
                                            sourceApplication:sourceApplication
                                                   annotation:annotation];
    }
    else {
        urlWasHandled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url  sourceApplication:sourceApplication annotation:annotation];
        
    }
    
    
    
    return urlWasHandled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Logs 'install' and 'app activate' App Events.
    
    //Remove Dec 1 FBAPPEvent is Deprecated
    //[FBAppEvents activateApp];
    
    //Added Log App Activations Dec 1
    [FBSDKAppEvents activateApp];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateAppointments" object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    
}

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    NSString *userId = user.userID;
    NSString *idToken = user.authentication.idToken;
    NSString *name  = user.profile.name;
    NSString *email = user.profile.email;
}



-(void)showModal:(NSString *)message{
    if(blackout == nil)
    {
        blackout = [[UIView alloc] initWithFrame:self.window.frame];
        blackout.backgroundColor = [UIColor blackColor];
        blackout.layer.opacity = 0.7;
        [self.window addSubview:blackout];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeModal:)];
        singleTap.numberOfTapsRequired = 1;
        [blackout addGestureRecognizer:singleTap];
    }
    
    if(bg == nil)
    {
        bg = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.window.frame.size.height /2 - 100, self.window.frame.size.width - 100.0, 400.0)];
        bg.backgroundColor = [UIColor whiteColor];
        [self.window addSubview:bg];
        
        
    }
    
    if(lblError == nil)
    {
        lblError = [[UILabel alloc] initWithFrame:CGRectZero];
        lblError.textAlignment = NSTextAlignmentCenter;
        lblError.numberOfLines = 0;
        
        lblError.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
        lblError.lineBreakMode = NSLineBreakByWordWrapping;
        [bg addSubview:lblError]; 
    }
    
    if(activity != nil)
       [activity stopAnimating];
    
    
    lblError.text = message;
    
    UIFont *font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:message attributes:@{ NSFontAttributeName: font }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){bg.frame.size.width - 20.0, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    CGFloat height = ceilf(rect.size.height);
    
    lblError.frame = CGRectMake(10.0, 20.0, bg.frame.size.width - 20.0, height);
    
    CGFloat bgHeight = 20 + height + 30.0 + 70.0; //20 is top padding, 30 is the whitespace, and 70 is the button + padding
    
    bg.frame = CGRectMake(50.0, self.window.frame.size.height /2 - bgHeight/2, self.window.frame.size.width - 100.0, bgHeight);

    if(btnDismiss == nil)
    {
        btnDismiss = [UIButton buttonWithType:UIButtonTypeCustom];
        btnDismiss.frame = CGRectMake(bg.frame.size.width/2 - 30.0 , bg.frame.size.height - 70.0, 60.0, 60.0);
        
        [btnDismiss addTarget:self action:@selector(closeModal:) forControlEvents:UIControlEventTouchUpInside];
        [bg addSubview:btnDismiss]; 
    }
    
    [btnDismiss setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    
}

-(void)showModalWithSuccess:(NSString *)message{
    if(blackout == nil)
    {
        blackout = [[UIView alloc] initWithFrame:self.window.frame];
        blackout.backgroundColor = [UIColor blackColor];
        blackout.layer.opacity = 0.7;
        [self.window addSubview:blackout];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeModal:)];
        singleTap.numberOfTapsRequired = 1;
        [blackout addGestureRecognizer:singleTap];
    }
    
    if(bg == nil)
    {
        bg = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.window.frame.size.height /2 - 100, self.window.frame.size.width - 100.0, 400.0)];
        bg.backgroundColor = [UIColor whiteColor];
        [self.window addSubview:bg];
        
        
    }
    
    if(lblError == nil)
    {
        lblError = [[UILabel alloc] initWithFrame:CGRectZero];
        lblError.textAlignment = NSTextAlignmentCenter;
        lblError.numberOfLines = 0;
        
        lblError.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
        lblError.lineBreakMode = NSLineBreakByWordWrapping;
        [bg addSubview:lblError];
    }
    
    if(activity != nil)
        [activity stopAnimating];
    
    
    lblError.text = message;
    
    UIFont *font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:message attributes:@{ NSFontAttributeName: font }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){bg.frame.size.width - 20.0, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    CGFloat height = ceilf(rect.size.height);
    
    lblError.frame = CGRectMake(10.0, 20.0, bg.frame.size.width - 20.0, height);
    
    CGFloat bgHeight = 20 + height + 30.0 + 70.0; //20 is top padding, 30 is the whitespace, and 70 is the button + padding
    
    bg.frame = CGRectMake(50.0, self.window.frame.size.height /2 - bgHeight/2, self.window.frame.size.width - 100.0, bgHeight);
    
    if(btnDismiss == nil)
    {
        btnDismiss = [UIButton buttonWithType:UIButtonTypeCustom];
        btnDismiss.frame = CGRectMake(bg.frame.size.width/2 - 30.0 , bg.frame.size.height - 70.0, 60.0, 60.0);
        
        [btnDismiss addTarget:self action:@selector(closeModal:) forControlEvents:UIControlEventTouchUpInside];
        [bg addSubview:btnDismiss];
    }
    
    [btnDismiss setImage:[UIImage imageNamed:@"icon_confirm"] forState:UIControlStateNormal];
}

-(void)showProcessingModal:(NSString *)message{
    if(blackout == nil)
    {
        blackout = [[UIView alloc] initWithFrame:self.window.frame];
        blackout.backgroundColor = [UIColor blackColor];
        blackout.layer.opacity = 0.7;
        [self.window addSubview:blackout];
        
        /*UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeModal:)];
        singleTap.numberOfTapsRequired = 1;
        [blackout addGestureRecognizer:singleTap];*/
    }
    
    if(bg == nil)
    {
        bg = [[UIView alloc] initWithFrame:CGRectMake(50.0, self.window.frame.size.height /2 - 100, self.window.frame.size.width - 100.0, 400.0)];
        bg.backgroundColor = [UIColor whiteColor];
        [self.window addSubview:bg];
        
        
    }
    
    if(lblError == nil)
    {
        lblError = [[UILabel alloc] initWithFrame:CGRectZero];
        lblError.textAlignment = NSTextAlignmentCenter;
        lblError.numberOfLines = 0;
        
        lblError.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
        lblError.lineBreakMode = NSLineBreakByWordWrapping;
        [bg addSubview:lblError];
    }
    
    
    lblError.text = message;
    
    UIFont *font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:message attributes:@{ NSFontAttributeName: font }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){bg.frame.size.width - 20.0, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    CGFloat height = ceilf(rect.size.height);
    
    lblError.frame = CGRectMake(10.0, 20.0, bg.frame.size.width - 20.0, height);
    
    CGFloat bgHeight = 20 + height + 30.0 + 45.0; //20 is top padding, 30 is the whitespace, and 30 is the button + padding
    
    bg.frame = CGRectMake(50.0, self.window.frame.size.height /2 - bgHeight/2, self.window.frame.size.width - 100.0, bgHeight);
    
    if(activity == nil)
    {
        activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activity.color = [UIColor blackColor];
        activity.frame = CGRectMake(bg.frame.size.width/2 - 30.0 , bg.frame.size.height - 70.0, 60.0, 60.0);
        activity.hidesWhenStopped = YES;
        [bg addSubview:activity];
    }
    
    [activity startAnimating];
}

-(IBAction)closeModal:(id)sender{
    [self hideModal];
}

-(void)hideModal{
    [btnDismiss removeFromSuperview];
    btnDismiss = nil;
    [lblError removeFromSuperview];
    lblError = nil;
    [bg removeFromSuperview];
    bg = nil;
    [blackout removeFromSuperview];
    blackout = nil;
    [activity removeFromSuperview];
    activity = nil;
}

@end
