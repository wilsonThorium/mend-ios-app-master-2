//
//  main.m
//  Mend
//
//  Created by Andrew Goodwin on 12/14/14.
//  Copyright (c) 2014 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
