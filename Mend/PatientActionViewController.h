//
//  PatientActionViewController.h
//  Mend
//
//  Created by Andrew Goodwin on 2/4/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GPS.h"
#import "GAITrackedViewController.h"

@interface PatientActionViewController : GAITrackedViewController <MKMapViewDelegate>
@property (nonatomic,strong) NSMutableDictionary *appointment;
@property (nonatomic,strong) UIImage *imgPatient;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *distance;
@end
