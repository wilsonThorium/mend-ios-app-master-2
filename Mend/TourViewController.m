//
//  TourViewController.m
//  Mend
//
//  Created by Andrew Goodwin on 2/18/15.
//  Copyright (c) 2015 Few. All rights reserved.
//

#import "TourViewController.h"

@interface TourViewController ()
{
    UIView *arc;
    IBOutlet UIScrollView *scroller;
    UIPageControl *pager;
}
@end

@implementation TourViewController
@synthesize fromProfile;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    /*arc = [[UIView alloc] initWithFrame:CGRectMake(-1000, self.view.frame.size.height - 106.0, 2000 + self.view.frame.size.width, 170)];
    arc.backgroundColor = [UIColor whiteColor];
    arc.layer.cornerRadius = arc.frame.size.width/2;
    arc.clipsToBounds = YES;
    //[self.view addSubview:arc];
    arc.layer.zPosition = 100;*/
    
    scroller.layer.zPosition = 1000;
    scroller.delaysContentTouches = NO;
    
    scroller.contentSize = CGSizeMake(self.view.frame.size.width * 5, self.view.frame.size.height);
    
    
    double ratio = 766.0/376.0;
    
    UIView *screen0 = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height - 150.0)];
    
    UIImageView *step0 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50.0, 60.0, 100.0, 100.0)];
    step0.image = [UIImage imageNamed:@"mend_logo_white"];
    step0.contentMode = UIViewContentModeScaleAspectFit;
    [screen0 addSubview:step0];
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 190.0, self.view.frame.size.width - 20.0, 30.0)];
    lbl1.font = [UIFont fontWithName:@"Avenir-Heavy" size:28.0];
    lbl1.text = @"Why use Mend?";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.textColor = [UIColor whiteColor];
    [screen0 addSubview:lbl1];
    
     UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 240.0, self.view.frame.size.width - 20.0, 30.0)];
     lbl2.font = [UIFont fontWithName:@"Avenir-Book" size:15.0];
     lbl2.text = @"We come to you on your schedule.";
     lbl2.textAlignment = NSTextAlignmentCenter;
     lbl2.textColor = [UIColor whiteColor];
     [screen0 addSubview:lbl2];
     
     UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 270.0, self.view.frame.size.width - 20.0, 30.0)];
     lbl3.font = [UIFont fontWithName:@"Avenir-Book" size:15.0];
     lbl3.text = @"Diagnosis and treatment where you are.";
    lbl3.textAlignment = NSTextAlignmentCenter;
     lbl3.textColor = [UIColor whiteColor];
     [screen0 addSubview:lbl3];
     
     UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 300.0, self.view.frame.size.width - 20.0, 30.0)];
     lbl4.font = [UIFont fontWithName:@"Avenir-Book" size:15.0];
     lbl4.text = @"No charge until you're seen.";
    lbl4.textAlignment = NSTextAlignmentCenter;
     lbl4.textColor = [UIColor whiteColor];
     [screen0 addSubview:lbl4];
     
     UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 330.0, self.view.frame.size.width - 20.0, 30.0)];
     lbl5.font = [UIFont fontWithName:@"Avenir-Book" size:15.0];
     lbl5.text = @"Prescription delivery included.";
    lbl5.textAlignment = NSTextAlignmentCenter;
     lbl5.textColor = [UIColor whiteColor];
     [screen0 addSubview:lbl5];
    
    UILabel *lblStep0 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 90.0, self.view.frame.size.width, 30.0)];
    lblStep0.text = @"Hello.";
    lblStep0.font = [UIFont fontWithName:@"Avenir-Heavy" size:21.0];
    lblStep0.textAlignment = NSTextAlignmentCenter;
    [screen0 addSubview:lblStep0];
    
    UILabel *lblDirection0 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 60.0, self.view.frame.size.width, 30.0)];
    lblDirection0.text = @"Swipe to learn more.";
    lblDirection0.font = [UIFont fontWithName:@"Avenir-Book" size:18.0];
    lblDirection0.textAlignment = NSTextAlignmentCenter;
    [screen0 addSubview:lblDirection0];
    
    UIView *screen1 = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0.0, self.view.frame.size.width, self.view.frame.size.height - 150.0)];
    
    UIImageView *step1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width * .6)/2, 40.0, self.view.frame.size.width * .6, self.view.frame.size.width * .6 * (ratio))];
    step1.image = [UIImage imageNamed:@"step_1_screen"];
    [screen1 addSubview:step1];
    
    UILabel *lblStep1 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 90.0, self.view.frame.size.width, 30.0)];
    lblStep1.text = @"Step 1.";
    lblStep1.font = [UIFont fontWithName:@"Avenir-Heavy" size:21.0];
    lblStep1.textAlignment = NSTextAlignmentCenter;
    [screen1 addSubview:lblStep1];
    
    UILabel *lblDirection1 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 60.0, self.view.frame.size.width, 30.0)];
    lblDirection1.text = @"Create an account.";
    lblDirection1.font = [UIFont fontWithName:@"Avenir-Book" size:18.0];
    lblDirection1.textAlignment = NSTextAlignmentCenter;
    [screen1 addSubview:lblDirection1];
    
    UIView *screen2 = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 2, 0.0, self.view.frame.size.width, self.view.frame.size.height - 150.0)];
    
    UIImageView *step2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width * .6)/2, 40.0, self.view.frame.size.width * .6, self.view.frame.size.width * .6 * (ratio))];
    step2.image = [UIImage imageNamed:@"step_2_screen"];
    [screen2 addSubview:step2];
    
    UILabel *lblStep2 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 90.0, self.view.frame.size.width, 30.0)];
    lblStep2.text = @"Step 2.";
    lblStep2.font = [UIFont fontWithName:@"Avenir-Heavy" size:21.0];
    lblStep2.textAlignment = NSTextAlignmentCenter;
    [screen2 addSubview:lblStep2];
    
    UILabel *lblDirection2 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 60.0, self.view.frame.size.width, 30.0)];
    lblDirection2.text = @"Request a visit.";
    lblDirection2.font = [UIFont fontWithName:@"Avenir-Book" size:18.0];
    lblDirection2.textAlignment = NSTextAlignmentCenter;
    [screen2 addSubview:lblDirection2];
    
    UIView *screen3 = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 3, 0.0, self.view.frame.size.width, self.view.frame.size.height - 150.0)];
    
    UIImageView *step3 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width * .6)/2, 40.0, self.view.frame.size.width * .6, self.view.frame.size.width * .6 * (ratio))];
    step3.image = [UIImage imageNamed:@"step_3_screen"];
    [screen3 addSubview:step3];
    
    UILabel *lblStep3 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 90.0, self.view.frame.size.width, 30.0)];
    lblStep3.text = @"Step 3.";
    lblStep3.font = [UIFont fontWithName:@"Avenir-Heavy" size:21.0];
    lblStep3.textAlignment = NSTextAlignmentCenter;
    [screen3 addSubview:lblStep3];
    
    UILabel *lblDirection3 = [[UILabel alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height - 60.0, self.view.frame.size.width, 30.0)];
    lblDirection3.text = @"Just rest. We'll be there soon.";
    lblDirection3.font = [UIFont fontWithName:@"Avenir-Book" size:18.0];
    lblDirection3.textAlignment = NSTextAlignmentCenter;
    [screen3 addSubview:lblDirection3];
    
    UIView *screen4 = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 4, 0.0, self.view.frame.size.width, self.view.frame.size.height - 150.0)];
    
    UIImageView *step4 = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50.0, 60.0, 100.0, 100.0)];
    step4.image = [UIImage imageNamed:@"mend_logo_white"];
    step4.contentMode = UIViewContentModeScaleAspectFit;
    [screen4 addSubview:step4];
    
    UILabel *lbl6 = [[UILabel alloc] initWithFrame:CGRectMake(40.0, 170.0, self.view.frame.size.width - 80.0, 160.0)];
    lbl6.font = [UIFont fontWithName:@"Avenir-Heavy" size:28.0];
    lbl6.text = @"So, what are you waiting for? Let's get you on the mend!";
    lbl6.textAlignment = NSTextAlignmentCenter;
    lbl6.textColor = [UIColor whiteColor];
    lbl6.numberOfLines = 0;
    lbl6.lineBreakMode = NSLineBreakByWordWrapping;
    [screen4 addSubview:lbl6];
    
    UIButton *btnGetStarted = [UIButton buttonWithType:UIButtonTypeCustom];
    btnGetStarted.frame = CGRectMake(self.view.frame.size.width * 4 + 20.0, self.view.frame.size.height - 70.0, self.view.frame.size.width - 40.0, 45.0);
    [btnGetStarted setTitle:@"GET STARTED" forState:UIControlStateNormal];
    [btnGetStarted addTarget:self action:@selector(getStarted) forControlEvents:UIControlEventTouchUpInside];
    btnGetStarted.backgroundColor = [UIColor blackColor];
    [btnGetStarted setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnGetStarted.layer.zPosition = 5000;
    btnGetStarted.userInteractionEnabled = YES;
    
    [scroller addSubview:screen0];
    [scroller addSubview:screen1];
    [scroller addSubview:screen2];
    [scroller addSubview:screen3];
    [scroller addSubview:screen4];
    [scroller addSubview:btnGetStarted];
    // Do any additional setup after loading the view.
    
    pager = [[UIPageControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 75.0, self.view.frame.size.height - 140.0, 150.0, 30.0)];
    pager.numberOfPages = 5;
    pager.currentPage = 0;
    pager.tintColor = [UIColor whiteColor];
    [self.view addSubview:pager];
    pager.pageIndicatorTintColor = [UIColor blackColor];
    
    pager.layer.zPosition = 3000;
    
    if(fromProfile){
        UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
        close.frame = CGRectMake(10.0, 20.0, 50.0, 20.0);
        [close setTitle:@"CLOSE" forState:UIControlStateNormal];
        close.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:14.0];
        [close addTarget:self action:@selector(getStarted) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:close];
        close.layer.zPosition = 4000;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = scroller.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = scroller.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    pager.currentPage = page;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName = @"Tour";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [arc removeFromSuperview];
}

-(void)getStarted{
    if(fromProfile)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
