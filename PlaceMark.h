//
//  PlaceMark.h
//  ConwayCorp
//
//  Created by Developer on 7/13/09.
//  Copyright 2009 Conway Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PlaceMark : NSObject <MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString *_title;
	NSString *_subtitle;
	NSString *_color;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *_title;
@property (nonatomic, copy) NSString *_subtitle;
@property (nonatomic, copy) NSString *_color;
-(id)initWithCoordinate:(CLLocationCoordinate2D) coordinate
				  title:(NSString *)title
				  subtitle:(NSString *)subtitle
				  color:(NSString *)color;


@end

